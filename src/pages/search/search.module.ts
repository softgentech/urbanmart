import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SearchPage } from './search';

@NgModule({
  declarations: [
    SearchPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchPage),
    Ng2SearchPipeModule
  ],
})
export class SearchPageModule {}
