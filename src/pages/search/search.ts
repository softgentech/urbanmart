import { Component } from "@angular/core";
import { Events, IonicPage, LoadingController, NavController, NavParams, ToastController } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { HttpClient } from "@angular/common/http";
import { Api } from "../../providers";
import { WoocommerceProvider } from "../../providers/woocommerce/woocommerce";
/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-search",
  templateUrl: "search.html",
})
export class SearchPage {

  term= '';

  cartLength: any;
  noProductDIV = false;
  ProductDIV = false;

  getproductList: any;

  subCategoryList: any;
  subCategoryID: any;

  categoryID:any;

  WooCommerce: any; 
  page: number;

  allMixProductList:any;

  AllDiv= false;

  product: any; 
  reviews: any[] = [];
  selectedOptions: any = {};
  requireOptions: boolean = true;
  productVariations: any[] = [];
  productPrice: number = 0.0;
  selectedVariation: any;
  imgSrc:any;

  checkId:any; 

  //count:any;
  //count_:any;

  public entryStatus = "";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private http: HttpClient,
    private api: Api,
    public loadingCtrl: LoadingController,
    private WP: WoocommerceProvider,
    public toastCtrl: ToastController,
    public events: Events
  ) {

    this.WooCommerce = WP.init(true);

    events.subscribe('cartLength:created', val => {
      this.cartLength = val;
    });

  }

  getAllMixProductList(){
    this.page = 2;

    this.WooCommerce = this.WP.init();     
    let loading = this.loadingCtrl.create({
      spinner: "dots",
      content: "Please wait...",
    });
    loading.present();

    this.WooCommerce.getAsync("products").then(
      (data) => {         
        this.product = JSON.parse(data.body).products;
        console.log("allMixProductList:-", this.product);

        this.noProductDIV = false;
        this.ProductDIV = true;

        loading.dismiss();
      },
      (err) => {
        console.log(err);
      }
    );

  }

  getAllMixProuctListWPJSON(){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
   });

   loading.present();
    this.http.get(this.api.GetAllMixProductList).subscribe((response: any) => {
      loading.dismiss();
     // console.log('response:',response);
      this.product = response.data;
      console.log("allMixProductList_WPJSON:", this.product);
      this.noProductDIV = false;
      this.ProductDIV = true;
    });
  }

  ionViewWillEnter() {
    this.storage.get("cart").then((val) => {
      if (val == null) {
        console.log("cart_null");
      } else {
        this.cartLength = val.length;
      }
    });

  // this.getAllMixProductList();
   this.getAllMixProuctListWPJSON();
  }


// navigate product details
openProductPage(product) {
  console.log("testing:-", product);
  this.navCtrl.push("ProductDetails", { product: product });
}

// testing
async check(justSelectedAttribute, prdct, prID) {
    console.log('Search');
  this.checkId = prID;
 // console.log("CheckID:", this.checkId);

//  console.log("length:-", prdct);
//  console.log("ProductID:-",prdct.id)
 // console.log('check:',justSelectedAttribute);
 // this.selectedOptions = justSelectedAttribute;
 

  let loading = this.loadingCtrl.create({
    content: "Getting Product Variations"
  });

  //counting selected attribute options
  let count = 0;
  for (let k in this.selectedOptions) 
    if (this.selectedOptions.hasOwnProperty(k)) 
      count++;

  let count_ = 0;
  for (var index = 0; index < prdct.attributes.length; index++) {
    
    if(prdct.attributes[index].variation)
      count_++;
         }


         count = 0; // change
         count_ = 0; // change
         

  //checking if user selected all the variation options or not

  if(count_ != count){         
   // this.navCtrl.setRoot(this.navCtrl.getActive().component);  // test
    this.requireOptions = true;
   
    return;
  } else {     

   // this.selectedOptions = justSelectedAttribute  // change

    this.requireOptions = false;
        //Get product variations only once when all product variables are selected by the user
    loading.present();    
    this.productVariations = JSON.parse((await this.WooCommerce.getAsync('products/' + prdct.id + '/variations/')).body);
            console.log('productVariations:-',this.productVariations)
  }   
  let i = 0, matchFailed = false;   
  if (this.productVariations.length > 0) {   
    for (i = 0; i < this.productVariations.length; i++) {      
      matchFailed = false;
      let key: string = "";
      
      for (let j = 0; j < this.productVariations[i].attributes.length; j++) {         
        key = this.productVariations[i].attributes[j].name;

        console.log(this.selectedOptions[key].toLowerCase()+ " " + this.productVariations[i].attributes[j].option.toLowerCase())

        if (this.selectedOptions[key].toLowerCase() == this.productVariations[i].attributes[j].option.toLowerCase()) {
        
          //Do nothing
        } else {
          console.log(matchFailed)
          matchFailed = true;
          break;
        }
      }

      if (matchFailed) {
        continue;
      } else {
        //found the matching variation
        //console.log(productVariations[i])
        this.productPrice = this.productVariations[i].price;
        this.selectedVariation = this.productVariations[i];
        console.log(this.selectedVariation)

        break;

      }

    }

    if(matchFailed == true){
      this.toastCtrl.create({
        message: "No Such Product Found",
        duration: 3000
      }).present().then(()=>{
        this.requireOptions = true;
      })
    }
  } else {
    this.productPrice = this.product.price;

  }

  loading.dismiss();

}

addToCart(product, prID) {         
 // console.log('selectedOptions:',this.selectedOptions);

  if(this.checkId == prID){
   
    //counting selected attribute options
let count = 0;
for (let k in this.selectedOptions) if (this.selectedOptions.hasOwnProperty(k)) count++;

//counting variation attributes options
let count_ = 0;
for (var index = 0; index < product.attributes.length; index++) {
  
  if(product.attributes[index].variation)
    count_++;
  
}
console.log("cart--count_",count_);

//checking if user selected all the variation options or not

 count_ = 0;  // change
 count = 0;  // change

if(count_ != count || this.requireOptions)  // change
{
  this.toastCtrl.create({
    message: "Select Product Options",
    duration: 2000,
    showCloseButton: true
  }).present();
  return; 
}

// console.log('asdsadasd');

this.storage.get("cart").then((data) => {

  if (data == undefined || data.length == 0) {
    data = [];

    data.push({
      "product": product,
      "qty": 1,
      "amount": parseFloat(product.price)
    });

    if(this.selectedVariation){
      data[0].variation = this.selectedVariation;
      data[0].amount = parseFloat(this.selectedVariation.price);
    }
  //  console.log('asdsadasd');

  } else {

    let alreadyAdded = false;
    let alreadyAddedIndex = -1;

    for (let i = 0; i < data.length; i++){
      if(data[i].product.id == product.id){ //Product ID matched
        if(this.productVariations.length > 0){ //Now match variation ID also if it exists
          if(data[i].variation.id == this.selectedVariation.id){
            alreadyAdded = true;
            alreadyAddedIndex = i;
            break;
          }
        } else { //product is simple product so variation does not  matter
          alreadyAdded = true;
          alreadyAddedIndex = i;
          break;
        }
      }
    }

    if(alreadyAdded == true){
      if(this.selectedVariation){
        data[alreadyAddedIndex].qty = parseFloat(data[alreadyAddedIndex].qty) + 1;
        data[alreadyAddedIndex].amount = parseFloat(data[alreadyAddedIndex].amount) + parseFloat(this.selectedVariation.price);
        data[alreadyAddedIndex].variation = this.selectedVariation;
      } else {
        data[alreadyAddedIndex].qty = parseFloat(data[alreadyAddedIndex].qty) + 1;
        data[alreadyAddedIndex].amount = parseFloat(data[alreadyAddedIndex].amount) + parseFloat(data[alreadyAddedIndex].product.price);
      } 
    } else {
      if(this.selectedVariation){
        data.push({
          product: product,
          qty: 1,
          amount: parseFloat(this.selectedVariation.price),
          variation: this.selectedVariation
        })
      } else {
        data.push({
          product: product,
          qty: 1,
          amount: parseFloat(product.price)
        })
      }
    }

  }


  this.storage.set("cart", data).then(() => {
    console.log("Cart Updated");
    console.log(data); 

    this.events.publish('cartLength:created', data.length); // set cart length events  

    this.toastCtrl.create({
      message: "Cart Updated",
      duration: 500
    }).present();
   // this.navCtrl.setRoot(this.navCtrl.getActive().component);  // test   
  })

})
  } else{
    this.toastCtrl.create({
      message: "Select Product Options",
      duration: 2000,
      showCloseButton: true
    }).present();
    return; 
  }



}


  // tab navigation
  homeTab() {
    this.navCtrl.setRoot(
      "HomePage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
  categoryTab() {
    this.navCtrl.setRoot(
      "CategoryPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
  myListTab() {
    this.navCtrl.setRoot(
      "MyListPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
  CartTab() {
    this.navCtrl.setRoot(
      "CartPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
  // end tab navigation

  back(){
    this.storage.get('Session_BackSearch').then((val) => {
      if(val == 'home'){
        this.navCtrl.setRoot(
          "HomePage",
          {},
          {
            animate: true,
            direction: "forward",
          }
        );
      }else if(val == 'myList'){
        this.navCtrl.setRoot(
          "MyListPage",
          {},
          {
            animate: true,
            direction: "forward",
          }
        );
      }else if(val == 'category'){
        this.navCtrl.setRoot(
          "CategoryPage",
          {},
          {
            animate: true,
            direction: "forward",
          }
        );
      }else if(val == 'search'){
        this.navCtrl.setRoot(
          "HomePage",
          {},
          {
            animate: true,
            direction: "forward",
          }
        );
      }else if(val == 'product-details'){
        this.navCtrl.setRoot(
          "HomePage",
          {},
          {
            animate: true,
            direction: "forward",
          }
        );
      }else{
        this.navCtrl.setRoot(
          "HomePage",
          {},
          {
            animate: true,
            direction: "forward",
          }
        );
      }
    });
  }
}
