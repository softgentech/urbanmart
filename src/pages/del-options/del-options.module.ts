import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DelOptionsPage } from './del-options';

@NgModule({
  declarations: [
    DelOptionsPage,
  ],
  imports: [
    IonicPageModule.forChild(DelOptionsPage),
  ],
})
export class DelOptionsPageModule {}
