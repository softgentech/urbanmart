import { HttpClient } from "@angular/common/http";
import { Component } from "@angular/core";
import {
  IonicPage,
  LoadingController,
  NavController,
  NavParams,
  ToastController,
} from "ionic-angular";
import { Api } from "../../providers";
import { Storage } from "@ionic/storage";

/**
 * Generated class for the DelOptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-del-options",
  templateUrl: "del-options.html",
})
export class DelOptionsPage {
  Time: any;
  myDate: any;

  Default_Address: any;
  userID: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private http: HttpClient,
    public apiPvdr: Api,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController
  ) {}

  ionViewWillEnter() {
    this.storage.get("Session_login").then((val) => {
      console.log('UserLogin:',val);
      console.log("USERID:",val.ID);
      this.userID = val.ID;
      let loading = this.loadingCtrl.create({
        content: "Please wait...",
      });
      loading.present();
      // get default address
      this.http
        .get(this.apiPvdr.getAllAddressApi + this.userID)
        .subscribe((response: any) => {
          if (response.code == 200) {
            for (let i = 0; i < response.data.length; i++) {
              if (response.data[i].defaultAddress == 1) {
                this.Default_Address = response.data[i];
                // console.log('Default_Address:',this.Default_Address);
                loading.dismiss();
              }
            }
          } else {
           // alert("Something went wrong! Please try again later");
            loading.dismiss();
            this.navCtrl.setRoot(
              "AddAddressPage",
              {},
              {
                animate: true,
                direction: "forward",
              }
            );
          }
        });
    });
  }

  back() {
    this.navCtrl.setRoot(
      "CartPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
  chooseDelpage() {
    this.storage.set("Session_chooseDelPage", "delOption"); // set the session

    this.navCtrl.setRoot(
      "ChooseDelAddressPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }

  proceedToPay() {
    //this.navCtrl.push('Checkout');
    if (this.Time == null) {
      let toast = this.toastCtrl.create({
        message: 'Please select time',
        duration: 1000,
        position: 'bottom'
      });    
      toast.onDidDismiss(() => {       
      });
    
      toast.present();
    } else if (this.myDate == null) {    
        let toast = this.toastCtrl.create({
          message: 'Please select date',
          duration: 1000,
          position: 'bottom'
        });      
        toast.onDidDismiss(() => {        
        });      
        toast.present();
      }
    else {
      this.navCtrl.push('Checkout');
    }
  
}

  onChange(selectedValue) {
    this.Time = selectedValue;
   // console.info("Selected_time:", this.Time);
    this.storage.set("session_selectedTime", this.Time);
  }

  showdate() {
   // console.log("Date:-", this.myDate);
    this.storage.set("session_date", this.myDate);
  }
}
