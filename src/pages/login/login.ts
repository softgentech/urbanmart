import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  AlertController,
  Events,
  LoadingController,
} from "ionic-angular";
import { Storage } from "@ionic/storage";
import { HttpClient } from "@angular/common/http";
import { Api } from "../../providers/api/api";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html",
})
export class LoginPage {
  ionicForm: FormGroup;
  isSubmitted = false;

  username: string;
  password: string;

  constructor(
    public loadingCtrl: LoadingController,
    public formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public storage: Storage,
    public alertCtrl: AlertController,
    public events: Events,
    public http: HttpClient,
    public api: Api
  ) {
    this.username = "";
    this.password = "";

    this.ionicForm = this.formBuilder.group({
      mobile: ["", [Validators.required, Validators.pattern("^[0-9]{10}$")]],
    });
  }

  // validaion login
  get errorControl() {
    return this.ionicForm.controls;
  }
  submitForm() {
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log("Please provide all the required values!");
      return false;
    } else {
      console.log(this.ionicForm.value.mobile);
      this.sendOtp(this.ionicForm.value.mobile);
    }
  }

  // generate Otp
  sendOtp(mobile) {
    let postData = {
      mobile: mobile,
    };
    //console.log('body:',postData)
    let loading = this.loadingCtrl.create({
      content: "Please wait...",
    });

    loading.present();

    this.http
      .post(this.api.otpResendApi, postData)

      .subscribe(
        (data: any) => {
          loading.dismiss();
          console.log("data:", data);

          if (data.code == 200) {
            this.storage.set("session_mobile", mobile);
           // alert(data.message);
            this.navCtrl.setRoot(
              "OtpLoginPage",
              {},
              {
                animate: true,
                direction: "forward",
              }
            );
          } else {
            alert(data.message);
          }
        },
        (error) => {
          console.log("Error", error);
        }
      );
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad LoginPage");
  }

  close() {
    this.navCtrl.setRoot(
      "HomePage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }

  signuppage() {
    this.navCtrl.setRoot(
      "SignupPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }

  login() {
    this.http
      .get(
        "https://aba-india.org/urbanMart/wp-json/wp/v3/users?insecure=cool&username=" +
          this.username +
          "&password=" +
          this.password
      )
      .subscribe((res: any) => {
        console.log(res);

        let response = res;

        if (response.error) {
          this.toastCtrl
            .create({
              message: response.error,
              duration: 5000,
            })
            .present();
          return;
        }

        this.storage.set("userLoginInfo", response).then((data) => {
          this.alertCtrl
            .create({
              title: "Login Successful",
              message: "You have been logged in successfully.",
              buttons: [
                {
                  text: "OK",
                  handler: () => {
                    this.navCtrl.push("Checkout");
                    // this.events.publish("updateMenu");

                    // if(this.navParams.get("next")){
                    //   this.navCtrl.push(this.navParams.get("next"));
                    // } else {
                    //   this.navCtrl.pop();
                    // }
                  },
                },
              ],
            })
            .present();
        });
      });
  }
}
