import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupDetailsPage } from './signup-details';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    SignupDetailsPage,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    IonicPageModule.forChild(SignupDetailsPage),
  ],
})
export class SignupDetailsPageModule {}
