import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Events, IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';
import { Api } from '../../providers';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
;
/**
 * Generated class for the SignupDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup-details',
  templateUrl: 'signup-details.html',
})
export class SignupDetailsPage { 
  player_id: "";
  sessionMobile = '';
  ionicForm: FormGroup;
  isSubmitted = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public httpClient: HttpClient, public apiPvdr: Api, public formBuilder: FormBuilder,
              public storage: Storage, public events: Events, public loadingCtrl: LoadingController) {

                


      // validation customer
     this.ionicForm = this.formBuilder.group({
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
    //  username: ['', [Validators.required]],
      email: ['',[Validators.pattern('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}')]],         
    //  password: ['', [Validators.required, Validators.minLength(6)]],
    //  con_password: ['', [Validators.required]],        
    }, 
    {
      validators: this.password.bind(this)
    });
    // end validation
               
  }

  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get('password');
    const { value: con_password } = formGroup.get('con_password');
    return password === con_password ? null : { passwordNotMatch: true };

  }
  get errorControl() {
    return this.ionicForm.controls;
  }
  submitForm() {
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    } else {
      console.log(this.ionicForm.value);
      this.sendPostRequest(this.ionicForm.value); // post method
    }
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupDetailsPage');

    // get session mobile number
    this.storage.get('session_mobile').then((val) => {      
      this.sessionMobile = val;       
    });
    this.storage.get('session_uid').then((val) => {
      this.player_id = val;
      console.log('Your playerID is:', this.player_id);     
    });

  }
  closeIcon(){
    this.navCtrl.setRoot('SignupPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }
  


  sendPostRequest(data) { 
    let postData = {
            firstname: data.firstname,
            lastname: data.lastname,
           // username: data.username,
            email: data.email,    // add email
           // password: data.password,
            mobile : this.sessionMobile,               // add mobile
            playerId: this.player_id           // add playerId
    }
    console.log('body:',postData)

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });    
    loading.present();

    // this.httpClient.post("https://aba-india.org/urbanMart/wp-json/wp/v2/users/register", postData)
    this.httpClient.post(this.apiPvdr.signupApi, postData)

      .subscribe((data: any) => {    
        loading.dismiss(); 
        console.log("data", data);           
                  if(data.code == 200){   
       
                  alert(data.message);        
                       
                  this.storage.set('Session_login', data);  // session login user data

                  this.events.publish('user:created', data);  // events

                  this.navCtrl.setRoot('HomePage', {}, {
         animate: true,
         direction: 'forward'
       });
   
     }  else if(data.code == 406){         
            alert(data.message);
     } 
       }, error => {
        console.log(error);
      });
  }

  
}
