import { Api } from "./../../providers/api/api";
import { Component } from "@angular/core";
import { IonicPage, LoadingController, NavController, NavParams } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { HttpClient } from "@angular/common/http";

@IonicPage()
@Component({
  selector: "page-sub-category",
  templateUrl: "sub-category.html",
})
export class SubCategoryPage {

  subCategoryList:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private http: HttpClient,
    private api: Api,
    public loadingCtrl: LoadingController
  ) {}

  ionViewDidLoad() {
    this.storage.get("Session_Category_Id").then((val) => {     
      let postData = {
        categoryid: val       
      };
    //  console.log("body:-", postData);

    let loading = this.loadingCtrl.create({
       content: 'Please wait...'
    });

    loading.present();

      this.http
        .post(this.api.Sub_CategoryApi, postData)

        .subscribe(
          (data: any) => {            
            this.subCategoryList = data.data;
            loading.dismiss();
            //console.log("subCategoryList:", this.subCategoryList);
          },
          (error) => {
            console.log(error);
          }
        );
    });
  }

// Navigate to my-list
productList(subID){  
  this.storage.set("Session_subCatId", subID);
  this.navCtrl.setRoot(
    "MyListPage",
    {},
    {
      animate: true,
      direction: "forward",
    }
  );
}

  close() {
    this.navCtrl.setRoot(
      "HomePage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
}
