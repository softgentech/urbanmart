import { HttpClient } from "@angular/common/http";
import { Component } from "@angular/core";
import { Storage } from "@ionic/storage";
import {
  IonicPage,
  LoadingController,
  NavController,
  NavParams,
} from "ionic-angular";
import { Api } from "../../providers";

/**
 * Generated class for the CategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-category",
  templateUrl: "category.html",
})
export class CategoryPage {
  cartLength:any;
  categoryList: any;
  subCategoryList: any;

  testDiv = false;

  public subcategoryDiv:any;
  downIcon = true;
  UpIcon = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: Api,
    private http: HttpClient,
    public loadingCtrl: LoadingController,
    private storage: Storage
  ) {}

  categoryDiv(category_id) {
  // console.log("category_id:-", category_id);
   this.storage.set("Session_Category_Id", category_id);
    this.testDiv = false;
    this.subcategoryDiv = category_id;

    // this.subcategoryDiv = !this.subcategoryDiv;
    // this.downIcon = !this.downIcon;
    // this.UpIcon = !this.UpIcon;

    let postData = {
      categoryid: category_id,
    };

    let loading = this.loadingCtrl.create({
      // content: 'Please wait...'
    });

    loading.present();

    this.http
      .post(this.api.Sub_CategoryApi, postData)

      .subscribe(
        (data: any) => {
         loading.dismiss(); // dismiss loading
          this.subCategoryList = data.data;
          this.testDiv = true;
          console.log("subCategoryList:", this.subCategoryList);
        },
        (error) => {
          console.log(error);
        }
      );
  }

  // Navigate to my-list
productList(subID){  
 // console.log('SubCategoryID:', subID);
  this.storage.set("Session_subCatId", subID);
  this.navCtrl.setRoot(
    "MyListPage",
    {},
    {
      animate: true,
      direction: "forward",
    }
  );
}

  ionViewWillEnter() {
    this.storage.get('cart').then((val) => {
      if(val == null){
        console.log("cart_null");
      }else{
        this.cartLength = val.length;
      }
  });
    this.getAllCategoryList();
  }

  getAllCategoryList() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
   });

   loading.present();
    this.http.get(this.api.getAllCatergoryApi).subscribe((response: any) => {
      loading.dismiss();
      this.categoryList = response.data;
     // console.log("CategoryList:", this.categoryList);
    });
  }

  // tab navigation
  homeTab() {
    this.navCtrl.setRoot(
      "HomePage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
  searchTab() {
    this.storage.set('Session_BackSearch', 'search');
    this.navCtrl.setRoot(
      "SearchPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
  myListTab() {
    this.navCtrl.setRoot(
      "MyListPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
  CartTab() {
    this.navCtrl.setRoot(
      "CartPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
  // end tab navigation

  searchPage(){
    this.storage.set('Session_BackSearch', 'category');
    this.navCtrl.setRoot(
      "SearchPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
}
