import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChooseDelAddressPage } from './choose-del-address';

@NgModule({
  declarations: [
    ChooseDelAddressPage,
  ],
  imports: [
    IonicPageModule.forChild(ChooseDelAddressPage),
  ],
})
export class ChooseDelAddressPageModule {}
