import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';
import { Api } from '../../providers';
import { Storage } from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-choose-del-address',
  templateUrl: 'choose-del-address.html',
})
export class ChooseDelAddressPage {

  userID:any;
  userAddress:any;

  noAddressDiv = false;
  AddressDiv = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private storage: Storage,
              private http: HttpClient, 
              public apiPvdr: Api, 
              public loadingCtrl: LoadingController) {
  }

  ionViewWillEnter(){
    this.storage.get('Session_login').then((val) => {         
      this.userID = val.ID;  
      console.log('UserID:', this.userID);   
      this.getAllAddress();          
    });
  }

  getAllAddress(){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });    
    loading.present();
    
    this.http.get(this.apiPvdr.getAllAddressApi + this.userID).subscribe((response:any) => {
      loading.dismiss(); 
      if(response.code == 200){
        this.noAddressDiv = false;
        this.AddressDiv = true;
        this.userAddress = response.data;
        console.log('User_Address:', this.userAddress);
      }else{
        this.AddressDiv = false;   
        this.noAddressDiv = true;    

      }
      
});
  }

  back() {

    this.storage.get('Session_chooseDelPage').then((val) => {
      console.log('page:', val);
      if(val == 'myAccount'){
        console.log('MyAccount');
        this.navCtrl.setRoot('MyAccountPage', {}, {
          animate: true,
          direction: 'forward'
        });
      } else if(val == 'delOption'){
        console.log('Deloption');
        this.navCtrl.setRoot('DelOptionsPage', {}, {
          animate: true,
          direction: 'forward'
        });
      }
    });
   
     
  }
  addAddress() {
    this.navCtrl.setRoot('AddAddressPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }

  deleteAddress(addID){
    console.log('AddressID:', addID);

    let loading = this.loadingCtrl.create({
      content: 'Deleting Address...'
    });    
    loading.present();
    
    this.http.get(this.apiPvdr.deleteAddressApi + this.userID + "&aid=" + addID).subscribe((response:any) => {
      loading.dismiss();      
      if(response.code == 200){
        // alert(response.message);
         this.getAllAddress();
      }else{
        alert('Something went wrong! Please try again later');
      }
      
});

  }

  radioGroupChange(addressID) {
    console.log("addressID:",addressID);   
    let loading = this.loadingCtrl.create({
      content: 'Selecting Address...'
    });    
    loading.present();
    
    this.http.get(this.apiPvdr.defaultAddressApi + this.userID + "&aid=" + addressID).subscribe((response:any) => {
      loading.dismiss(); 
      console.log("Data:", response);     
      if(response.code == 200){
        // alert(response.message);
         this.getAllAddress();
      }else{
        alert('Something went wrong! Please try again later');
      }
      
});

  }
  radioSelect(event) {
    console.log("radioSelect",event);    
  }

}
