import { HttpClient } from "@angular/common/http";
import { Component } from "@angular/core";
import {
  AlertController,
  IonicPage,
  LoadingController,
  NavController,
  NavParams,
  ViewController,
} from "ionic-angular";
import { Api } from "../../providers";

/**
 * Generated class for the OrderDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-order-details",
  templateUrl: "order-details.html",
})
export class OrderDetailsPage {
  orderId: number;
  orderDetailsList:any;
  lineItem:any;
  userID:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public http: HttpClient,
    public apiPvdr: Api,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) {
   // console.log("orderId", navParams.get("orderId"));
   // console.log("userid", navParams.get("userid"));
    this.orderId = navParams.get("orderId");
    this.userID = navParams.get("userid");
  }

  ionViewDidLoad() {
   // console.log("ionViewDidLoad OrderDetailsPage");
    this.orderDetails();
  }

  dismissModal() {
    let data = { foo: "bar" };
    this.viewCtrl.dismiss(data);
  }

  orderDetails(){
    let postData = {
      orderid: this.orderId,
     //  userid:  12
    };
  //  console.log("body:", postData);

    let loading = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loading.present();
    this.http
      .post(this.apiPvdr.OrderDetailsApi, postData)

      .subscribe(
        (response: any) => {
          loading.dismiss();
        //  console.log("response:-", response.data.orderdata.order);
          if (response.code == 200) {
            this.orderDetailsList = response.data.orderdata.order;    
            this.lineItem =  response.data.orderdata.order.line_items;
           // console.log('Line_item:', this.lineItem);     
          } else {
            // do nothing           
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }


  presentConfirm(orderID) {
   // console.log('test',orderID)
    let alert = this.alertCtrl.create({
      title: 'Cancel order',
      message: 'Are your sure?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
           // console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
           // console.log('Buy clicked');
           this.cancelOrder(orderID);
          }
        }
      ]
    });
    alert.present();
  }
  
  cancelOrder(orderID){   
    let postData = {
      orderid: orderID,
      userid: this.userID    
    };
  //  console.log("body:", postData);
   let loading = this.loadingCtrl.create({
    content: "Please wait...",
  });
  loading.present();
  this.http
    .post(this.apiPvdr.CancelOrderApi, postData)

    .subscribe(
      (response: any) => {
        loading.dismiss();
      //  console.log("response:-", response);
        if (response.code == 200) {
            alert(response.message);
            this.dismissModal();

        } else {
          // do nothing           
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

}
