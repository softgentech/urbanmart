import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams, AlertController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as WC from 'woocommerce-api';
// import { HomePage } from '../home/home';
// import { Menu } from '../menu/menu';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { WoocommerceProvider } from '../../providers/woocommerce/woocommerce';
import { Api } from '../../providers/api/api';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { HttpClient } from '@angular/common/http';
declare var RazorpayCheckout: any;
@IonicPage({})
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class Checkout {

  selectedTime:any;
  selectedDate:any;

  totalPrice:any;
  cod:any;
  Default_Address:any;

  // set geoloaction
  geoLatitude: number;
  geoLongitude: number;
  geoAccuracy:number;
  geoAddress: string;  
 // address_1: 'lucknow'; 
  
   //Geocoder configuration
   geoencoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };
  // end geolocation

  paymentType:any; // Change

  WooCommerce: any;
  newOrder: any;
  paymentMethods: any[];
  createOrderData  : any[];
  paymentMethod: any;
  billing_shipping_same: boolean;
  userInfo: any;
  loadCtrl: LoadingController;

  constructor(public events: Events,private http: HttpClient,public navCtrl: NavController, loadCtrl: LoadingController, public navParams: NavParams, public storage: Storage, public alertCtrl: AlertController, private WP: WoocommerceProvider,public api: Api, public geolocation: Geolocation, private nativeGeocoder: NativeGeocoder) {

    // event fire
    events.subscribe("Session_paymentMethod", (val) => {
      // console.log('Events Data:', data);
      if(val == 'Cash on Delivery'){
        this.paymentType = val;
      //  console.log("TestCOD:", this.paymentType);
    }else {
        this.paymentType = val;
       // console.log('TestRZOR:',this.paymentType);
    }
    });
    // end event fire

    this.newOrder = {};
    this.newOrder.billing = {};
    this.newOrder.shipping = {};
    this.billing_shipping_same = false;
    this.loadCtrl = loadCtrl;

    this.paymentMethods = [
      { method_id: "bacs", method_title: "Direct Bank Transfer" },
      { method_id: "cheque", method_title: "Cheque Payment" },
      { method_id: "cod", method_title: "Cash on Delivery" },
      { method_id: "paypal", method_title: "PayPal" }];

    this.WooCommerce = WP.init(true);

    let loading = this.loadCtrl.create({
      spinner: 'dots',
      content: 'Please wait...'
    });
    loading.present();

    // get total storage
    this.storage.get('Session_total_Price').then((val) => {
      this.totalPrice = val;
      console.log('Session_total_Price:-', this.totalPrice);
    });

    // this.storage.get("userLoginInfo").then((userLoginInfo) => {
      this.storage.get("Session_login").then((userLoginInfo) => {

      console.log('user-Login-info:-', userLoginInfo);

      this.userInfo = userLoginInfo;

      // let email = userLoginInfo[0].name;
      // let id = userLoginInfo[0].id;
       let email = userLoginInfo.user_email;
      let id = userLoginInfo.ID;

      this.WooCommerce.getAsync("customers/"+id).then((data) => {

        this.newOrder = JSON.parse(data.body);
       loading.dismiss();

      // this.getGeolocation();
       // this.newOrder.billing.address_1 = 'lucnow'; // testing
      })

    })

 
 
 
 
  }

  setBillingToShipping() {
    this.billing_shipping_same = !this.billing_shipping_same;

    if (this.billing_shipping_same) {
      this.newOrder.shipping = this.newOrder.billing;
    }

  }

  ionViewWillEnter(){
    this.storage.get('Session_login').then((val) => {                                              
      // get default address
      this.http.get(this.api.getAllAddressApi + val.ID).subscribe((response:any) => {       
        console.log
        if(response.code == 200){         
            for(let i=0; i<response.data.length; i ++){              
              if(response.data[i].defaultAddress == 1){
                this.Default_Address= response.data[i];
                console.log('Default_Address:',this.Default_Address);
               
              }
            }
        }else{
          alert('Something went wrong! Please try again later');
        }
        
  });

    });

    // get  time
    this.storage.get('session_selectedTime').then((val) => {
      this.selectedTime = val;
     // console.log('session_selectedTime', this.selectedTime);
    });
    // get date
    this.storage.get('session_date').then((val) => {
      this.selectedDate = val;
     // console.log('session_date', this.selectedDate);
    });
    
  }

  placeOrder() {

    let loading = this.loadCtrl.create({
      spinner: 'dots',
      content: 'Please wait...'
    });
    loading.present();
   

    let orderItems: any[] = [];
    let data: any = {};

    let paymentData: any = {};

    this.paymentMethods.forEach((element, index) => {
      if (element.method_id == this.paymentMethod) {
        paymentData = element;
      }
    });


    data = {

      //Fixed a bug here. Updated in accordance with wc/v2 API
      payment_method: paymentData.method_id,
      payment_method_title: paymentData.method_title,
      set_paid: true,

      billing: this.newOrder.billing,
      shipping: this.newOrder.shipping,
      customer_id: this.userInfo.ID,
      line_items: orderItems
    };
    console.log("fsdfs", data);

     this.storage.get("cart").then((cart) => {

        cart.forEach((element, index) => {
          if(element.variation){
            orderItems.push({ product_id: element.product.id, variation_id: element.variation.id, quantity: element.qty });
            ///total = total + (element.variation.price * element.qty);
          } else {
            orderItems.push({ product_id: element.product.id, quantity: element.qty });
            ///total = total + (element.product.price * element.qty);
          }
        });

        //data.line_items = orderItems;

        let orderData: any = {};

        orderData.order = data;
         console.log('test12', this.newOrder.email);


        const datas = {
          payment_method: this.paymentType,  // change
          payment_method_title: this.cod,
          customer_id: this.userInfo.ID,
          set_paid: true,
          billing: {
            // first_name: this.newOrder.billing.first_name, // mohd amir
            // last_name: this.newOrder.billing.last_name,  // house no
            // address_1: this.newOrder.billing.address_1,  // apartment name
            // address_2: this.newOrder.billing.address_2,  // street details
            // city: this.newOrder.billing.city,            // Area details
            // state: this.newOrder.billing.state,          // City
            // postcode: this.newOrder.billing.postcode,    // Pin Code
            // country: this.newOrder.billing.country,      // Time & date
            // email: this.newOrder.email,                  // email
            // phone: this.newOrder.billing.phone           // phone

            first_name: this.userInfo.display_name, // mohd amir
            last_name: this.Default_Address.houseno,  // house no
            address_1: this.Default_Address.apartmentname,  // apartment name
            address_2: this.Default_Address.streetDetails,  // street details
            city: this.Default_Address.area,            // Area details
            state: this.Default_Address.city,          // City
            postcode: this.Default_Address.pincode,    // Pin Code
            country: this.selectedTime + this.selectedDate,      // Time & date
            email: this.userInfo.user_email,                  // email
            phone: this.userInfo.user_login           // phone

          },
          shipping: {
            first_name: this.userInfo.display_name,
            last_name: this.Default_Address.houseno,
            address_1: this.Default_Address.apartmentname,
            address_2: this.Default_Address.streetDetails,
            city: this.Default_Address.area,
            state: this.Default_Address.city,
            postcode: this.Default_Address.pincode,
            country: this.selectedTime + this.selectedDate
          },
          line_items: orderItems,
          shipping_lines: [
            {
              method_id: "flat_rate",
              method_title: "Flat Rate",
              total: "00.0"
            }
          ]
        };

        console.log('IYIUYIYIUYUIY', datas);
       

        this.WooCommerce.postAsync("orders", datas)
        .then((response) => {
          loading.dismiss();
          let res_data = (JSON.parse(response.body));
          this.alertCtrl.create({
            title: "Order Placed Successfully",
            message: "Your order has been placed successfully. Your order number is " + res_data.order_key+ "             [ " +this.paymentType+" ]",
            buttons: [{
              text: "OK",
              handler: () => {
                this.navCtrl.setRoot('HomePage');
                this.storage.set('cart', null); // set cart storage
              }
            }]
          }).present();
        })
        .catch((error) => {
          console.log(error.response.body);
        });


        // this.WooCommerce.postAsync("orders", data).then((data:any) => {

        //   let response = (JSON.parse(data.body));

        //   this.alertCtrl.create({
        //     title: "Order Placed Successfully",
        //     message: "Your order has been placed successfully. Your order number is " + response.order_number,
        //     buttons: [{
        //       text: "OK",
        //       handler: () => {
        //         this.navCtrl.setRoot('HomePage');
        //       }
        //     }]
        //   }).present();

        // })

      })

    


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyOrderPage');   
   // this.getGeolocation();
  
  }

  // current location

//Get current coordinates of device
getGeolocation(){
  console.log('work');
 // this.newOrder.billing.address_1 = 'lucnow';
  this.geolocation.getCurrentPosition().then((resp) => {
    this.geoLatitude = resp.coords.latitude;
    this.geoLongitude = resp.coords.longitude; 
    this.geoAccuracy = resp.coords.accuracy; 
    this.getGeoencoder(this.geoLatitude,this.geoLongitude);
   }).catch((error) => {
     alert('Error getting location'+ JSON.stringify(error));
   });
}

//geocoder method to fetch address from coordinates passed as arguments
getGeoencoder(latitude,longitude){
  this.nativeGeocoder.reverseGeocode(latitude, longitude, this.geoencoderOptions)
  .then((result: NativeGeocoderReverseResult[]) => {
   // this.geoAddress = this.generateAddress(result[0]);

    this.newOrder.billing.address_1 = this.generateAddress(result[0]);  // current location
   
  })
  .catch((error: any) => {
    alert('Error getting location'+ JSON.stringify(error));
  });
}

//Return Comma saperated address
generateAddress(addressObj){
    let obj = [];
    let address = "";
    for (let key in addressObj) {
      obj.push(addressObj[key]);
    }
    obj.reverse();
    for (let val in obj) {
      if(obj[val].length)
      address += obj[val]+', ';
    }
  return address.slice(0, -2);
}

  // end current loaction

  // radio select
  codSelect(event) {
    this.cod = "Cash on Delivery";
    this.events.publish('Session_paymentMethod', "Cash on Delivery");
   // console.log(this.cod);    
  }
  razorSelect(event){
    this.cod="Razor Pay";
    //console.log(this.cod);      
  }

  OrderToPlace(){
    if(this.cod == "Cash on Delivery"){
      console.log("COD");    
      this.placeOrder();
    }else {
      console.log("Razor");           
      this.razorPay();     
    }
  }

  razorPay() {
    var options = {
      description: 'Credits towards consultation',
      image: 'https://i.imgur.com/3g7nmJC.png',
      currency: 'INR',
      key: 'rzp_test_E64IpPTPtRHHWh',
      amount: this.totalPrice * 100,
      name: this.userInfo.display_name,
      prefill: {
        email: this.userInfo.user_email,
        contact: this.userInfo.user_login,
        name: this.userInfo.display_name
      },
      theme: {
        color: '#F37254'
      },
      modal: {
        ondismiss: function() {
          alert('dismissed')
        }
      }
    };

    var successCallback = (payment_id) => {
     // alert('payment_id: ' + payment_id);
     // this.storage.set('Session_paymentMethod', "Your Razorpay payment id is: " + payment_id);
      this.events.publish('Session_paymentMethod', "Your Razorpay payment id is: " + payment_id);
      this.placeOrder();      
    };

    var cancelCallback = (error) => {
      alert(error.description + ' (Error ' + error.code + ')');
      //Navigate to another page using the nav controller
      //this.navCtrl.setRoot(ErrorPage)
    };

    RazorpayCheckout.open(options, successCallback, cancelCallback);
  }

}
