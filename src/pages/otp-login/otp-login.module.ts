import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicPageModule } from 'ionic-angular';
import { OtpLoginPage } from './otp-login';

@NgModule({
  declarations: [
    OtpLoginPage,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    IonicPageModule.forChild(OtpLoginPage),
  ],
})
export class OtpLoginPageModule {}
