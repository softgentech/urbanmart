import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import {
  IonicPage,
  LoadingController,
  MenuController,
  NavController,
  ToastController,
} from "ionic-angular";

import { Api, User } from "../../providers";
import { MainPage } from "../";
import { HttpClient } from "@angular/common/http";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Storage } from "@ionic/storage";
@IonicPage()
@Component({
  selector: "page-signup",
  templateUrl: "signup.html",
})
export class SignupPage {
  ionicForm: FormGroup;
  isSubmitted = false;

  // mobile:any;
  buttonDisabled = true;

  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { name: string; email: string; password: string } = {
    name: "Test Human",
    email: "test@example.com",
    password: "test",
  };

  // Our translated text strings
  private signupErrorString: string;

  constructor(
    public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    private menu: MenuController,
    public apiPvdr: Api,
    public httpClient: HttpClient,
    public formBuilder: FormBuilder,
    private storage: Storage,
    public loadingCtrl: LoadingController
  ) {
    this.translateService.get("SIGNUP_ERROR").subscribe((value) => {
      this.signupErrorString = value;
    });

    this.ionicForm = this.formBuilder.group({
      mobile: ["", [Validators.required, Validators.pattern("^[0-9]{10}$")]],
    });
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);
    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(false, 'menu1');
  }

  // validaion customer
  get errorControl() {
    return this.ionicForm.controls;
  }
  submitForm() {
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log("Please provide all the required values!");
      return false;
    } else {
      console.log(this.ionicForm.value.mobile);
      this.sendOtp(this.ionicForm.value.mobile);
    }
  }

  // generate Otp
  sendOtp(mobile) {
    let postData = {
      mobile: mobile,
    };
    //console.log('body:',postData)
    let loading = this.loadingCtrl.create({
      content: "Please wait...",
    });

    loading.present();

    this.httpClient
      .post(this.apiPvdr.otpApi, postData)

      .subscribe(
        (data: any) => {
          loading.dismiss();
          console.log("data:", data);
          if (data.code == 200) {
            this.storage.set("session_mobile", mobile);
           // alert(data.message);

            this.navCtrl.setRoot(
              "OtpPage",
              {},
              {
                animate: true,
                direction: "forward",
              }
            );
          } else if (data.code == 401) {
            alert(data.message);
          }
        },
        (error) => {
          console.log("Error", error);
        }
      );
  }

  close() {
    this.navCtrl.setRoot(
      "HomePage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
  loginpage() {
    this.navCtrl.setRoot(
      "LoginPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }

  doSignup() {
    // Attempt to login in through our User service
    this.user.signup(this.account).subscribe(
      (resp) => {
        console.log("details:", resp);
        this.navCtrl.push(MainPage);
      },
      (err) => {
        console.log(err);
        this.navCtrl.push(MainPage);

        // Unable to sign up
        let toast = this.toastCtrl.create({
          message: this.signupErrorString,
          duration: 3000,
          position: "top",
        });

        toast.present();
      }
    );
  }
}
