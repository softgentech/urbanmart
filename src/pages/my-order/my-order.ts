import { Component } from "@angular/core";
import {
  IonicPage,
  LoadingController,
  ModalController,
  NavController,
  NavParams,
} from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import {
  NativeGeocoder,
  NativeGeocoderReverseResult,
  NativeGeocoderOptions,
} from "@ionic-native/native-geocoder";
import { PostProvider } from "../../providers/post/post";
import { c } from "@angular/core/src/render3";
import { HttpClient } from "@angular/common/http";
import { Api } from "../../providers";
import { Storage } from "@ionic/storage";
import { OrderDetailsPage } from "../order-details/order-details";
/**
 * Generated class for the MyOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-my-order",
  templateUrl: "my-order.html",
})
export class MyOrderPage {
  userID: any;
  emptyDiv = false;
  fillDiv = false;
  madalDismissData: any;
  orderList:any;

  geoLatitude: number;
  geoLongitude: number;
  geoAccuracy: number;
  geoAddress: string;

  //Geocoder configuration
  geoencoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5,
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    public postPvdr: PostProvider,
    public http: HttpClient,
    public apiPvdr: Api,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController
  ) {}

  ionViewWillEnter() {
    this.storage.get("Session_login").then((val) => {
      this.userID = val.ID;
      // console.log('UserID:', this.userID);
     this.getALlOrderList();
    });
  }

  //Get current coordinates of device
  getGeolocation() {
    this.geolocation
      .getCurrentPosition()
      .then((resp) => {
        this.geoLatitude = resp.coords.latitude;
        this.geoLongitude = resp.coords.longitude;
        this.geoAccuracy = resp.coords.accuracy;
        this.getGeoencoder(this.geoLatitude, this.geoLongitude);
      })
      .catch((error) => {
        alert("Error getting location" + JSON.stringify(error));
      });
  }

  //geocoder method to fetch address from coordinates passed as arguments
  getGeoencoder(latitude, longitude) {
    this.nativeGeocoder
      .reverseGeocode(latitude, longitude, this.geoencoderOptions)
      .then((result: NativeGeocoderReverseResult[]) => {
        this.geoAddress = this.generateAddress(result[0]);
        console.log("address-console:", this.geoAddress);
      })
      .catch((error: any) => {
        alert("Error getting location" + JSON.stringify(error));
      });
  }

  //Return Comma saperated address
  generateAddress(addressObj) {
    let obj = [];
    let address = "";
    for (let key in addressObj) {
      obj.push(addressObj[key]);
    }
    obj.reverse();
    for (let val in obj) {
      if (obj[val].length) address += obj[val] + ", ";
    }
    return address.slice(0, -2);
  }

  // get All Order
  getALlOrderList() {
    let postData = {
      userid: this.userID,
     //  userid:  12
    };
   // console.log("body:", postData);

    let loading = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loading.present();
    this.http
      .post(this.apiPvdr.GetAllOrderAPI, postData)

      .subscribe(
        (data: any) => {
          loading.dismiss();
         // console.log("Data:", data.data.prodata);
          if (data.code == 200) {
            this.orderList = data.data.prodata;
            this.emptyDiv = false;
            this.fillDiv = true;
          } else {
            // do nothing
            this.emptyDiv= true;
            this.fillDiv = false;
          }
        },
        (error) => {
          console.log(error);
        }
      );

      
  }

  openModal(orderID) {
    const profileModal = this.modalCtrl.create(OrderDetailsPage, {
      orderId: orderID,
      userid: this.userID
    });
    profileModal.onDidDismiss((data) => {
     // console.log('test2:',data);
      this.getALlOrderList();
      this.madalDismissData = JSON.stringify(data);
    });
    profileModal.present();
  }
}
