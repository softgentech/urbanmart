import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, LoadingController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { WoocommerceProvider } from '../../providers/woocommerce/woocommerce';



/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  pID: any;
  item: any[] = [];

  footerDiv = true;

  cartLength: any;
  permalink: any;

  regularPrice: any;
  sellPrice: any;

  segment: any;
  imgSrc: any;

  product: any;
  WooCommerce: any;
  reviews: any[] = [];
  selectedOptions: any = {};
  requireOptions: boolean = true;
  productVariations: any[] = [];
  productPrice: number = 0.0;
  selectedVariation: any;

  similarProductList: any;
  checkId: any;
  wishList: any;

  cartItems: any[] = [];
  total: any;
  showEmptyCartMessage: boolean = false;
  

  constructor(private WP: WoocommerceProvider, public events: Events,public toastCtrl: ToastController,private loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public viewCtrl: ViewController, public toastController: ToastController) {
  
    this.WooCommerce = WP.init(true)

    this.total = 0.0;
    this.getCartItem();
   

  }

  getCartItem(){
    this.storage.ready().then(()=>{

      this.storage.get("cart").then( (data)=>{
       

        if(data == null){
          console.log("Cart---null");
          this.showEmptyCartMessage = true;
        }else{
          this.cartItems = data;
          console.log('Cart_Items:-', this.cartItems);
          if(this.cartItems.length > 0){

            this.cartItems.forEach( (item, index)=> {
  
              if(item.variation){
                this.total = this.total + (parseFloat(item.variation.price) * item.qty);
               
              } else {
                this.total = this.total + (item.product.price * item.qty)
               // console.log('2-Total:', this.total);
               this.storage.set('Session_total_Price',this.total); // set session
              }
  
            })
            console.log('1-Total:', this.total);
            this.storage.set('Session_total_Price',this.total); // set session
          } else {
  
            this.showEmptyCartMessage = true;
  
          }
        }

      


      })

    })
  }

  ionViewDidLoad() {
    this.storage.get('itemWishList').then((val) => {
      console.log('itemWishList', val);
      this.similarProductList = val;
    });
  }

  removeFromCart(item, i){

    let price;
    
    if(item.variation){
      price = item.variation.price
    } else {
      price = item.product.price;
    }
    let qty = item.qty;

    this.cartItems.splice(i, 1);

    this.storage.set("cart", this.cartItems).then( ()=> {

      this.total = this.total - (price * qty);
     // console.log("remove-total:-",this.total);
     this.storage.set('Session_total_Price',this.total); // set session
    });

    if(this.cartItems.length == 0){
      this.showEmptyCartMessage = true;
    }

  }

  closeModal(){
    this.navCtrl.push('HomePage');
  }

  checkout(){

    // this.storage.get("userLoginInfo").then( (data) => {
      this.storage.get("Session_login").then( (data) => {
      console.log('user info:', data);
      if(data != null){
       // this.navCtrl.push('Checkout');
        this.navCtrl.push('DelOptionsPage');
      } else {
        this.navCtrl.push('LoginPage', {next: 'Checkout'})
      //  this.navCtrl.push('DelOptionsPage', {next: 'Checkout'}) // testing
      }
    })

  }

  changeQty(item, i, change){

    let price;
    
    if(!item.variation)
      price = item.product.price;
    else
      price = parseFloat(item.variation.price);
    
    let  qty = item.qty;

    if(change < 0 && item.qty == 1){
      return;
    }

    qty = qty + change;
    item.qty = qty;
    item.amount = qty * price;

    this.cartItems[i] = item;

    this.storage.set("cart", this.cartItems).then( ()=> {

      this.toastController.create({
        message: "Cart Updated.",
        duration: 2000,
        showCloseButton: true
      }).present();

    });

    this.total = (parseFloat(this.total.toString()) + (parseFloat(price.toString()) * change));
   // console.log("Change-total:-", this.total);
   this.storage.set('Session_total_Price',this.total); // set session

  }

  startShopping() {
    this.navCtrl.setRoot('HomePage', {}, {
      animate: true,
      direction: 'forward'
    });
  }

   // navigate product details
   openProductPage(product) {
   // console.log("testing:-", product);
    this.navCtrl.push("ProductDetails", { product: product });
  }

  // testing
  async check1(justSelectedAttribute, prdct, prID) {
    this.footerDiv = false; // test
    this.checkId = prID;
    let loading = this.loadingCtrl.create({
      content: "Getting Product Variations",
    });

    //counting selected attribute options
    let count = 0;
    for (let k in this.selectedOptions)
      if (this.selectedOptions.hasOwnProperty(k)) count++;

    let count_ = 0;
    for (var index = 0; index < prdct.attributes.length; index++) {
      if (prdct.attributes[index].variation) count_++;
    }

    count = 0; // change
    count_ = 0; // change

    //checking if user selected all the variation options or not

    if (count_ != count) {
      // this.navCtrl.setRoot(this.navCtrl.getActive().component);  // test
      this.requireOptions = true;

      return;
    } else {
      // this.selectedOptions = justSelectedAttribute  // change

      this.requireOptions = false;
      //Get product variations only once when all product variables are selected by the user
      loading.present();
      this.productVariations = JSON.parse(
        (
          await this.WooCommerce.getAsync(
            "products/" + prdct.id + "/variations/"
          )
        ).body
      );
      // console.log('productVariations:-',this.productVariations)
    }
    let i = 0,
      matchFailed = false;
    if (this.productVariations.length > 0) {
      for (i = 0; i < this.productVariations.length; i++) {
        matchFailed = false;
        let key: string = "";

        for (let j = 0; j < this.productVariations[i].attributes.length; j++) {
          key = this.productVariations[i].attributes[j].name;

          // console.log(this.selectedOptions[key].toLowerCase()+ " " + this.productVariations[i].attributes[j].option.toLowerCase())

          if (
            this.selectedOptions[key].toLowerCase() ==
            this.productVariations[i].attributes[j].option.toLowerCase()
          ) {
            //Do nothing
          } else {
            // console.log(matchFailed)
            matchFailed = true;
            break;
          }
        }

        if (matchFailed) {
          continue;
        } else {
          //found the matching variation
          this.productPrice = this.productVariations[i].price;
          this.selectedVariation = this.productVariations[i];

          break;
        }
      }

      if (matchFailed == true) {
        this.toastCtrl
          .create({
            message: "No Such Product Found",
            duration: 3000,
          })
          .present()
          .then(() => {
            this.requireOptions = true;
          });
      }
    } else {
      this.productPrice = this.product.price;
    }

    loading.dismiss();
  }

  addToCart1(product, prID) {
    if (this.checkId == prID) {
      //counting selected attribute options
      let count = 0;
      for (let k in this.selectedOptions)
        if (this.selectedOptions.hasOwnProperty(k)) count++;

      //counting variation attributes options
      let count_ = 0;
      for (var index = 0; index < product.attributes.length; index++) {
        if (product.attributes[index].variation) count_++;
      }

      //checking if user selected all the variation options or not

      count_ = 0; // change
      count = 0; // change

      if (count_ != count || this.requireOptions) {
        // change
        this.toastCtrl
          .create({
            message: "Select Product Options",
            duration: 2000,
            showCloseButton: true,
          })
          .present();
        return;
      }

      this.storage.get("cart").then((data) => {
        if (data == undefined || data.length == 0) {
          data = [];

          data.push({
            product: product,
            qty: 1,
            amount: parseFloat(product.price),
          });

          if (this.selectedVariation) {
            data[0].variation = this.selectedVariation;
            data[0].amount = parseFloat(this.selectedVariation.price);
          }
        } else {
          let alreadyAdded = false;
          let alreadyAddedIndex = -1;

          for (let i = 0; i < data.length; i++) {
            if (data[i].product.id == product.id) {
              //Product ID matched
              if (this.productVariations.length > 0) {
                //Now match variation ID also if it exists
                if (data[i].variation.id == this.selectedVariation.id) {
                  alreadyAdded = true;
                  alreadyAddedIndex = i;
                  break;
                }
              } else {
                //product is simple product so variation does not  matter
                alreadyAdded = true;
                alreadyAddedIndex = i;
                break;
              }
            }
          }

          if (alreadyAdded == true) {
            if (this.selectedVariation) {
              data[alreadyAddedIndex].qty =
                parseFloat(data[alreadyAddedIndex].qty) + 1;
              data[alreadyAddedIndex].amount =
                parseFloat(data[alreadyAddedIndex].amount) +
                parseFloat(this.selectedVariation.price);
              data[alreadyAddedIndex].variation = this.selectedVariation;
            } else {
              data[alreadyAddedIndex].qty =
                parseFloat(data[alreadyAddedIndex].qty) + 1;
              data[alreadyAddedIndex].amount =
                parseFloat(data[alreadyAddedIndex].amount) +
                parseFloat(data[alreadyAddedIndex].product.price);
            }
          } else {
            if (this.selectedVariation) {
              data.push({
                product: product,
                qty: 1,
                amount: parseFloat(this.selectedVariation.price),
                variation: this.selectedVariation,
              });
            } else {
              data.push({
                product: product,
                qty: 1,
                amount: parseFloat(product.price),
              });
            }
          }
        }

        this.storage.set("cart", data).then(() => {
          console.log(data);

          this.events.publish("cartLength:created", data.length); // set cart length events

          this.toastCtrl
            .create({
              message: "Cart Updated",
              duration: 500,
            })
            .present();
          // this.navCtrl.setRoot(this.navCtrl.getActive().component);  // test
          this.cartLength = data.length;
          this.showEmptyCartMessage = false;
          this.getCartItem();
        });
      });
    } else {
      this.toastCtrl
        .create({
          message: "Select Product Options",
          duration: 2000,
          showCloseButton: true,
        })
        .present();
      return;
    }
  }

  remove(prdct){   
    this.similarProductList.splice(prdct,1);
    this.storage.set('itemWishList', this.similarProductList);
  }
 

}
