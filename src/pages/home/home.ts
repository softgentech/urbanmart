import { Component, ViewChild } from "@angular/core";
import {
  Events,
  IonicPage,
  LoadingController,
  NavController,
  Slides,
  ToastController,
} from "ionic-angular";
// import { ProductDetails } from '../product-details/product-details';

import * as WC from "woocommerce-api";
// import { SearchPage } from "../search/search";
import { WoocommerceProvider } from "../../providers/woocommerce/woocommerce";
import { Api } from "../../providers/api/api";
import { HttpClient } from "@angular/common/http";
import { Storage } from "@ionic/storage";
@IonicPage({})
@Component({
  selector: "page-home",
  templateUrl: "home.html",
})
export class HomePage {

  cartLength:any;
  CategoryListAll = [];

  categoryList: any;
  categoryList2: any;

  WooCommerce: any;
  products: any[];
  moreProducts: any[];
  page: number;
  searchQuery: string = "";

  loadCtrl: LoadingController;

  @ViewChild("productSlides") productSlides: Slides;

  constructor(
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    private WP: WoocommerceProvider,
    public api: Api,
    loadCtrl: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    public events: Events
  ) {

    events.subscribe('cartLength:created', val => {
      this.cartLength = val;
    });

    this.loadCtrl = loadCtrl;

    this.page = 2;

    this.WooCommerce = WP.init();
    console.log("TESTEST", this.WooCommerce);
    this.loadMoreProducts(null);

    let loading = this.loadCtrl.create({
      spinner: "dots",
      content: "Please wait...",
    });
    loading.present();

    this.WooCommerce.getAsync("products").then(
      (data) => {
      //  console.log('hello:',data);
        console.log("2", JSON.parse(data.body));
        this.products = JSON.parse(data.body).products;

        loading.dismiss();
      },
      (err) => {
        console.log(err);
      }
    );
  }

  // ionViewDidLoad(){
  //   setInterval(()=> {

  //     if(this.productSlides.getActiveIndex() == this.productSlides.length() -1)
  //       this.productSlides.slideTo(0);

  //     this.productSlides.slideNext();
  //   }, 3000)
  // }

  // ionViewWillEnter() {
  //   this.getAllCategoryList();
  // }
  ionViewDidLoad() {
    this.storage.get('cart').then((val) => {
      if(val == null){
        console.log("cart_null");
      }else{
        this.cartLength = val.length;
      }
  });
    this.getAllCategoryList();
  }

  getAllCategoryList() {
    this.http.get(this.api.getAllCatergoryApi).subscribe((response: any) => {   
      this.categoryList = response.data[0];
      this.categoryList2 = response.data[1];

      for (let i = 2; i < response.data.length; i++) {
        this.CategoryListAll.push(response.data[i]);       
      }
    });
  }

  loadMoreProducts(event) {
    if (event == null) {
      this.page = 2;
      this.moreProducts = [];
    } else this.page++;

    this.WooCommerce.getAsync("products?page=" + this.page).then(
      (data) => {
        console.log("1:", JSON.parse(data.body));
        this.moreProducts = this.moreProducts.concat(
          JSON.parse(data.body).products
        );

        if (event != null) {
          event.complete();
        }

        if (JSON.parse(data.body).products.length < 10) {
          //event.enable(false);

          this.toastCtrl
            .create({
              message: "No more products!",
              duration: 500,
            })
            .present();
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  openProductPage(product) {
   // console.log("testing:-",product);
    this.navCtrl.push("ProductDetails", { product: product });
  }

  onSearch(event) {
    console.log('Search:-',event);
    if (this.searchQuery.length > 0) {
      this.navCtrl.push("SearchPage", { searchQuery: this.searchQuery });
    }
  }
  searchPage(){
    this.storage.set('Session_BackSearch', 'home');
    this.navCtrl.setRoot(
      "SearchPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }

  // open subcategory
  Subcategory(categoryId) {   
    this.storage.set("Session_Category_Id", categoryId);

    this.navCtrl.setRoot(
      "SubCategoryPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
  razor(){
    this.navCtrl.setRoot(
      "RazorPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }

  // tab navigation
  categoryTab() {
    this.navCtrl.setRoot(
      "CategoryPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
  searchTab() {
    this.storage.set('Session_BackSearch', 'search');
    this.navCtrl.setRoot(
      "SearchPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
  myListTab() {
    this.navCtrl.setRoot(
      "MyListPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
  CartTab() {
    this.navCtrl.setRoot(
      "CartPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }
  // end tab navigation
  
}
