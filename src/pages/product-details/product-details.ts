import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  ModalController,
  LoadingController,
  Events,
} from "ionic-angular";
import * as WC from "woocommerce-api";
//import { Cart } from '../cart/cart';

import { Storage } from "@ionic/storage";
import { WoocommerceProvider } from "../../providers/woocommerce/woocommerce";
import { SocialSharing } from "@ionic-native/social-sharing";
import { HttpClient } from "@angular/common/http";
import { Api } from "../../providers";
@IonicPage({})
@Component({
  selector: "page-product-details",
  templateUrl: "product-details.html",
})
export class ProductDetails {
  pID: any;
  item: any[] = [];

  a:any;

  footerDiv = true;

  cartLength: any;
  permalink: any;

  regularPrice: any;
  sellPrice: any;

  segment: any;
  imgSrc: any;

  product: any;
  WooCommerce: any;
  reviews: any[] = [];
  selectedOptions: any = {};
  requireOptions: boolean = true;
  productVariations: any[] = [];
  productPrice: number = 0.0;
  selectedVariation: any;

  similarProductList: any;
  checkId: any;
  wishList: any;

  constructor(
    private socialSharing: SocialSharing,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    private WP: WoocommerceProvider,
    private loadingCtrl: LoadingController,
    public events: Events,
    public http: HttpClient,
    public apiPvdr: Api
  ) {
    this.product = this.navParams.get("product");
    this.similarProduct(this.product.id);

    this.permalink = this.product.permalink;
    this.imgSrc = this.product.images[0].src;

    this.WooCommerce = WP.init(true);

    this.WooCommerce.getAsync("products/" + this.product.id + "/reviews").then(
      (data) => {
        this.reviews = JSON.parse(data.body);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  imageShow(srcLink) {
    this.imgSrc = srcLink;
  }

  ionViewWillEnter() {
   //this.storage.set("itemWishList",null);
    this.storage.get("itemWishList").then((res) => {     

      // for(let i=0; i<res.length; i++){
      //   console.log('ii',res[i].id);
      // }

      if (res) {
        this.item = res;
        console.log("itemWishList:1", this.item);
      }
    });

    this.storage.get("cart").then((val) => {
      if (val == null) {
        console.log("cart_null");
      } else {
        this.cartLength = val.length;
      }
    });
  }

  addToCart(product) {
    //counting selected attribute options
    let count = 0;
    for (let k in this.selectedOptions)
      if (this.selectedOptions.hasOwnProperty(k)) count++;

    //counting variation attributes options
    let count_ = 0;
    for (var index = 0; index < this.product.attributes.length; index++) {
      if (this.product.attributes[index].variation) count_++;
    }

    count_ = 0; // change
    count = 0; // change
    //checking if user selected all the variation options or not

    if (count_ != count || this.requireOptions) {
      this.toastCtrl
        .create({
          message: "Select Product Options",
          duration: 2000,
          showCloseButton: true,
        })
        .present();
      return;
    }

    this.storage.get("cart").then((data) => {
      if (data == undefined || data.length == 0) {
        data = [];

        data.push({
          product: product,
          qty: 1,
          amount: parseFloat(product.price),
        });

        if (this.selectedVariation) {
          data[0].variation = this.selectedVariation;
          data[0].amount = parseFloat(this.selectedVariation.price);
        }
      } else {
        let alreadyAdded = false;
        let alreadyAddedIndex = -1;

        for (let i = 0; i < data.length; i++) {
          if (data[i].product.id == product.id) {
            //Product ID matched
            if (this.productVariations.length > 0) {
              //Now match variation ID also if it exists
              if (data[i].variation.id == this.selectedVariation.id) {
                alreadyAdded = true;
                alreadyAddedIndex = i;
                break;
              }
            } else {
              //product is simple product so variation does not  matter
              alreadyAdded = true;
              alreadyAddedIndex = i;
              break;
            }
          }
        }

        if (alreadyAdded == true) {
          if (this.selectedVariation) {
            data[alreadyAddedIndex].qty =
              parseFloat(data[alreadyAddedIndex].qty) + 1;
            data[alreadyAddedIndex].amount =
              parseFloat(data[alreadyAddedIndex].amount) +
              parseFloat(this.selectedVariation.price);
            data[alreadyAddedIndex].variation = this.selectedVariation;
          } else {
            data[alreadyAddedIndex].qty =
              parseFloat(data[alreadyAddedIndex].qty) + 1;
            data[alreadyAddedIndex].amount =
              parseFloat(data[alreadyAddedIndex].amount) +
              parseFloat(data[alreadyAddedIndex].product.price);
          }
        } else {
          if (this.selectedVariation) {
            data.push({
              product: product,
              qty: 1,
              amount: parseFloat(this.selectedVariation.price),
              variation: this.selectedVariation,
            });
          } else {
            data.push({
              product: product,
              qty: 1,
              amount: parseFloat(product.price),
            });
          }
        }
      }

      this.storage.set("cart", data).then(() => {
        console.log(data);
        this.cartLength = data.length;
        this.events.publish("cartLength:created", data.length); // set cart length events

        this.toastCtrl
          .create({
            message: "Cart Updated",
            duration: 500,
          })
          .present();
      });
    });
  }

  openCart() {
    //this.modalCtrl.create(Cart).present();
    this.navCtrl.push("CartPage");
    //this.navCtrl.push('my-page');
  }

  async check(justSelectedAttribute) {
    let loading = this.loadingCtrl.create({
      content: "Getting Product Variations",
    });

    //counting selected attribute options
    let count = 0;
    for (let k in this.selectedOptions)
      if (this.selectedOptions.hasOwnProperty(k)) count++;

    let count_ = 0;
    for (var index = 0; index < this.product.attributes.length; index++) {
      if (this.product.attributes[index].variation) count_++;
    }

    count_ = 0; // change
    count = 0; // change
    //checking if user selected all the variation options or not

    if (count_ != count) {
      this.requireOptions = true;
      return;
    } else {
      this.requireOptions = false;

      //Get product variations only once when all product variables are selected by the user
      loading.present();
      this.productVariations = JSON.parse(
        (
          await this.WooCommerce.getAsync(
            "products/" + this.product.id + "/variations/"
          )
        ).body
      );
    }

    let i = 0,
      matchFailed = false;

    if (this.productVariations.length > 0) {
      for (i = 0; i < this.productVariations.length; i++) {
        matchFailed = false;
        let key: string = "";

        for (let j = 0; j < this.productVariations[i].attributes.length; j++) {
          key = this.productVariations[i].attributes[j].name;

          console.log(
            "TEST",
            this.selectedOptions[key].toLowerCase() +
              " " +
              this.productVariations[i].attributes[j].option.toLowerCase()
          );

          if (
            this.selectedOptions[key].toLowerCase() ==
            this.productVariations[i].attributes[j].option.toLowerCase()
          ) {
            //Do nothing
          } else {
            matchFailed = true;
            break;
          }
        }

        if (matchFailed) {
          continue;
        } else {
          //found the matching variation
          this.productPrice = this.productVariations[i].price;
          this.selectedVariation = this.productVariations[i];
          this.regularPrice = this.productVariations[i].regular_price; // regular price
          this.sellPrice = this.productVariations[i].sale_price; // sell price
          this.footerDiv = true; // test

          break;
        }
      }

      if (matchFailed == true) {
        this.toastCtrl
          .create({
            message: "No Such Product Found",
            duration: 3000,
          })
          .present()
          .then(() => {
            this.requireOptions = true;
          });
      }
    } else {
      this.productPrice = this.product.price;
    }

    loading.dismiss();
  }

  share() {
    this.socialSharing.share(this.permalink);
  }

  searchPage() {
    this.storage.set("Session_BackSearch", "product-details");
    this.navCtrl.setRoot(
      "SearchPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }

  // similar product
  similarProduct(prID) {
    let postData = {
      proid: prID,
    };

    this.http
      .post(this.apiPvdr.SimilarProductApi, postData)

      .subscribe(
        (data: any) => {
          console.log('similar:',data);
          if (data.code == 200) {
            this.similarProductList = data.message.related;
          } else {
            // do nothing
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

  // testing
  async check1(justSelectedAttribute, prdct, prID) {
    this.footerDiv = false; // test
    this.checkId = prID;
    let loading = this.loadingCtrl.create({
      content: "Getting Product Variations",
    });

    //counting selected attribute options
    let count = 0;
    for (let k in this.selectedOptions)
      if (this.selectedOptions.hasOwnProperty(k)) count++;

    let count_ = 0;
    for (var index = 0; index < prdct.attributes.length; index++) {
      if (prdct.attributes[index].variation) count_++;
    }

    count = 0; // change
    count_ = 0; // change

    //checking if user selected all the variation options or not

    if (count_ != count) {
      // this.navCtrl.setRoot(this.navCtrl.getActive().component);  // test
      this.requireOptions = true;

      return;
    } else {
      // this.selectedOptions = justSelectedAttribute  // change

      this.requireOptions = false;
      //Get product variations only once when all product variables are selected by the user
      loading.present();
      this.productVariations = JSON.parse(
        (
          await this.WooCommerce.getAsync(
            "products/" + prdct.id + "/variations/"
          )
        ).body
      );
      // console.log('productVariations:-',this.productVariations)
    }
    let i = 0,
      matchFailed = false;
    if (this.productVariations.length > 0) {
      for (i = 0; i < this.productVariations.length; i++) {
        matchFailed = false;
        let key: string = "";

        for (let j = 0; j < this.productVariations[i].attributes.length; j++) {
          key = this.productVariations[i].attributes[j].name;

          // console.log(this.selectedOptions[key].toLowerCase()+ " " + this.productVariations[i].attributes[j].option.toLowerCase())

          if (
            this.selectedOptions[key].toLowerCase() ==
            this.productVariations[i].attributes[j].option.toLowerCase()
          ) {
            //Do nothing
          } else {
            // console.log(matchFailed)
            matchFailed = true;
            break;
          }
        }

        if (matchFailed) {
          continue;
        } else {
          //found the matching variation
          this.productPrice = this.productVariations[i].price;
          this.selectedVariation = this.productVariations[i];

          break;
        }
      }

      if (matchFailed == true) {
        this.toastCtrl
          .create({
            message: "No Such Product Found",
            duration: 3000,
          })
          .present()
          .then(() => {
            this.requireOptions = true;
          });
      }
    } else {
      this.productPrice = this.product.price;
    }

    loading.dismiss();
  }

  addToCart1(product, prID) {
    if (this.checkId == prID) {
      //counting selected attribute options
      let count = 0;
      for (let k in this.selectedOptions)
        if (this.selectedOptions.hasOwnProperty(k)) count++;

      //counting variation attributes options
      let count_ = 0;
      for (var index = 0; index < product.attributes.length; index++) {
        if (product.attributes[index].variation) count_++;
      }

      //checking if user selected all the variation options or not

      count_ = 0; // change
      count = 0; // change

      if (count_ != count || this.requireOptions) {
        // change
        this.toastCtrl
          .create({
            message: "Select Product Options",
            duration: 2000,
            showCloseButton: true,
          })
          .present();
        return;
      }

      this.storage.get("cart").then((data) => {
        if (data == undefined || data.length == 0) {
          data = [];

          data.push({
            product: product,
            qty: 1,
            amount: parseFloat(product.price),
          });

          if (this.selectedVariation) {
            data[0].variation = this.selectedVariation;
            data[0].amount = parseFloat(this.selectedVariation.price);
          }
        } else {
          let alreadyAdded = false;
          let alreadyAddedIndex = -1;

          for (let i = 0; i < data.length; i++) {
            if (data[i].product.id == product.id) {
              //Product ID matched
              if (this.productVariations.length > 0) {
                //Now match variation ID also if it exists
                if (data[i].variation.id == this.selectedVariation.id) {
                  alreadyAdded = true;
                  alreadyAddedIndex = i;
                  break;
                }
              } else {
                //product is simple product so variation does not  matter
                alreadyAdded = true;
                alreadyAddedIndex = i;
                break;
              }
            }
          }

          if (alreadyAdded == true) {
            if (this.selectedVariation) {
              data[alreadyAddedIndex].qty =
                parseFloat(data[alreadyAddedIndex].qty) + 1;
              data[alreadyAddedIndex].amount =
                parseFloat(data[alreadyAddedIndex].amount) +
                parseFloat(this.selectedVariation.price);
              data[alreadyAddedIndex].variation = this.selectedVariation;
            } else {
              data[alreadyAddedIndex].qty =
                parseFloat(data[alreadyAddedIndex].qty) + 1;
              data[alreadyAddedIndex].amount =
                parseFloat(data[alreadyAddedIndex].amount) +
                parseFloat(data[alreadyAddedIndex].product.price);
            }
          } else {
            if (this.selectedVariation) {
              data.push({
                product: product,
                qty: 1,
                amount: parseFloat(this.selectedVariation.price),
                variation: this.selectedVariation,
              });
            } else {
              data.push({
                product: product,
                qty: 1,
                amount: parseFloat(product.price),
              });
            }
          }
        }

        this.storage.set("cart", data).then(() => {
          console.log(data);

          this.events.publish("cartLength:created", data.length); // set cart length events

          this.toastCtrl
            .create({
              message: "Cart Updated",
              duration: 500,
            })
            .present();
          // this.navCtrl.setRoot(this.navCtrl.getActive().component);  // test
          this.cartLength = data.length;
        });
      });
    } else {
      this.toastCtrl
        .create({
          message: "Select Product Options",
          duration: 2000,
          showCloseButton: true,
        })
        .present();
      return;
    }
  }

  openProductPage(product) {
    this.navCtrl.push("ProductDetails", { product: product });
  }

  WishList(product) {
    // this.item.push(product);
    // // Saving item array instead of res value.
    // this.storage.set("itemWishList", this.item).then(
    //   () => {
    //     console.log("Item Stored");
    //   },
    //   (error) => console.error("Error storing item", error)
    // );

    this.storage.get("itemWishList").then((res) => {
        if(res == null){
           this.item.push(product);  
    this.storage.set("itemWishList", this.item).then(
      () => {
        console.log("Item Stored---111");
      },
      (error) => console.error("Error storing item", error)
    );
        }else if (product.id) {
            for(let i=0; i<res.length; i++){
              console.log('ii',res[i].id);
              if(res[i].id == product.id){   
                  alert("Product already added in wishlist"); 
                  this.a=0;
              }else{
                this.a = 1;                           
              }
            }   
            if(this.a == 1){  
               this.item.push(product);  
                this.storage.set("itemWishList", this.item).then(
                  () => {
                    console.log("Item Stored--022");
                  },
                  (error) => console.error("Error storing item", error)
                );
            }
            console.log('LoopEnd')  
      }else{
       
      }
    });

  }
}
