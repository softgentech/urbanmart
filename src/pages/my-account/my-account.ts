import { Component } from '@angular/core';
import { Events, IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { Api } from '../../providers';

@IonicPage()
@Component({
  selector: 'page-my-account',
  templateUrl: 'my-account.html',
})
export class MyAccountPage {

  userID:any;
  userData:any;

  Default_Address:any;
  cartLength: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage,
              private http: HttpClient, public apiPvdr: Api, public loadingCtrl: LoadingController,
              public events: Events) {
  }

  ionViewDidLoad() {
   
  }

  ionViewWillEnter(){

    this.storage.get("cart").then((val) => {
      if (val == null) {
        console.log("cart_null");
      } else {
        this.cartLength = val.length;
      }
    });

    this.storage.get('Session_login').then((val) => {         
      this.userID = val.ID;  
    //  console.log('UserID:', this.userID);   
      
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });    
      loading.present();
      
      // get user data details api
      this.http.get(this.apiPvdr.userProfileAPI + this.userID).subscribe((response) => {
        loading.dismiss(); 
        this.userData = response;
      //  console.log('User_data:', this.userData);
});

      // get default address
      this.http.get(this.apiPvdr.getAllAddressApi + this.userID).subscribe((response:any) => {       
        if(response.code == 200){         
            for(let i=0; i<response.data.length; i ++){              
              if(response.data[i].defaultAddress == 1){
                this.Default_Address= response.data[i];
                console.log('Default_Address:',this.Default_Address);
              }
            }
        }else{
         // do nothing
        }
        
  });

    });
  }

  close() {
    this.navCtrl.setRoot('HomePage', {}, {
      animate: true,
      direction: 'forward'
    });
  }
  MyOrder(){
    this.navCtrl.setRoot('MyOrderPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }
  myAddress(){
    this.storage.set('Session_chooseDelPage', 'myAccount');
    this.navCtrl.setRoot('ChooseDelAddressPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }
  updateProfile(){
    this.navCtrl.setRoot('UpdateProfilePage', {}, {
      animate: true,
      direction: 'forward'
    });
  }

  // tab navigation
  homeTab(){
    this.navCtrl.setRoot('HomePage', {}, {
      animate: true,
      direction: 'forward'
    });
  }
  searchTab(){
    this.navCtrl.setRoot('SearchPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }
  myListTab(){
    this.navCtrl.setRoot('MyListPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }
  CartTab(){
    this.navCtrl.setRoot('CartPage', {}, {
      animate: true,
      direction: 'forward'
    });
  } 
  CategoryTab(){
    this.navCtrl.setRoot('CategoryPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }
  // end tab navigation

  logout(){
    this.storage.set('Session_login', null);  // session login user data null

    this.events.publish('user:created', null);  // events

    this.navCtrl.setRoot('LoginPage', {}, {
      animate: true,
      direction: 'forward'
    });

  }

  changeAddress(){

    this.storage.set('Session_chooseDelPage', 'myAccount'); // set the session

    this.navCtrl.setRoot('ChooseDelAddressPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }

}
