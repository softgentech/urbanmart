import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { Api } from '../../providers';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@IonicPage()
@Component({
  selector: 'page-update-profile',
  templateUrl: 'update-profile.html',
})
export class UpdateProfilePage {

  ionicForm: FormGroup;
  isSubmitted = false;
  
  public myDate;

  userID:any;
  userData:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private storage: Storage,
              private http: HttpClient, public apiPvdr: Api, 
              public loadingCtrl: LoadingController, public formBuilder: FormBuilder) {

                this.ionicForm = this.formBuilder.group({      
                  firstname: [''],
                  lastname:  ['']   
                });
  }

     // validaion login
 get errorControl() {
  return this.ionicForm.controls;
}
submitForm() {
  this.isSubmitted = true;
  if (!this.ionicForm.valid) {
    console.log('Please provide all the required values!');
    return false;
  } else  {
    //console.log(this.ionicForm.value);   
    this.UpdateProfile(this.ionicForm.value);   
   }
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateProfilePage');
  }
  back() {
    this.navCtrl.setRoot('MyAccountPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }

  ionViewWillEnter(){
    this.storage.get('Session_login').then((val) => {    
     // console.log(val); 
      this.userID = val.ID;  
     console.log('UserID:', this.userID);   
      
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });    
      loading.present();
      
      this.http.get(this.apiPvdr.userProfileAPI + this.userID).subscribe((response) => {
        loading.dismiss(); 
        this.userData = response;
        console.log('User_data:', this.userData);
});

    });
  }

  showdate(){
    console.log('Date:',this.myDate);
  }

  UpdateProfile(value) { 
    let postData = {
            userid: this.userID,
            firstname: value.firstname,
            lastname: value.lastname,            
            dob: this.myDate         
    }
    console.log('body:',postData)

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });    
    loading.present();   
    this.http.post(this.apiPvdr.updateProfileAPI, postData)

      .subscribe((data: any) => {    
        loading.dismiss(); 
        alert(data.message);           
                
       }, error => {
        console.log(error);
      });
  }

}
