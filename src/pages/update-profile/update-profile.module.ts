import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicPageModule } from 'ionic-angular';
import { UpdateProfilePage } from './update-profile';

@NgModule({
  declarations: [
    UpdateProfilePage,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    IonicPageModule.forChild(UpdateProfilePage),
  ],
})
export class UpdateProfilePageModule {}
