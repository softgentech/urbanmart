import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicPageModule } from 'ionic-angular';
import { AddAddressPage } from './add-address';

@NgModule({
  declarations: [
    AddAddressPage,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    IonicPageModule.forChild(AddAddressPage),
  ],
})
export class AddAddressPageModule {}
