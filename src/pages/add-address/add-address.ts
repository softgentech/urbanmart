import { Component } from "@angular/core";
import {
  AlertController,
  IonicPage,
  LoadingController,
  NavController,
  NavParams,
} from "ionic-angular";
import { Storage } from "@ionic/storage";
import { HttpClient } from "@angular/common/http";
import { Api } from "../../providers/api/api";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
/**
 * Generated class for the AddAddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-add-address",
  templateUrl: "add-address.html",
})
export class AddAddressPage {
  ionicForm: FormGroup;
  isSubmitted = false;

  defaultAddress = false;
  userID: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public formBuilder: FormBuilder,
    public storage: Storage,
    public alertCtrl: AlertController,
    public http: HttpClient,
    public apiPvdr: Api
  ) {
    this.ionicForm = this.formBuilder.group({
      houseno: ["", [Validators.required]],
      apartmentname: ["", [Validators.required]],
      streetDetails: ["", [Validators.required]],
      landmark: ["", [Validators.required]],
      area: ["", [Validators.required]],
      city: ["", [Validators.required]],
      pincode: ["", [Validators.required]],
      nickname: ["", [Validators.required]],
    });
  }

  // validaion Address
  get errorControl() {
    return this.ionicForm.controls;
  }
  submitForm() {
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log("Please provide all the required values!");
      return false;
    } else {
      // console.log('FormValue:',this.ionicForm.value);
      this.addAddress(this.ionicForm.value);
    }
  }

  ionViewWillEnter() {
    this.storage.get("Session_login").then((val) => {
      this.userID = val.ID;
      console.log('UserID:', this.userID);
    });
  }

  back() {
    this.navCtrl.setRoot(
      "ChooseDelAddressPage",
      {},
      {
        animate: true,
        direction: "forward",
      }
    );
  }

  // add address
  addAddress(value) {
    let postData = {
      user_id: this.userID,
      houseno: value.houseno,
      apartmentname: value.apartmentname,
      streetDetails: value.streetDetails,
      landmark: value.landmark,
      area: value.area,
      city: value.city,
      pincode: value.pincode,
      nickname: value.nickname,
      defaultAddress: this.defaultAddress,
    };
    console.log("body:", postData);

    let loading = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loading.present();
    this.http
      .post(this.apiPvdr.addAddressApi, postData)

      .subscribe(
        (data: any) => {
          loading.dismiss();
           console.log('Data:', data);
          if (data.code == 200) {
          //  alert(data.message);
            this.navCtrl.setRoot(
              "ChooseDelAddressPage",
              {},
              {
                animate: true,
                direction: "forward",
              }
            );
          } else {           
            alert("Something went wrong! Please try again later.");
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }
}
