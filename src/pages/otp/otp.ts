import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { Api } from '../../providers';
import { HttpClient } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
})
export class OtpPage {

  sessionMobile = '';

  resendDiv = false;

  ionicForm: FormGroup;
  isSubmitted = false;

  timeInSeconds: any;
  time: any;
  runTimer: boolean;
  hasStarted: boolean;
  hasFinished: boolean;
  remainingTime: any;
  displayTime: string;

  constructor(public loadingCtrl: LoadingController, public httpClient: HttpClient,
    public apiPvdr: Api, private storage: Storage, public navCtrl: NavController,
     public navParams: NavParams, public formBuilder: FormBuilder) {

    this.ionicForm = this.formBuilder.group({      
      otp: ['', [Validators.required]]     
    });
  
  }

  ionViewWillEnter(){
    // get session mobile number
    this.storage.get('session_mobile').then((val) => {      
      this.sessionMobile = val;
      console.log('Mobile:', this.sessionMobile);       
    });
      }

  // otp validation
  get errorControl() {
    return this.ionicForm.controls;
  }
  submitForm() {
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    } else  {
     // console.log(this.ionicForm.value.otp); 
      this.verifyOtp(this.ionicForm.value.otp);    
     }
  }
  // end otp validation

  ionViewDidLoad() {
  
    
   
    this.initTimer();
    this. startTimer();
  }

  backIcon(){
    this.navCtrl.setRoot('SignupPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }
  signup(){
    this.navCtrl.setRoot('SignupDetailsPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }
  change(){
    this.navCtrl.setRoot('SignupPage', {}, {
      animate: true,
      direction: 'forward'
    });
  }

  //timer
  initTimer() {
    if (!this.timeInSeconds) {
      this.timeInSeconds = 59;
    }
   
    this.time = this.timeInSeconds;
    this.runTimer = false;
    this.hasStarted = false;
    this.hasFinished = false;
    this.remainingTime = this.timeInSeconds;
    this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
   }
   
   startTimer() {
     this.runTimer = true;
     this.hasStarted = true;
     this.timerTick();
   }

   timerTick() {
    setTimeout(() => {
   
      if (!this.runTimer) { return; }
      this.remainingTime--;
      this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
      if (this.remainingTime > 0) {
        this.timerTick();
      } else {
        this.hasFinished = true;
       // console.log('timeup');
   
          this.resendDiv = true;
      }
    }, 1000);
   }

   getSecondsAsDigitalClock(inputSeconds: number) {
    let sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
    const hours = Math.floor(sec_num / 3600);
    const minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    const seconds = sec_num - (hours * 3600) - (minutes * 60);
   // let hoursString = '';
    let minutesString = '';
    let secondsString = '';
   // hoursString = (hours < 10) ? '0' + hours : hours.toString();
    minutesString = (minutes < 10) ? '0' + minutes : minutes.toString();
    secondsString = (seconds < 10) ? '0' + seconds : seconds.toString();
   // return hoursString + ':' + minutesString + ':' + secondsString;
    return  minutesString + ':' + secondsString;
   
   }
  //end timer

  resendOtp(){
    this.resendDiv=false;
    this.initTimer();
    this. startTimer();
    this.ResendOtpAgain();
  }

  verifyOtp(otp){   
    let postData = {  
      mobile : this.sessionMobile,
      otp : otp
    }
   // console.log('body:',postData);
   let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  
  loading.present();

    this.httpClient.post(this.apiPvdr.otpVerifyApi, postData)

.subscribe((data:any) => {  
 // console.log('data:', data); 
 loading.dismiss();
  if(data.code == 200){  

   // alert(data.message);
     this.navCtrl.setRoot('SignupDetailsPage', {}, {
      animate: true,
      direction: 'forward'
    });

  }  else if(data.code == 201){
   
    alert(data.message);
   

  }      
 }, error => {
  console.log('Error',error);
});
}

ResendOtpAgain(){ 

  let postData = {  
   mobile : this.sessionMobile
 }
// console.log('body:',postData)
let loading = this.loadingCtrl.create({
  content: 'Please wait...'
});

loading.present();

 this.httpClient.post(this.apiPvdr.otpResendApi, postData)
 
 .subscribe((data:any) => {   
  loading.dismiss();
  console.log('resendOTPdata:', data);

 // alert(data.message);
       
  }, error => {
   console.log('Error',error);
 });
 }

}
