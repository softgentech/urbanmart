import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RazorPage } from './razor';

@NgModule({
  declarations: [
    RazorPage,
  ],
  imports: [
    IonicPageModule.forChild(RazorPage),
  ],
})
export class RazorPageModule {}
