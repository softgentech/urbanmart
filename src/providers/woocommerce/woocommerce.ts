import { Injectable } from '@angular/core';
import * as WC from 'woocommerce-api';


@Injectable()
export class WoocommerceProvider {

  Woocommerce: any;
  WoocommerceV2: any;
  WoocommerceV3: any;

  constructor() {
    this.Woocommerce = WC({
      url: "https://urbanmart.online/",
      // url: "https://aba-india.org/urbanMart/",
      // consumerKey: "ck_07895ecd64dfdfbc650f61112d4ca9e520b0913c",
      // consumerSecret: "cs_1d75fafd3f41764deefc2176e10cca889b04857e"
      consumerKey: "ck_763b990c99348a7479ec50a6d854f577197e5b7f",
      consumerSecret: "cs_de8ebc649cc8b29325df87253a2befa823998db2"
    });

    this.WoocommerceV2 = WC({
      url: "https://urbanmart.online/",
      consumerKey: "ck_763b990c99348a7479ec50a6d854f577197e5b7f",
      consumerSecret: "cs_de8ebc649cc8b29325df87253a2befa823998db2",
      wpAPI: true,
      version: "wc/v2"
    });

    this.WoocommerceV3 = WC({
      url: "https://urbanmart.online/",
      consumerKey: "ck_763b990c99348a7479ec50a6d854f577197e5b7f",
      consumerSecret: "cs_de8ebc649cc8b29325df87253a2befa823998db2",
      wpAPI: true,
      version: "wc/v3"
    });
  }

  init(v2?: boolean){
    if(v2 == true){
      return this.WoocommerceV2;
    } else {
      return this.Woocommerce;
    }
  }

}
