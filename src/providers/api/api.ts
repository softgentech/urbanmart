import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {

  otpVerifyApi:string = "https://urbanmart.online/wp-json/wp/v2/users/otpmatching";
  otpApi: string = "https://urbanmart.online/wp-json/wp/v2/users/otpverify";
  signupApi: string = "https://urbanmart.online/wp-json/wp/v2/users/signup";
  otpResendApi: string = "https://urbanmart.online/wp-json/wp/v2/users/otpresend";
  loginApi: string = "https://urbanmart.online/wp-json/wp/v2/users/login";
  userProfileAPI: string = "https://urbanmart.online/wp-json/wp/v2/users/profile?userid=";
  updateProfileAPI: string = "https://urbanmart.online/wp-json/wp/v2/users/update";
  addAddressApi: string = "https://urbanmart.online/wp-json/wp/v2/users/addcustomeraddress";
  getAllAddressApi: string = "https://urbanmart.online/wp-json/wp/v2/users/getcustomeraddress?userid=";
  deleteAddressApi: string = "https://urbanmart.online/wp-json/wp/v2/users/deletecustomeraddress?userid=";
  defaultAddressApi: string = "https://urbanmart.online/wp-json/wp/v2/users/defaultcustomeraddress?userid=";
  getAllCatergoryApi: string = "https://urbanmart.online/wp-json/wp/v2/users/productcategory";
  Sub_CategoryApi: string = "https://urbanmart.online/wp-json/wp/v2/users/productsubcategory";
  Product_List_Api: string = "https://urbanmart.online/wp-json/wp/v2/users/productsbysubcategory";
  GetAllMixProductList: string = "https://urbanmart.online/wp-json/wp/v2/users/products";
  GetAllOrderAPI: string = "https://urbanmart.online/wp-json/wp/v2/users/myorders";
  OrderDetailsApi: string = "https://urbanmart.online/wp-json/wp/v2/users/orderdetails";
  CancelOrderApi: string = "https://urbanmart.online/wp-json/wp/v2/users/cancelorder";
  SimilarProductApi: string = "https://urbanmart.online/wp-json/wp/v2/users/relatedproduct";
  

  url: string = 'https://urbanmart.online';

  constructor(public http: HttpClient) {
  }

  get(endpoint: string, params?: any, reqOpts?: any) {
    if (!reqOpts) {
      reqOpts = {
        params: new HttpParams()
      };
    }

    // Support easy query params for GET requests
    if (params) {
      reqOpts.params = new HttpParams();
      for (let k in params) {
        reqOpts.params = reqOpts.params.set(k, params[k]);
      }
    }

    return this.http.get(this.url + '/' + endpoint, reqOpts);
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    return this.http.post(this.url + '/' + endpoint, body, reqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.url + '/' + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http.delete(this.url + '/' + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return this.http.patch(this.url + '/' + endpoint, body, reqOpts);
  }
}
