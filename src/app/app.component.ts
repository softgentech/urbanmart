import { Component, ViewChild } from "@angular/core";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { TranslateService } from "@ngx-translate/core";
import { AlertController, App, Config, Events, Nav, Platform } from "ionic-angular";
import { OneSignal } from "@ionic-native/onesignal";
import { FirstRunPage } from "../pages";
import { Settings } from "../providers";
import { Storage } from "@ionic/storage";

@Component({
  // template: `<ion-menu [content]="content" type="overlay">
  //   <ion-header>
  //     <ion-toolbar>
  //       <ion-title>URBAN MART</ion-title>
  //     </ion-toolbar>
  //   </ion-header>

  //   <ion-content>
  //     <ion-list>
  //       <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">
  //         {{p.title}}
  //       </button>
  //     </ion-list>
  //   </ion-content>

  // </ion-menu>
  // <ion-nav #content [root]="rootPage"></ion-nav>`

  template: `<ion-menu [content]="content" type="overlay">
      <ion-header>
        <img src="/assets/icon/logo3.png" style="margin-top: 0px;" />
      </ion-header>

      <ion-content>
        <ion-list class="menuBtn" *ngIf="logoutMenu == true">
          <button
            menuClose
            ion-item
            *ngFor="let p of pages"
            (click)="openPage(p)"
          >
            {{ p.title }}
          </button>
        </ion-list>
        <ion-list class="menuBtn" *ngIf="loginMenu == true">
          <button
            menuClose
            ion-item
            *ngFor="let p of pages2"
            (click)="openPage(p)"
          >
            {{ p.title }}
          </button>
        </ion-list>
      </ion-content>
    </ion-menu>
    <ion-nav #content [root]="rootPage"></ion-nav>`,
})
export class MyApp {
  loginMenu = false;
  logoutMenu = false;

  rootPage = FirstRunPage;
  loginState = false;
  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    // { title: 'Tutorial', component: 'TutorialPage' },
    // { title: 'Welcome', component: 'WelcomePage' },
    // { title: 'Tabs', component: 'TabsPage' },
    // { title: 'Cards', component: 'CardsPage' },
    // { title: 'Content', component: 'ContentPage' },
    { title: "Home", component: "HomePage" },
    // { title: 'My Account', component: 'MyAccountPage' },
    // { title: 'Cart', component: 'CartPage' },
    // { title: 'My List', component: 'MyListPage' },
    // { title: 'Search Product', component: 'SearchPage' },
    // { title: 'My Order', component: 'MyOrderPage' },
    { title: "Login", component: "LoginPage" },
    { title: "Signup", component: "SignupPage" },
    // { title: 'Master Detail', component: 'ListMasterPage' },
    // { title: 'Menu', component: 'MenuPage' },
    // { title: 'Settings', component: 'SettingsPage' },
    // { title: 'Search', component: 'SearchPage' }
  ];

  pages2: any[] = [
    { title: "Home", component: "HomePage" },
    { title: "My Account", component: "MyAccountPage" },
    { title: "Cart", component: "CartPage" },
    { title: "My List", component: "MyListPage" },
    { title: "Search Product", component: "SearchPage" },
   // { title: "My Order", component: "MyOrderPage" },
  ];

  constructor(
    public events: Events,
    public storage: Storage,
    private alertCtrl: AlertController,
    private oneSignal: OneSignal,
    private translate: TranslateService,
    private platform: Platform,
    settings: Settings,
    private config: Config,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    public  app: App
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleLightContent();
      this.splashScreen.hide();

      // this.oneSignalNotificatoin(); //call one signal

      this.storage.set('Session_subCatId', null);  // category_null
      this.storage.set('Session_Category_Id', null); // subcategory_null

      this.storage.get("Session_login").then((val) => {
        // console.log('session_user:',val);
        if (val != null) {
          console.log("00");
          this.logoutMenu = false;
          this.loginMenu = true;
        } else {
          console.log("0022");
          this.loginMenu = false;
          this.logoutMenu = true;
        }
      });

      events.subscribe("user:created", (data) => {
        // console.log('Events Data:', data);

        if (data != null) {
          console.log("1");
          this.logoutMenu = false;
          this.loginMenu = true;
        } else {
          this.loginMenu = false;
          this.logoutMenu = true;
        }
      });

      // one signal integration
      if (this.platform.is("cordova")) {
        this.setupPush();
      }
      // end one signal

      this.hardwareBackButtonExit();

    });
    this.initTranslate();
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang("en");
    const browserLang = this.translate.getBrowserLang();

    if (browserLang) {
      if (browserLang === "zh") {
        const browserCultureLang = this.translate.getBrowserCultureLang();

        if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
          this.translate.use("zh-cmn-Hans");
        } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
          this.translate.use("zh-cmn-Hant");
        }
      } else {
        this.translate.use(this.translate.getBrowserLang());
      }
    } else {
      this.translate.use("en"); // Set your language here
    }

    this.translate.get(["BACK_BUTTON_TEXT"]).subscribe((values) => {
      this.config.set("ios", "backButtonText", values.BACK_BUTTON_TEXT);
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  // one signal
  //    oneSignalNotificatoin(){
  //     this.oneSignal.startInit('1b24fc37-1981-473b-a06b-124083889351', '953935720924');

  // this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

  // this.oneSignal.handleNotificationReceived().subscribe(() => {
  //  // do something when notification is received
  //  alert('success');
  // });

  // this.oneSignal.handleNotificationOpened().subscribe(() => {
  //   // do something when a notification is opened
  //   alert('Fail');
  // });

  // this.oneSignal.endInit();

  // this.oneSignal.getIds().then((data) => {
  //   console.log('playerID:',data.userId);
  //  // this.storage.set('session_uid', data.userId);
  // });

  //   }

  // testing   one signal
  setupPush() {
    this.oneSignal.startInit(
      "1b24fc37-1981-473b-a06b-124083889351",
      "953935720924"
    );

    this.oneSignal.inFocusDisplaying(
      this.oneSignal.OSInFocusDisplayOption.None
    );

    this.oneSignal.handleNotificationReceived().subscribe((data) => {
      let msg = data.payload.body;
      let title = data.payload.title;
      let additionalData = data.payload.additionalData;
      this.showAlert(title, msg, additionalData.task);
    });

    this.oneSignal.handleNotificationOpened().subscribe((data) => {
      let additionalData = data.notification.payload.additionalData;

      this.showAlert(
        "Notification opened",
        "You already read this before",
        additionalData.task
      );
    });
    this.oneSignal.endInit();

    this.oneSignal.getIds().then((data) => {
      console.log("playerID:", data.userId);
      this.storage.set("session_uid", data.userId);
    });
  }
  async showAlert(title, msg, task) {
    const alert = await this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: [
        {
          // text: `Action: ${task}`,
          role: task,
          handler: () => {
            // navigate to specific screen
          },
        },
      ],
    });

    await alert.present();
  }
  // end one signal

  // hardware back button
      hardwareBackButtonExit(){
        this.platform.registerBackButtonAction(() => {
          // Catches the active view
          let nav = this.app.getActiveNavs()[0];
          let activeView = nav.getActive();                
          // Checks if can go back before show up the alert
          if(activeView.name === 'HomePage') {
              if (nav.canGoBack()){
                  nav.pop();
              } else {
                  const alert = this.alertCtrl.create({
                      title: 'Exit App',
                      message: 'Are you sure?',
                      buttons: [{
                          text: 'Cancel',
                          role: 'cancel',
                          handler: () => {
                            this.nav.setRoot('HomePage');
                            console.log('** Saída do App Cancelada! **');
                          }
                      },{
                          text: 'Exit',
                          handler: () => {
                           // this.logout();
                            this.platform.exitApp();
                          }
                      }]
                  });
                  alert.present();
              }
          }
      });
      }
  // end hardware back button
}
