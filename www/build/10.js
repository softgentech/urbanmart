webpackJsonp([10],{

/***/ 714:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailsModule", function() { return ProductDetailsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_details__ = __webpack_require__(744);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProductDetailsModule = /** @class */ (function () {
    function ProductDetailsModule() {
    }
    ProductDetailsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__product_details__["a" /* ProductDetails */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__product_details__["a" /* ProductDetails */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__product_details__["a" /* ProductDetails */]
            ]
        })
    ], ProductDetailsModule);
    return ProductDetailsModule;
}());

//# sourceMappingURL=product-details.module.js.map

/***/ }),

/***/ 744:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductDetails; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_woocommerce_woocommerce__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


//import { Cart } from '../cart/cart';





var ProductDetails = /** @class */ (function () {
    function ProductDetails(socialSharing, navCtrl, navParams, storage, toastCtrl, modalCtrl, WP, loadingCtrl, events, http, apiPvdr) {
        var _this = this;
        this.socialSharing = socialSharing;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.WP = WP;
        this.loadingCtrl = loadingCtrl;
        this.events = events;
        this.http = http;
        this.apiPvdr = apiPvdr;
        this.item = [];
        this.footerDiv = true;
        this.reviews = [];
        this.selectedOptions = {};
        this.requireOptions = true;
        this.productVariations = [];
        this.productPrice = 0.0;
        this.product = this.navParams.get("product");
        this.similarProduct(this.product.id);
        this.permalink = this.product.permalink;
        this.imgSrc = this.product.images[0].src;
        this.WooCommerce = WP.init(true);
        this.WooCommerce.getAsync("products/" + this.product.id + "/reviews").then(function (data) {
            _this.reviews = JSON.parse(data.body);
        }, function (err) {
            console.log(err);
        });
    }
    ProductDetails.prototype.imageShow = function (srcLink) {
        this.imgSrc = srcLink;
    };
    ProductDetails.prototype.ionViewWillEnter = function () {
        var _this = this;
        //this.storage.set("itemWishList",null);
        this.storage.get("itemWishList").then(function (res) {
            // for(let i=0; i<res.length; i++){
            //   console.log('ii',res[i].id);
            // }
            if (res) {
                _this.item = res;
                console.log("itemWishList:1", _this.item);
            }
        });
        this.storage.get("cart").then(function (val) {
            if (val == null) {
                console.log("cart_null");
            }
            else {
                _this.cartLength = val.length;
            }
        });
    };
    ProductDetails.prototype.addToCart = function (product) {
        var _this = this;
        //counting selected attribute options
        var count = 0;
        for (var k in this.selectedOptions)
            if (this.selectedOptions.hasOwnProperty(k))
                count++;
        //counting variation attributes options
        var count_ = 0;
        for (var index = 0; index < this.product.attributes.length; index++) {
            if (this.product.attributes[index].variation)
                count_++;
        }
        count_ = 0; // change
        count = 0; // change
        //checking if user selected all the variation options or not
        if (count_ != count || this.requireOptions) {
            this.toastCtrl
                .create({
                message: "Select Product Options",
                duration: 2000,
                showCloseButton: true,
            })
                .present();
            return;
        }
        this.storage.get("cart").then(function (data) {
            if (data == undefined || data.length == 0) {
                data = [];
                data.push({
                    product: product,
                    qty: 1,
                    amount: parseFloat(product.price),
                });
                if (_this.selectedVariation) {
                    data[0].variation = _this.selectedVariation;
                    data[0].amount = parseFloat(_this.selectedVariation.price);
                }
            }
            else {
                var alreadyAdded = false;
                var alreadyAddedIndex = -1;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].product.id == product.id) {
                        //Product ID matched
                        if (_this.productVariations.length > 0) {
                            //Now match variation ID also if it exists
                            if (data[i].variation.id == _this.selectedVariation.id) {
                                alreadyAdded = true;
                                alreadyAddedIndex = i;
                                break;
                            }
                        }
                        else {
                            //product is simple product so variation does not  matter
                            alreadyAdded = true;
                            alreadyAddedIndex = i;
                            break;
                        }
                    }
                }
                if (alreadyAdded == true) {
                    if (_this.selectedVariation) {
                        data[alreadyAddedIndex].qty =
                            parseFloat(data[alreadyAddedIndex].qty) + 1;
                        data[alreadyAddedIndex].amount =
                            parseFloat(data[alreadyAddedIndex].amount) +
                                parseFloat(_this.selectedVariation.price);
                        data[alreadyAddedIndex].variation = _this.selectedVariation;
                    }
                    else {
                        data[alreadyAddedIndex].qty =
                            parseFloat(data[alreadyAddedIndex].qty) + 1;
                        data[alreadyAddedIndex].amount =
                            parseFloat(data[alreadyAddedIndex].amount) +
                                parseFloat(data[alreadyAddedIndex].product.price);
                    }
                }
                else {
                    if (_this.selectedVariation) {
                        data.push({
                            product: product,
                            qty: 1,
                            amount: parseFloat(_this.selectedVariation.price),
                            variation: _this.selectedVariation,
                        });
                    }
                    else {
                        data.push({
                            product: product,
                            qty: 1,
                            amount: parseFloat(product.price),
                        });
                    }
                }
            }
            _this.storage.set("cart", data).then(function () {
                console.log(data);
                _this.cartLength = data.length;
                _this.events.publish("cartLength:created", data.length); // set cart length events
                _this.toastCtrl
                    .create({
                    message: "Cart Updated",
                    duration: 500,
                })
                    .present();
            });
        });
    };
    ProductDetails.prototype.openCart = function () {
        //this.modalCtrl.create(Cart).present();
        this.navCtrl.push("CartPage");
        //this.navCtrl.push('my-page');
    };
    ProductDetails.prototype.check = function (justSelectedAttribute) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var loading, count, k, count_, index, _a, _b, _c, i, matchFailed, key, j;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        loading = this.loadingCtrl.create({
                            content: "Getting Product Variations",
                        });
                        count = 0;
                        for (k in this.selectedOptions)
                            if (this.selectedOptions.hasOwnProperty(k))
                                count++;
                        count_ = 0;
                        for (index = 0; index < this.product.attributes.length; index++) {
                            if (this.product.attributes[index].variation)
                                count_++;
                        }
                        count_ = 0; // change
                        count = 0; // change
                        if (!(count_ != count)) return [3 /*break*/, 1];
                        this.requireOptions = true;
                        return [2 /*return*/];
                    case 1:
                        this.requireOptions = false;
                        //Get product variations only once when all product variables are selected by the user
                        loading.present();
                        _a = this;
                        _c = (_b = JSON).parse;
                        return [4 /*yield*/, this.WooCommerce.getAsync("products/" + this.product.id + "/variations/")];
                    case 2:
                        _a.productVariations = _c.apply(_b, [(_d.sent()).body]);
                        _d.label = 3;
                    case 3:
                        i = 0, matchFailed = false;
                        if (this.productVariations.length > 0) {
                            for (i = 0; i < this.productVariations.length; i++) {
                                matchFailed = false;
                                key = "";
                                for (j = 0; j < this.productVariations[i].attributes.length; j++) {
                                    key = this.productVariations[i].attributes[j].name;
                                    console.log("TEST", this.selectedOptions[key].toLowerCase() +
                                        " " +
                                        this.productVariations[i].attributes[j].option.toLowerCase());
                                    if (this.selectedOptions[key].toLowerCase() ==
                                        this.productVariations[i].attributes[j].option.toLowerCase()) {
                                        //Do nothing
                                    }
                                    else {
                                        matchFailed = true;
                                        break;
                                    }
                                }
                                if (matchFailed) {
                                    continue;
                                }
                                else {
                                    //found the matching variation
                                    this.productPrice = this.productVariations[i].price;
                                    this.selectedVariation = this.productVariations[i];
                                    this.regularPrice = this.productVariations[i].regular_price; // regular price
                                    this.sellPrice = this.productVariations[i].sale_price; // sell price
                                    this.footerDiv = true; // test
                                    break;
                                }
                            }
                            if (matchFailed == true) {
                                this.toastCtrl
                                    .create({
                                    message: "No Such Product Found",
                                    duration: 3000,
                                })
                                    .present()
                                    .then(function () {
                                    _this.requireOptions = true;
                                });
                            }
                        }
                        else {
                            this.productPrice = this.product.price;
                        }
                        loading.dismiss();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductDetails.prototype.share = function () {
        this.socialSharing.share(this.permalink);
    };
    ProductDetails.prototype.searchPage = function () {
        this.storage.set("Session_BackSearch", "product-details");
        this.navCtrl.setRoot("SearchPage", {}, {
            animate: true,
            direction: "forward",
        });
    };
    // similar product
    ProductDetails.prototype.similarProduct = function (prID) {
        var _this = this;
        var postData = {
            proid: prID,
        };
        this.http
            .post(this.apiPvdr.SimilarProductApi, postData)
            .subscribe(function (data) {
            console.log('similar:', data);
            if (data.code == 200) {
                _this.similarProductList = data.message.related;
            }
            else {
                // do nothing
            }
        }, function (error) {
            console.log(error);
        });
    };
    // testing
    ProductDetails.prototype.check1 = function (justSelectedAttribute, prdct, prID) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var loading, count, k, count_, index, _a, _b, _c, i, matchFailed, key, j;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        this.footerDiv = false; // test
                        this.checkId = prID;
                        loading = this.loadingCtrl.create({
                            content: "Getting Product Variations",
                        });
                        count = 0;
                        for (k in this.selectedOptions)
                            if (this.selectedOptions.hasOwnProperty(k))
                                count++;
                        count_ = 0;
                        for (index = 0; index < prdct.attributes.length; index++) {
                            if (prdct.attributes[index].variation)
                                count_++;
                        }
                        count = 0; // change
                        count_ = 0; // change
                        if (!(count_ != count)) return [3 /*break*/, 1];
                        // this.navCtrl.setRoot(this.navCtrl.getActive().component);  // test
                        this.requireOptions = true;
                        return [2 /*return*/];
                    case 1:
                        // this.selectedOptions = justSelectedAttribute  // change
                        this.requireOptions = false;
                        //Get product variations only once when all product variables are selected by the user
                        loading.present();
                        _a = this;
                        _c = (_b = JSON).parse;
                        return [4 /*yield*/, this.WooCommerce.getAsync("products/" + prdct.id + "/variations/")];
                    case 2:
                        _a.productVariations = _c.apply(_b, [(_d.sent()).body]);
                        _d.label = 3;
                    case 3:
                        i = 0, matchFailed = false;
                        if (this.productVariations.length > 0) {
                            for (i = 0; i < this.productVariations.length; i++) {
                                matchFailed = false;
                                key = "";
                                for (j = 0; j < this.productVariations[i].attributes.length; j++) {
                                    key = this.productVariations[i].attributes[j].name;
                                    // console.log(this.selectedOptions[key].toLowerCase()+ " " + this.productVariations[i].attributes[j].option.toLowerCase())
                                    if (this.selectedOptions[key].toLowerCase() ==
                                        this.productVariations[i].attributes[j].option.toLowerCase()) {
                                        //Do nothing
                                    }
                                    else {
                                        // console.log(matchFailed)
                                        matchFailed = true;
                                        break;
                                    }
                                }
                                if (matchFailed) {
                                    continue;
                                }
                                else {
                                    //found the matching variation
                                    this.productPrice = this.productVariations[i].price;
                                    this.selectedVariation = this.productVariations[i];
                                    break;
                                }
                            }
                            if (matchFailed == true) {
                                this.toastCtrl
                                    .create({
                                    message: "No Such Product Found",
                                    duration: 3000,
                                })
                                    .present()
                                    .then(function () {
                                    _this.requireOptions = true;
                                });
                            }
                        }
                        else {
                            this.productPrice = this.product.price;
                        }
                        loading.dismiss();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductDetails.prototype.addToCart1 = function (product, prID) {
        var _this = this;
        if (this.checkId == prID) {
            //counting selected attribute options
            var count = 0;
            for (var k in this.selectedOptions)
                if (this.selectedOptions.hasOwnProperty(k))
                    count++;
            //counting variation attributes options
            var count_ = 0;
            for (var index = 0; index < product.attributes.length; index++) {
                if (product.attributes[index].variation)
                    count_++;
            }
            //checking if user selected all the variation options or not
            count_ = 0; // change
            count = 0; // change
            if (count_ != count || this.requireOptions) {
                // change
                this.toastCtrl
                    .create({
                    message: "Select Product Options",
                    duration: 2000,
                    showCloseButton: true,
                })
                    .present();
                return;
            }
            this.storage.get("cart").then(function (data) {
                if (data == undefined || data.length == 0) {
                    data = [];
                    data.push({
                        product: product,
                        qty: 1,
                        amount: parseFloat(product.price),
                    });
                    if (_this.selectedVariation) {
                        data[0].variation = _this.selectedVariation;
                        data[0].amount = parseFloat(_this.selectedVariation.price);
                    }
                }
                else {
                    var alreadyAdded = false;
                    var alreadyAddedIndex = -1;
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].product.id == product.id) {
                            //Product ID matched
                            if (_this.productVariations.length > 0) {
                                //Now match variation ID also if it exists
                                if (data[i].variation.id == _this.selectedVariation.id) {
                                    alreadyAdded = true;
                                    alreadyAddedIndex = i;
                                    break;
                                }
                            }
                            else {
                                //product is simple product so variation does not  matter
                                alreadyAdded = true;
                                alreadyAddedIndex = i;
                                break;
                            }
                        }
                    }
                    if (alreadyAdded == true) {
                        if (_this.selectedVariation) {
                            data[alreadyAddedIndex].qty =
                                parseFloat(data[alreadyAddedIndex].qty) + 1;
                            data[alreadyAddedIndex].amount =
                                parseFloat(data[alreadyAddedIndex].amount) +
                                    parseFloat(_this.selectedVariation.price);
                            data[alreadyAddedIndex].variation = _this.selectedVariation;
                        }
                        else {
                            data[alreadyAddedIndex].qty =
                                parseFloat(data[alreadyAddedIndex].qty) + 1;
                            data[alreadyAddedIndex].amount =
                                parseFloat(data[alreadyAddedIndex].amount) +
                                    parseFloat(data[alreadyAddedIndex].product.price);
                        }
                    }
                    else {
                        if (_this.selectedVariation) {
                            data.push({
                                product: product,
                                qty: 1,
                                amount: parseFloat(_this.selectedVariation.price),
                                variation: _this.selectedVariation,
                            });
                        }
                        else {
                            data.push({
                                product: product,
                                qty: 1,
                                amount: parseFloat(product.price),
                            });
                        }
                    }
                }
                _this.storage.set("cart", data).then(function () {
                    console.log(data);
                    _this.events.publish("cartLength:created", data.length); // set cart length events
                    _this.toastCtrl
                        .create({
                        message: "Cart Updated",
                        duration: 500,
                    })
                        .present();
                    // this.navCtrl.setRoot(this.navCtrl.getActive().component);  // test
                    _this.cartLength = data.length;
                });
            });
        }
        else {
            this.toastCtrl
                .create({
                message: "Select Product Options",
                duration: 2000,
                showCloseButton: true,
            })
                .present();
            return;
        }
    };
    ProductDetails.prototype.openProductPage = function (product) {
        this.navCtrl.push("ProductDetails", { product: product });
    };
    ProductDetails.prototype.WishList = function (product) {
        // this.item.push(product);
        // // Saving item array instead of res value.
        // this.storage.set("itemWishList", this.item).then(
        //   () => {
        //     console.log("Item Stored");
        //   },
        //   (error) => console.error("Error storing item", error)
        // );
        var _this = this;
        this.storage.get("itemWishList").then(function (res) {
            if (res == null) {
                _this.item.push(product);
                _this.storage.set("itemWishList", _this.item).then(function () {
                    console.log("Item Stored---111");
                }, function (error) { return console.error("Error storing item", error); });
            }
            else if (product.id) {
                for (var i = 0; i < res.length; i++) {
                    console.log('ii', res[i].id);
                    if (res[i].id == product.id) {
                        alert("Product already added in wishlist");
                        _this.a = 0;
                    }
                    else {
                        _this.a = 1;
                    }
                }
                if (_this.a == 1) {
                    _this.item.push(product);
                    _this.storage.set("itemWishList", _this.item).then(function () {
                        console.log("Item Stored--022");
                    }, function (error) { return console.error("Error storing item", error); });
                }
                console.log('LoopEnd');
            }
            else {
            }
        });
    };
    ProductDetails = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-product-details",template:/*ion-inline-start:"D:\Amir\app\urbanmart\src\pages\product-details\product-details.html"*/'<ion-header class="headerCart">\n\n  <!-- <ion-navbar>\n    <ion-title> {{ product.title }} </ion-title>\n  </ion-navbar> -->\n\n  <ion-navbar>       \n    <ion-buttons end>\n      <button class="navBtn">\n        <span class="qtySpan" *ngIf="cartLength">{{cartLength}}</span>\n        <ion-icon name="search" (click)="searchPage()"></ion-icon>        \n        <ion-icon name="share" (click)="share()"></ion-icon>\n        <ion-icon name="ios-basket-outline" (click)="openCart()"></ion-icon>\n       \n      </button>\n  </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<!-- <ion-content>\n\n  <ion-fab right top edge (click)="openCart()">\n    <button ion-fab class="fabBtn" color="danger"><ion-icon name="cart"></ion-icon></button>\n  </ion-fab>\n\n  <ion-card>\n    <ion-slides autoplay="3000" loop="true">\n      <ion-slide *ngFor="let image of product.images" style="padding: 30px;">\n        <img [src]="image.src" style="width: 100%;" />\n      </ion-slide>\n    </ion-slides>\n\n    <ion-card-content>\n      <ion-card-title class="cardTitle">\n        {{ product.title }} &nbsp;\n        <ion-chip *ngFor="let cat of product.categories" >\n          <ion-label color="danger" class="detailsLbl"> {{ cat }} </ion-label>\n        </ion-chip>\n      </ion-card-title>\n\n      <div [innerHTML]="product.description"></div>\n      \n    </ion-card-content>\n\n\n  </ion-card>\n\n  <ion-card *ngIf="product.variations.length > 0">\n    <ion-item-divider color="light" class="dividerOption">Product options</ion-item-divider>\n    <ng-container *ngFor="let attribute of product.attributes">\n      <ion-item *ngIf="attribute.variation" class="dvdrItem">\n        <ion-label class="dividerLbl"> {{ attribute.name | titlecase }}</ion-label>\n        <ion-select interface="popover" [(ngModel)]="selectedOptions[attribute.name]" (ionChange)="check(attribute.name)">\n          <ion-option *ngFor="let option of attribute.options" [value]="option">{{ option }}</ion-option>\n        </ion-select>\n      </ion-item>\n    </ng-container>\n  </ion-card>\n\n  <ion-card *ngIf="product.attributes.length > 0">\n    <ion-card-content>\n      <ion-card-title class="spcTitle">\n        Specifications\n      </ion-card-title>\n\n      <ion-grid>\n        <ion-row *ngFor="let att of product.attributes" class="spcRow">\n          <ion-col col-4>\n            {{ att.name}}\n          </ion-col>\n          <ion-col col-8>\n            <span *ngFor="let option of att.options"> {{ option }}</span>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card *ngIf="reviews.length > 0">\n    <ion-card-content>\n      <ion-card-title>\n        Reviews\n      </ion-card-title>\n\n      <ion-grid>\n        <ion-row *ngFor="let review of reviews">\n          <ion-col col-4>\n            <b>{{ review.reviewer_name }}</b><br/>\n            <span *ngIf="review.rating >= 1">\n            <ion-icon style="color: #d4af37" small name="star"></ion-icon>\n          </span>\n            <span *ngIf="review.rating >= 2">\n            <ion-icon style="color: #d4af37" small name="star"></ion-icon>\n          </span>\n            <span *ngIf="review.rating >= 3">\n            <ion-icon style="color: #d4af37" small name="star"></ion-icon>\n          </span>\n            <span *ngIf="review.rating >= 4">\n            <ion-icon style="color: #d4af37" small name="star"></ion-icon>\n          </span>\n            <span *ngIf="review.rating >= 5">\n            <ion-icon style="color: #d4af37" small name="star"></ion-icon>\n          </span>\n\n          </ion-col>\n          <ion-col col-8>\n            {{ review.review }}\n          </ion-col>\n\n        </ion-row>\n      </ion-grid>\n    </ion-card-content>\n  </ion-card>\n\n</ion-content> -->\n\n<!-- <ion-footer>\n  <ion-toolbar>  \n      <button ion-button icon-left block  color="success" (click)="addToCart(product)" class="btnSelect">\n      <ion-icon name="basket"></ion-icon> {{ requireOptions ? \'Select Product Options\' : \'Add to Cart for \' + \'$\' + \' \' + productPrice}}\n    </button>\n  </ion-toolbar>\n</ion-footer> -->\n\n<!--create new content item-->\n<ion-content>\n<div padding>\n  <ion-chip *ngFor="let cat of product.categories" >\n    <span class="spanAmul">{{ cat }}</span>    \n  </ion-chip>\n\n<p class="ptitle">{{ product.title }}</p>\n\n<span *ngIf="!sellPrice">\n<span class="spanPrice" *ngIf="!productPrice">Rs: {{ product.price }}</span><br>\n<span class="spanPrice" *ngIf="productPrice">Rs: {{ productPrice }}</span><br>\n</span>\n<!-- <span class="spanPrice" *ngIf="product.sale_price == null">Rs: {{ product.price }}</span><br> -->\n\n<span *ngIf="sellPrice">\n<span class="spanPrice">Rs: {{ sellPrice }}</span>&nbsp;&nbsp;&nbsp;<span class="spanMrp">MRP: <span>Rs {{ regularPrice }}</span></span><br>\n</span>\n\n<span *ngIf="product.sale_price != null">\n<span class="spanPrice">Rs: {{ product.sale_price }}</span>&nbsp;&nbsp;&nbsp;<span class="spanMrp">MRP: <span>Rs {{ product.regular_price }}</span></span>\n <!-- <span class="offSpan">11% OFF</span> -->\n <br>\n</span>\n\n<span class="spanTax">(Inclusive of all taxes)</span>\n\n<!--slides-->\n\n    <div class="imageDiv">\n    <img [src]="imgSrc"  />\n\n      <span class="spanGreen">\n    <ion-icon name="ios-radio-button-on-outline"></ion-icon>\n     </span>\n\n     <!-- <span class="spanRed">\n      <ion-icon name="ios-radio-button-on-outline"></ion-icon>\n       </span> -->\n\n  </div>\n \n    <ion-segment [(ngModel)]="segment" class="segImg"> \n      \n      <ion-segment-button value={{image.src}} class="imgSegment" style="height: auto;" *ngFor="let image of product.images">\n        <img [src]="image.src" (click)="imageShow(image.src)" />\n      </ion-segment-button>        \n                  \n    </ion-segment>\n<!--end Slider-->\n</div>\n\n<!--selection div-->\n<div class="divPack">Pack Sizes</div>\n\n<div style="padding: 0px 16px 0px 16px;" *ngIf="product.attributes.length > 0">\n\n \n\n  <ng-container *ngFor="let attribute of product.attributes">\n<ion-list radio-group class="selectList" [(ngModel)]="selectedOptions[attribute.name]"> \n    <span *ngIf="attribute.variation">\n  <ion-item *ngFor="let option of attribute.options">\n    <ion-label>{{ attribute.name | titlecase }}</ion-label>\n    <ion-label>{{ option }}</ion-label>\n    <ion-radio [value]="option"  (ionSelect)="check(attribute.name)"  color="secondary"></ion-radio>\n  </ion-item>\n</span>\n</ion-list>\n</ng-container> \n\n<!-- <ion-list radio-group class="selectList"> \n  <span>\n<ion-item>\n  <ion-label>Weight</ion-label>\n  <ion-label>20 kg</ion-label>\n  <ion-radio value="option"   color="secondary"></ion-radio>\n</ion-item>\n</span>\n<span>\n  <ion-item>\n    <ion-label>Weight</ion-label>\n    <ion-label>10 kg</ion-label>\n    <ion-radio value="option"   color="secondary"></ion-radio>\n  </ion-item>\n  </span>\n  <span>\n    <ion-item>\n      <ion-label>Weight</ion-label>\n      <ion-label>30 kg</ion-label>\n      <ion-radio value="option"   color="secondary"></ion-radio>\n    </ion-item>\n    </span>\n</ion-list> -->\n\n\n</div>\n<!--end selection-->\n\n<div class="deliverDiv"><ion-icon name="car"></ion-icon>Delivers in 1 day</div>\n<!--about product-->\n<div class="aboutDiv">\n  <p class="ptitle" style="border-bottom: 1px solid #d0cbcb;\n  padding-bottom: 6px !important;">About this product</p>\n  <span [innerHTML]="product.description"></span>\n</div>\n\n<!--similar Product-->\n      <div padding *ngIf="similarProductList">\n        <p class="ptitle" style="border-top: 1px solid #d0cbcb;\n        padding: 10px 0px 0px 0px !important;">Similar Product</p>\n\n<div no-border class="scrollable-segments1">\n<ion-segment  value="heart" class="simSegment"> \n      \n  <ion-segment-button value="home1" style="height: auto;" *ngFor="let prdct of similarProductList">\n    <ion-card class="cardSim">\n         <ion-card (click)="openProductPage(prdct)">\n            <img src="{{prdct.images[0].src}}"  />\n        </ion-card>\n        <div class="b">{{prdct.title}}</div>\n          <!-- <div class="divGram">1 kg</div>  -->\n\n          <span *ngIf="prdct.variations.length > 0">\n            <ng-container *ngFor="let attribute of prdct.attributes">\n              <div *ngIf="attribute.variation">    \n  \n                <ion-select class="select12" placeholder="Select option" [(ngModel)]="selectedOptions[attribute.name]" (ionChange)="check1(attribute.name, prdct, prdct.id)">\n                  <!-- <ion-option selected="true"> 200 g- Pouch- Rs 118</ion-option> -->\n                  <ion-option *ngFor="let option of attribute.options" [value]="option">{{ option }}</ion-option>\n                 \n                </ion-select>\n                \n              </div>\n            </ng-container>\n      </span>\n\n        <!-- <div class="addSpanDiv">\n        <span class="spanPrice" style="color: black !important;">&#8377;333</span>&nbsp;&nbsp;&nbsp;<span class="spanMrp">&#8377;<span>75</span></span>\n      </div> -->\n\n      <div class="addSpanDiv">\n        <span class="spanPrice" style="color: black !important;">&#8377;{{prdct.price}}</span>\n      </div>\n        <button ion-button block color="danger" class="simBtn" (click)="addToCart1(prdct, prdct.id)">Add</button>\n     \n\n   </ion-card>\n  </ion-segment-button>\n\n\n              \n</ion-segment>\n</div>\n\n      </div>\n<!--end similar Product-->\n\n\n</ion-content>\n\n<ion-footer *ngIf="footerDiv">\n  <ion-toolbar>  \n      <ion-row class="footerRow">\n        <ion-col class="footerCol1" (click)="WishList(product)">\n          <ion-icon name="ios-bookmark-outline"></ion-icon>\n          SAVE FOR LATER\n        </ion-col>\n        <!-- <ion-col class="footerColSaved">\n          <ion-icon name="ios-bookmark-outline"></ion-icon>\n          SAVED\n        </ion-col> -->\n        <ion-col class="footerCol2" color="success" (click)="addToCart(product)">\n          <ion-icon name="ios-basket-outline"></ion-icon>\n          {{ requireOptions ? \'Select Product Options\' : \'Add to Cart for \' + \'$\' + \' \' + productPrice}}\n        </ion-col>\n      </ion-row>\n  </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"D:\Amir\app\urbanmart\src\pages\product-details\product-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_woocommerce_woocommerce__["a" /* WoocommerceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_6__providers__["a" /* Api */]])
    ], ProductDetails);
    return ProductDetails;
}());

//# sourceMappingURL=product-details.js.map

/***/ })

});
//# sourceMappingURL=10.js.map