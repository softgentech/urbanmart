webpackJsonp([5],{

/***/ 719:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageModule", function() { return SignupPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signup__ = __webpack_require__(749);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var SignupPageModule = /** @class */ (function () {
    function SignupPageModule() {
    }
    SignupPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__signup__["a" /* SignupPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["e" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__signup__["a" /* SignupPage */]),
                __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["b" /* TranslateModule */].forChild(),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_4__signup__["a" /* SignupPage */]
            ]
        })
    ], SignupPageModule);
    return SignupPageModule;
}());

//# sourceMappingURL=signup.module.js.map

/***/ }),

/***/ 749:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4____ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(114);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SignupPage = /** @class */ (function () {
    function SignupPage(navCtrl, user, toastCtrl, translateService, menu, apiPvdr, httpClient, formBuilder, storage, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.user = user;
        this.toastCtrl = toastCtrl;
        this.translateService = translateService;
        this.menu = menu;
        this.apiPvdr = apiPvdr;
        this.httpClient = httpClient;
        this.formBuilder = formBuilder;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.isSubmitted = false;
        // mobile:any;
        this.buttonDisabled = true;
        // The account fields for the login form.
        // If you're using the username field with or without email, make
        // sure to add it to the type
        this.account = {
            name: "Test Human",
            email: "test@example.com",
            password: "test",
        };
        this.translateService.get("SIGNUP_ERROR").subscribe(function (value) {
            _this.signupErrorString = value;
        });
        this.ionicForm = this.formBuilder.group({
            mobile: ["", [__WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["f" /* Validators */].pattern("^[0-9]{10}$")]],
        });
    }
    SignupPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
        // If you have more than one side menu, use the id like below
        // this.menu.swipeEnable(false, 'menu1');
    };
    Object.defineProperty(SignupPage.prototype, "errorControl", {
        // validaion customer
        get: function () {
            return this.ionicForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    SignupPage.prototype.submitForm = function () {
        this.isSubmitted = true;
        if (!this.ionicForm.valid) {
            console.log("Please provide all the required values!");
            return false;
        }
        else {
            console.log(this.ionicForm.value.mobile);
            this.sendOtp(this.ionicForm.value.mobile);
        }
    };
    // generate Otp
    SignupPage.prototype.sendOtp = function (mobile) {
        var _this = this;
        var postData = {
            mobile: mobile,
        };
        //console.log('body:',postData)
        var loading = this.loadingCtrl.create({
            content: "Please wait...",
        });
        loading.present();
        this.httpClient
            .post(this.apiPvdr.otpApi, postData)
            .subscribe(function (data) {
            loading.dismiss();
            console.log("data:", data);
            if (data.code == 200) {
                _this.storage.set("session_mobile", mobile);
                // alert(data.message);
                _this.navCtrl.setRoot("OtpPage", {}, {
                    animate: true,
                    direction: "forward",
                });
            }
            else if (data.code == 401) {
                alert(data.message);
            }
        }, function (error) {
            console.log("Error", error);
        });
    };
    SignupPage.prototype.close = function () {
        this.navCtrl.setRoot("HomePage", {}, {
            animate: true,
            direction: "forward",
        });
    };
    SignupPage.prototype.loginpage = function () {
        this.navCtrl.setRoot("LoginPage", {}, {
            animate: true,
            direction: "forward",
        });
    };
    SignupPage.prototype.doSignup = function () {
        var _this = this;
        // Attempt to login in through our User service
        this.user.signup(this.account).subscribe(function (resp) {
            console.log("details:", resp);
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4____["b" /* MainPage */]);
        }, function (err) {
            console.log(err);
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4____["b" /* MainPage */]);
            // Unable to sign up
            var toast = _this.toastCtrl.create({
                message: _this.signupErrorString,
                duration: 3000,
                position: "top",
            });
            toast.present();
        });
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-signup",template:/*ion-inline-start:"D:\Amir\app\urbanmart\src\pages\signup\signup.html"*/'<!-- <ion-header>\n\n  <ion-navbar> \n      <ion-icon name="close"></ion-icon>  \n    <ion-title>{{ \'SIGNUP_TITLE\' | translate }}</ion-title>\n  </ion-navbar>\n\n  <ion-row>\n    <ion-col size="5" size-sm="5">\n      <ion-icon name="close"></ion-icon>  \n    </ion-col>\n    <ion-col size="2" size-sm="2">\n      <img src="https://p7.hiclipart.com/preview/609/434/345/shopping-cart-computer-icons-online-shopping-shopping-cart.jpg">\n    </ion-col>\n    <ion-col size="5" size-sm="5"></ion-col>\n  </ion-row>  \n\n</ion-header> -->\n\n<ion-content>\n  <ion-row class="header1">\n    <ion-col size="5" size-sm="5">\n      <ion-icon\n        name="close"\n        (click)="close()"\n        style="color: white !important"\n      ></ion-icon>\n    </ion-col>\n    <ion-col size="2" size-sm="2">\n      <img src="/assets/icon/logo3.png" />\n    </ion-col>\n    <ion-col size="5" size-sm="5"></ion-col>\n  </ion-row>\n\n  <!-- <form (submit)="doSignup()"> -->\n\n  <div class="div2">\n    <ion-grid class="loginGrid">\n      <ion-card>\n        <ion-row class="row1">\n          <ion-col\n            size="6"\n            size-sm="6"\n            text-center\n            class="col1"\n            (click)="loginpage()"\n          >\n            Login\n          </ion-col>\n          <ion-col size="6" size-sm="6" text-center> Signup </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-grid>\n\n    <!-- <form (submit)="doSignup()"> -->\n    <form [formGroup]="ionicForm" (ngSubmit)="submitForm()" novalidate>\n      <ion-grid>\n        <ion-card>\n          <ion-list>\n            <ion-item>\n              <ion-label floating>Mobile Number (10-digit)</ion-label>\n              <ion-input type="number" formControlName="mobile"></ion-input>\n            </ion-item>\n\n            <span\n              class="error"\n              *ngIf="isSubmitted && errorControl.mobile.errors?.required"\n            >\n              Mobile number is required.\n            </span>\n            <span\n              class="error"\n              *ngIf="isSubmitted && errorControl.mobile.errors?.pattern"\n            >\n              Please insert 10 digit mobile number.\n            </span>\n\n            <!-- <ion-item>\n        <ion-label floating>{{ \'NAME\' | translate }}</ion-label>\n        <ion-input type="text" [(ngModel)]="account.name" name="name"></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label floating>{{ \'EMAIL\' | translate }}</ion-label>\n        <ion-input type="email" [(ngModel)]="account.email" name="email"></ion-input>\n      </ion-item> -->\n\n            <!--\n      Want to add a Username? Here you go:\n\n      <ion-item>\n        <ion-label floating>Username</ion-label>\n        <ion-input type="text" [(ngModel)]="account.username" name="username"></ion-input>\n      </ion-item>\n      -->\n\n            <!-- <ion-item>\n        <ion-label floating>{{ \'PASSWORD\' | translate }}</ion-label>\n        <ion-input type="password" [(ngModel)]="account.password" name="password"></ion-input>\n      </ion-item> -->\n\n            <div class="div1">\n              <button ion-button color="danger" block type="submit">\n                Signup Using OTP\n              </button>\n            </div>\n          </ion-list>\n          <p>\n            By continuing, you agree to our\n            <span>Terms and Conditions</span> and<span> Privacy Policy.</span>\n          </p>\n        </ion-card>\n      </ion-grid>\n    </form>\n  </div>\n  <!-- </form> -->\n</ion-content>\n'/*ion-inline-end:"D:\Amir\app\urbanmart\src\pages\signup\signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["d" /* User */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["a" /* Api */],
            __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* LoadingController */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ })

});
//# sourceMappingURL=5.js.map