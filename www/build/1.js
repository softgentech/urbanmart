webpackJsonp([1],{

/***/ 723:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateProfilePageModule", function() { return UpdateProfilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__update_profile__ = __webpack_require__(753);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var UpdateProfilePageModule = /** @class */ (function () {
    function UpdateProfilePageModule() {
    }
    UpdateProfilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__update_profile__["a" /* UpdateProfilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__update_profile__["a" /* UpdateProfilePage */]),
            ],
        })
    ], UpdateProfilePageModule);
    return UpdateProfilePageModule;
}());

//# sourceMappingURL=update-profile.module.js.map

/***/ }),

/***/ 753:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var UpdateProfilePage = /** @class */ (function () {
    function UpdateProfilePage(navCtrl, navParams, storage, http, apiPvdr, loadingCtrl, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.http = http;
        this.apiPvdr = apiPvdr;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.isSubmitted = false;
        this.ionicForm = this.formBuilder.group({
            firstname: [''],
            lastname: ['']
        });
    }
    Object.defineProperty(UpdateProfilePage.prototype, "errorControl", {
        // validaion login
        get: function () {
            return this.ionicForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    UpdateProfilePage.prototype.submitForm = function () {
        this.isSubmitted = true;
        if (!this.ionicForm.valid) {
            console.log('Please provide all the required values!');
            return false;
        }
        else {
            //console.log(this.ionicForm.value);   
            this.UpdateProfile(this.ionicForm.value);
        }
    };
    UpdateProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UpdateProfilePage');
    };
    UpdateProfilePage.prototype.back = function () {
        this.navCtrl.setRoot('MyAccountPage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    UpdateProfilePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.get('Session_login').then(function (val) {
            // console.log(val); 
            _this.userID = val.ID;
            console.log('UserID:', _this.userID);
            var loading = _this.loadingCtrl.create({
                content: 'Please wait...'
            });
            loading.present();
            _this.http.get(_this.apiPvdr.userProfileAPI + _this.userID).subscribe(function (response) {
                loading.dismiss();
                _this.userData = response;
                console.log('User_data:', _this.userData);
            });
        });
    };
    UpdateProfilePage.prototype.showdate = function () {
        console.log('Date:', this.myDate);
    };
    UpdateProfilePage.prototype.UpdateProfile = function (value) {
        var postData = {
            userid: this.userID,
            firstname: value.firstname,
            lastname: value.lastname,
            dob: this.myDate
        };
        console.log('body:', postData);
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.http.post(this.apiPvdr.updateProfileAPI, postData)
            .subscribe(function (data) {
            loading.dismiss();
            alert(data.message);
        }, function (error) {
            console.log(error);
        });
    };
    UpdateProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-update-profile',template:/*ion-inline-start:"D:\Amir\app\urbanmart\src\pages\update-profile\update-profile.html"*/'<!--\n  Generated template for the UpdateProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header class="headerCart">\n\n  <ion-navbar  color="danger">  \n       <ion-icon name="arrow-back" (click)="back()" class="subIcon" style="color: white !important;"></ion-icon>\n \n <ion-title>Update Profile</ion-title>\n\n</ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <form [formGroup]="ionicForm" (ngSubmit)="submitForm()" novalidate>\n  <ion-row *ngIf="userData"> \n    \n    <ion-col col-6>\n      <ion-input placeholder="Enter First Name" formControlName="firstname"  value="{{userData.firstname}}"></ion-input>\n    </ion-col>\n    <ion-col col-6>\n      <ion-input placeholder="Enter Last Name" formControlName="lastname"  value="{{userData.lastname}}"></ion-input>\n    </ion-col>\n    <!-- <ion-col col-12>\n      <ion-input placeholder="Mobile Number"  value="{{userData.mobile}}"></ion-input>\n    </ion-col> -->\n    <ion-col col-12>\n      <ion-item class="itemDate">\n        <ion-label *ngIf="userData.dob == null">Date of Birth (MM/DD/YYYY)</ion-label>\n        <ion-label *ngIf="userData.dob != null">{{userData.dob}}</ion-label>\n        <ion-datetime displayFormat="MM/DD/YYYY" [(ngModel)]="myDate" [ngModelOptions]="{standalone: true}"  (ionChange)="showdate()"></ion-datetime>\n      </ion-item>\n    </ion-col>   \n   \n        \n    <!--check-box-set-this-as-my-->\n    <!-- <ion-col col-12>\n      <ion-item class="checkItem">\n        <ion-label>Send me email on promotions, offer and service</ion-label>\n        <ion-checkbox></ion-checkbox>\n      </ion-item>\n    </ion-col> -->\n    <!-- <button ion-button block class="btnAdress" type="submit">Update</button> -->\n \n</ion-row>\n\n<ion-footer>\n  <ion-toolbar>\n      <button ion-button block class="btnAdress" type="submit">Update</button>\n  </ion-toolbar>\n</ion-footer>\n</form>\n</ion-content>\n\n\n'/*ion-inline-end:"D:\Amir\app\urbanmart\src\pages\update-profile\update-profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_4__providers__["a" /* Api */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */]])
    ], UpdateProfilePage);
    return UpdateProfilePage;
}());

//# sourceMappingURL=update-profile.js.map

/***/ })

});
//# sourceMappingURL=1.js.map