webpackJsonp([15],{

/***/ 709:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyAccountPageModule", function() { return MyAccountPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__my_account__ = __webpack_require__(739);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MyAccountPageModule = /** @class */ (function () {
    function MyAccountPageModule() {
    }
    MyAccountPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__my_account__["a" /* MyAccountPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__my_account__["a" /* MyAccountPage */]),
            ],
        })
    ], MyAccountPageModule);
    return MyAccountPageModule;
}());

//# sourceMappingURL=my-account.module.js.map

/***/ }),

/***/ 739:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyAccountPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyAccountPage = /** @class */ (function () {
    function MyAccountPage(navCtrl, navParams, storage, http, apiPvdr, loadingCtrl, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.http = http;
        this.apiPvdr = apiPvdr;
        this.loadingCtrl = loadingCtrl;
        this.events = events;
    }
    MyAccountPage.prototype.ionViewDidLoad = function () {
    };
    MyAccountPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.get("cart").then(function (val) {
            if (val == null) {
                console.log("cart_null");
            }
            else {
                _this.cartLength = val.length;
            }
        });
        this.storage.get('Session_login').then(function (val) {
            _this.userID = val.ID;
            //  console.log('UserID:', this.userID);   
            var loading = _this.loadingCtrl.create({
                content: 'Please wait...'
            });
            loading.present();
            // get user data details api
            _this.http.get(_this.apiPvdr.userProfileAPI + _this.userID).subscribe(function (response) {
                loading.dismiss();
                _this.userData = response;
                //  console.log('User_data:', this.userData);
            });
            // get default address
            _this.http.get(_this.apiPvdr.getAllAddressApi + _this.userID).subscribe(function (response) {
                if (response.code == 200) {
                    for (var i = 0; i < response.data.length; i++) {
                        if (response.data[i].defaultAddress == 1) {
                            _this.Default_Address = response.data[i];
                            console.log('Default_Address:', _this.Default_Address);
                        }
                    }
                }
                else {
                    // do nothing
                }
            });
        });
    };
    MyAccountPage.prototype.close = function () {
        this.navCtrl.setRoot('HomePage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    MyAccountPage.prototype.MyOrder = function () {
        this.navCtrl.setRoot('MyOrderPage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    MyAccountPage.prototype.myAddress = function () {
        this.storage.set('Session_chooseDelPage', 'myAccount');
        this.navCtrl.setRoot('ChooseDelAddressPage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    MyAccountPage.prototype.updateProfile = function () {
        this.navCtrl.setRoot('UpdateProfilePage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    // tab navigation
    MyAccountPage.prototype.homeTab = function () {
        this.navCtrl.setRoot('HomePage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    MyAccountPage.prototype.searchTab = function () {
        this.navCtrl.setRoot('SearchPage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    MyAccountPage.prototype.myListTab = function () {
        this.navCtrl.setRoot('MyListPage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    MyAccountPage.prototype.CartTab = function () {
        this.navCtrl.setRoot('CartPage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    MyAccountPage.prototype.CategoryTab = function () {
        this.navCtrl.setRoot('CategoryPage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    // end tab navigation
    MyAccountPage.prototype.logout = function () {
        this.storage.set('Session_login', null); // session login user data null
        this.events.publish('user:created', null); // events
        this.navCtrl.setRoot('LoginPage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    MyAccountPage.prototype.changeAddress = function () {
        this.storage.set('Session_chooseDelPage', 'myAccount'); // set the session
        this.navCtrl.setRoot('ChooseDelAddressPage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    MyAccountPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-my-account',template:/*ion-inline-start:"D:\Amir\app\urbanmart\src\pages\my-account\my-account.html"*/'<!--\n  Generated template for the MyAccountPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header class="headerCart">\n\n  <ion-navbar  color="danger">  \n       <ion-icon name="arrow-back" (click)="close()" class="subIcon" style="color: white !important;"></ion-icon>\n \n <ion-title>Urban Mart</ion-title>\n\n</ion-navbar>\n\n</ion-header>\n\n<ion-content>\n      <div class="myaccount">MY ACCOUNT</div>\n\n      <ion-row class="rowColor" *ngIf="userData">\n          <ion-col col-3 class="profileCol">\n              <img src="./assets/icon/user.png">\n          </ion-col>\n          <ion-col col-8 >\n              <span>{{userData.firstname}} {{userData.lastname}}</span><br>\n              <span>{{userData.user_email}}</span><br>\n              <span>{{userData.mobile}}</span>\n          </ion-col>\n          <ion-col col-1>\n            <ion-icon name="create" style="color: white !important;" (click)="updateProfile()"></ion-icon>\n          </ion-col>      \n          \n          <!-- <span *ngIf="Default_Address"> -->\n          <ion-col col-2 class="pinCol" *ngIf="Default_Address">\n            <ion-icon name="pin"></ion-icon>\n          </ion-col>\n          <ion-col col-7 class="nameCol" *ngIf="Default_Address">              \n               <span>{{Default_Address.houseno}}-{{Default_Address.apartmentname}}</span><br>\n               <span>{{Default_Address.city}}-{{Default_Address.pincode}}</span>\n          </ion-col>\n        <ion-col col-3 class="spCol" *ngIf="Default_Address">\n            <span (click)="changeAddress()">Change</span>\n        </ion-col>\n      <!-- </span> -->\n\n      </ion-row>     \n\n      <!--list-->\n      <ion-row class="catRow">       \n        <ion-col col-12 (click)="MyOrder()">\n           <ion-icon name="time" style="font-weight: 500 !important;"></ion-icon>\n            <span class="carSpan" style="font-weight: 500 !important;">My Order</span>              \n        </ion-col>        \n    </ion-row>\n    <ion-row class="catRow">       \n      <ion-col col-12 (click)="myAddress()">\n         <ion-icon name="pin" style="font-weight: 500 !important;"></ion-icon>\n          <span class="carSpan" style="font-weight: 500 !important;">My Deliver Address</span>              \n      </ion-col>        \n  </ion-row>\n  <ion-row class="catRow">       \n    <ion-col col-12>\n       <ion-icon name="exit" style="font-weight: 500 !important;"></ion-icon>\n        <span class="carSpan" style="font-weight: 500 !important;" (click)="logout()">Logout</span>              \n    </ion-col>        \n</ion-row>\n\n</ion-content>\n\n<ion-footer>\n  <ion-row class="footerRow">\n\n    <ion-col text-center (click)="homeTab()">\n      <ion-icon name="ios-home-outline"></ion-icon><br>\n      <span>Home</span>        \n    </ion-col>\n\n    <ion-col text-center (click)="CategoryTab()">\n      <ion-icon name="ios-apps-outline"></ion-icon><br>\n      <span>Categories</span>        \n    </ion-col>\n\n    <ion-col text-center (click)="searchTab()">\n      <ion-icon name="search"></ion-icon><br>\n      <span>Search</span>        \n    </ion-col>\n\n    <ion-col text-center (click)="myListTab()">\n      <ion-icon name="ios-document-outline"></ion-icon><br>\n      <span>My List</span>        \n    </ion-col>\n\n    <ion-col text-center (click)="CartTab()">\n      <span class="cartSpan"*ngIf="cartLength">{{cartLength}}</span>\n      <ion-icon name="ios-cart-outline"></ion-icon><br>\n      <span>Cart</span>        \n    </ion-col>\n\n  </ion-row>\n</ion-footer>'/*ion-inline-end:"D:\Amir\app\urbanmart\src\pages\my-account\my-account.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_4__providers__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], MyAccountPage);
    return MyAccountPage;
}());

//# sourceMappingURL=my-account.js.map

/***/ })

});
//# sourceMappingURL=15.js.map