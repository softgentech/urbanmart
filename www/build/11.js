webpackJsonp([11],{

/***/ 713:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpPageModule", function() { return OtpPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__otp__ = __webpack_require__(743);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var OtpPageModule = /** @class */ (function () {
    function OtpPageModule() {
    }
    OtpPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__otp__["a" /* OtpPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["e" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__otp__["a" /* OtpPage */]),
            ],
        })
    ], OtpPageModule);
    return OtpPageModule;
}());

//# sourceMappingURL=otp.module.js.map

/***/ }),

/***/ 743:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OtpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__(89);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var OtpPage = /** @class */ (function () {
    function OtpPage(loadingCtrl, httpClient, apiPvdr, storage, navCtrl, navParams, formBuilder) {
        this.loadingCtrl = loadingCtrl;
        this.httpClient = httpClient;
        this.apiPvdr = apiPvdr;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.sessionMobile = '';
        this.resendDiv = false;
        this.isSubmitted = false;
        this.ionicForm = this.formBuilder.group({
            otp: ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]]
        });
    }
    OtpPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // get session mobile number
        this.storage.get('session_mobile').then(function (val) {
            _this.sessionMobile = val;
            console.log('Mobile:', _this.sessionMobile);
        });
    };
    Object.defineProperty(OtpPage.prototype, "errorControl", {
        // otp validation
        get: function () {
            return this.ionicForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    OtpPage.prototype.submitForm = function () {
        this.isSubmitted = true;
        if (!this.ionicForm.valid) {
            console.log('Please provide all the required values!');
            return false;
        }
        else {
            // console.log(this.ionicForm.value.otp); 
            this.verifyOtp(this.ionicForm.value.otp);
        }
    };
    // end otp validation
    OtpPage.prototype.ionViewDidLoad = function () {
        this.initTimer();
        this.startTimer();
    };
    OtpPage.prototype.backIcon = function () {
        this.navCtrl.setRoot('SignupPage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    OtpPage.prototype.signup = function () {
        this.navCtrl.setRoot('SignupDetailsPage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    OtpPage.prototype.change = function () {
        this.navCtrl.setRoot('SignupPage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    //timer
    OtpPage.prototype.initTimer = function () {
        if (!this.timeInSeconds) {
            this.timeInSeconds = 59;
        }
        this.time = this.timeInSeconds;
        this.runTimer = false;
        this.hasStarted = false;
        this.hasFinished = false;
        this.remainingTime = this.timeInSeconds;
        this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
    };
    OtpPage.prototype.startTimer = function () {
        this.runTimer = true;
        this.hasStarted = true;
        this.timerTick();
    };
    OtpPage.prototype.timerTick = function () {
        var _this = this;
        setTimeout(function () {
            if (!_this.runTimer) {
                return;
            }
            _this.remainingTime--;
            _this.displayTime = _this.getSecondsAsDigitalClock(_this.remainingTime);
            if (_this.remainingTime > 0) {
                _this.timerTick();
            }
            else {
                _this.hasFinished = true;
                // console.log('timeup');
                _this.resendDiv = true;
            }
        }, 1000);
    };
    OtpPage.prototype.getSecondsAsDigitalClock = function (inputSeconds) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        // let hoursString = '';
        var minutesString = '';
        var secondsString = '';
        // hoursString = (hours < 10) ? '0' + hours : hours.toString();
        minutesString = (minutes < 10) ? '0' + minutes : minutes.toString();
        secondsString = (seconds < 10) ? '0' + seconds : seconds.toString();
        // return hoursString + ':' + minutesString + ':' + secondsString;
        return minutesString + ':' + secondsString;
    };
    //end timer
    OtpPage.prototype.resendOtp = function () {
        this.resendDiv = false;
        this.initTimer();
        this.startTimer();
        this.ResendOtpAgain();
    };
    OtpPage.prototype.verifyOtp = function (otp) {
        var _this = this;
        var postData = {
            mobile: this.sessionMobile,
            otp: otp
        };
        // console.log('body:',postData);
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.httpClient.post(this.apiPvdr.otpVerifyApi, postData)
            .subscribe(function (data) {
            // console.log('data:', data); 
            loading.dismiss();
            if (data.code == 200) {
                // alert(data.message);
                _this.navCtrl.setRoot('SignupDetailsPage', {}, {
                    animate: true,
                    direction: 'forward'
                });
            }
            else if (data.code == 201) {
                alert(data.message);
            }
        }, function (error) {
            console.log('Error', error);
        });
    };
    OtpPage.prototype.ResendOtpAgain = function () {
        var postData = {
            mobile: this.sessionMobile
        };
        // console.log('body:',postData)
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.httpClient.post(this.apiPvdr.otpResendApi, postData)
            .subscribe(function (data) {
            loading.dismiss();
            console.log('resendOTPdata:', data);
            // alert(data.message);
        }, function (error) {
            console.log('Error', error);
        });
    };
    OtpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-otp',template:/*ion-inline-start:"D:\Amir\app\urbanmart\src\pages\otp\otp.html"*/'<!--\n  Generated template for the OtpPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n  <ion-navbar>        \n          <ion-icon name="close"></ion-icon>    \n    <ion-title>Signup Using OTP</ion-title>\n  </ion-navbar>  \n</ion-header> -->\n\n<ion-content>\n  <form [formGroup]="ionicForm" (ngSubmit)="submitForm()" novalidate>\n    \n      <ion-row class="rowOtp">\n        <ion-col col-2 class="colIcon">\n          <ion-icon name="arrow-back" (click)="backIcon()"></ion-icon>\n        </ion-col>\n        <ion-col col-8>\n          <p text-center class="p2">Signup Using OTP</p>\n        </ion-col>\n        <ion-col col-2></ion-col>\n      </ion-row>\n  \n      <ion-row >\n        <ion-col size="5" size-sm="5">            \n        </ion-col>\n        <ion-col size="2" size-sm="2">\n          <img src="/assets/icon/logo3.png" style="margin-top: 0px;">\n        </ion-col>\n        <ion-col size="5" size-sm="5"></ion-col>\n      </ion-row>\n\n\n  <ion-grid>\n    <ion-row>\n      <ion-col col-12 text-center>\n        <!-- <p class="p1">Please check the OTP sent to your mobile number</p> -->\n        <span class="p1">Please check the OTP sent to your mobile number</span>\n      </ion-col>\n      <ion-col col-12 text-center>\n        <span class="p3">{{sessionMobile}}</span>\n        <span class="change" (click)="change()">Change</span>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n\n  <ion-item>\n    <ion-label floating>Enter OTP</ion-label>\n    <ion-input type="number" class="input" formControlName="otp"></ion-input>\n  </ion-item>\n  <span class="error" *ngIf="isSubmitted && errorControl.otp.errors?.required">\n    OTP is required.\n  </span> \n  <!-- <span class="error" *ngIf="isSubmitted && errorControl.otp.errors?.pattern">\n     Please insert 10 digit mobile number.\n  </span>  -->\n\n  <ion-row class="otpRow">\n    <ion-col text-center>\n      <span class="p4">{{displayTime}}</span>\n      <span class="change" *ngIf="resendDiv" (click)="resendOtp()">Resend OTP</span>\n    </ion-col>\n  </ion-row>\n\n  <div class="otpBtnDiv">\n  <button ion-button color="danger" block type="submit">Signup</button>\n</div>\n\n</form>\n</ion-content>\n'/*ion-inline-end:"D:\Amir\app\urbanmart\src\pages\otp\otp.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */]])
    ], OtpPage);
    return OtpPage;
}());

//# sourceMappingURL=otp.js.map

/***/ })

});
//# sourceMappingURL=11.js.map