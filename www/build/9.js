webpackJsonp([9],{

/***/ 715:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RazorPageModule", function() { return RazorPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__razor__ = __webpack_require__(745);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RazorPageModule = /** @class */ (function () {
    function RazorPageModule() {
    }
    RazorPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__razor__["a" /* RazorPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__razor__["a" /* RazorPage */]),
            ],
        })
    ], RazorPageModule);
    return RazorPageModule;
}());

//# sourceMappingURL=razor.module.js.map

/***/ }),

/***/ 745:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RazorPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(88);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the RazorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RazorPage = /** @class */ (function () {
    function RazorPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    RazorPage.prototype.ionViewDidLoad = function () {
    };
    RazorPage.prototype.pay = function () {
        var options = {
            description: 'Credits towards consultation',
            image: 'https://i.imgur.com/3g7nmJC.png',
            currency: 'INR',
            key: 'rzp_test_E64IpPTPtRHHWh',
            amount: '5000',
            name: 'foo',
            prefill: {
                email: 'pranav@razorpay.com',
                contact: '8808893632',
                name: 'Mohd Amir'
            },
            theme: {
                color: '#F37254'
            },
            modal: {
                ondismiss: function () {
                    alert('dismissed');
                }
            }
        };
        var successCallback = function (payment_id) {
            alert('payment_id: ' + payment_id);
            //Navigate to another page using the nav controller
            //this.navCtrl.setRoot(SuccessPage)
            //Inject the necessary controller to the constructor
        };
        var cancelCallback = function (error) {
            alert(error.description + ' (Error ' + error.code + ')');
            //Navigate to another page using the nav controller
            //this.navCtrl.setRoot(ErrorPage)
        };
        RazorpayCheckout.open(options, successCallback, cancelCallback);
    };
    RazorPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-razor',template:/*ion-inline-start:"D:\Amir\app\urbanmart\src\pages\razor\razor.html"*/'<!--\n  Generated template for the RazorPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header class="headerCart">\n\n  <ion-navbar  color="danger">  \n       <ion-icon name="arrow-back"  class="subIcon" style="color: white !important;"></ion-icon>\n \n <ion-title>Razor Pay</ion-title>\n\n</ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <ion-grid text-center>\n    <ion-row>\n      <ion-col class="razorCol">\n        Use this Pay button with the attached logic in your app\'s payment page.\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-card class="welcome-card">\n    <img src="/assets/razor.jpeg">\n    <ion-card-header>\n      <h1>Get Started</h1>\n      <h2 class="sample">Razor_pay Sample</h2>\n      <ion-row class="amountCol">\n        <ion-col>\n          Total Payment\n        </ion-col>\n        <ion-col>\n          {{currencyIcon}}{{paymentAmount/100}}\n        </ion-col>\n      </ion-row>\n    </ion-card-header>\n    <ion-card-content>\n      <button class="btnPay" ion-button  block color="success" (click)="pay()">Pay with RazorPay</button>\n    </ion-card-content>\n  </ion-card>\n \n</ion-content>\n\n\n'/*ion-inline-end:"D:\Amir\app\urbanmart\src\pages\razor\razor.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], RazorPage);
    return RazorPage;
}());

//# sourceMappingURL=razor.js.map

/***/ })

});
//# sourceMappingURL=9.js.map