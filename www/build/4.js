webpackJsonp([4],{

/***/ 720:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubCategoryPageModule", function() { return SubCategoryPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sub_category__ = __webpack_require__(750);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SubCategoryPageModule = /** @class */ (function () {
    function SubCategoryPageModule() {
    }
    SubCategoryPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sub_category__["a" /* SubCategoryPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sub_category__["a" /* SubCategoryPage */]),
            ],
        })
    ], SubCategoryPageModule);
    return SubCategoryPageModule;
}());

//# sourceMappingURL=sub-category.module.js.map

/***/ }),

/***/ 750:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubCategoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_api_api__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(89);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SubCategoryPage = /** @class */ (function () {
    function SubCategoryPage(navCtrl, navParams, storage, http, api, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.http = http;
        this.api = api;
        this.loadingCtrl = loadingCtrl;
    }
    SubCategoryPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.storage.get("Session_Category_Id").then(function (val) {
            var postData = {
                categoryid: val
            };
            //  console.log("body:-", postData);
            var loading = _this.loadingCtrl.create({
                content: 'Please wait...'
            });
            loading.present();
            _this.http
                .post(_this.api.Sub_CategoryApi, postData)
                .subscribe(function (data) {
                _this.subCategoryList = data.data;
                loading.dismiss();
                //console.log("subCategoryList:", this.subCategoryList);
            }, function (error) {
                console.log(error);
            });
        });
    };
    // Navigate to my-list
    SubCategoryPage.prototype.productList = function (subID) {
        this.storage.set("Session_subCatId", subID);
        this.navCtrl.setRoot("MyListPage", {}, {
            animate: true,
            direction: "forward",
        });
    };
    SubCategoryPage.prototype.close = function () {
        this.navCtrl.setRoot("HomePage", {}, {
            animate: true,
            direction: "forward",
        });
    };
    SubCategoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: "page-sub-category",template:/*ion-inline-start:"D:\Amir\app\urbanmart\src\pages\sub-category\sub-category.html"*/'<!--\n  Generated template for the SubCategoryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header class="headerCart">\n\n  <ion-navbar  color="danger">  \n       <ion-icon name="arrow-back" class="subIcon" style="color: white !important;" (click)="close()"></ion-icon>\n \n <ion-title>Sub Category</ion-title>\n\n</ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <!--top banner-->\n  <ion-card>\n    <ion-slides loop="true" autoplay="3000" pager>        \n      <ion-slide  class="slide1">\n        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRnndRCmfJgvMT1fKuRzVeZEtxRg7oKp4R8qw&usqp=CAU" />       \n      </ion-slide>\n\n      <ion-slide  class="slide1">\n        <img src="https://virtual-race-submissions.s3-ap-southeast-1.amazonaws.com/images/WEB-BANNER-1900X1000-jpg-16w24052018-155006" />       \n      </ion-slide>\n\n      <ion-slide  class="slide1">\n        <img src="https://ittisa.com/wp-content/uploads/2017/07/2-20.png" />       \n      </ion-slide>\n                 \n    </ion-slides>\n  </ion-card>\n  <!--end top banner-->\n\n  <div padding>\n\n    <ion-card text-center class="shopCard">\n      <span>Shop by Category</span>\n    </ion-card>\n\n    <!-- <ion-card class="cardCategory" style="margin-top: 16px !important;">         \n      <ion-card-content class="contentHome">\n         -->\n          <ion-row>\n           \n            \n            <ion-col class="colShop1" col-6 style="border-bottom: 1px solid #c9c5c5;" *ngFor="let sub of subCategoryList">\n              <ion-card (click)="productList(sub.id)">\n                <ion-card-content class="contentHome">\n              <div class="dottedDiv">\n                 <div>\n                    <img src="{{sub.image}}">\n                 </div>\n                 <div text-center><span class="catSpan">{{sub.name}}</span></div>\n\n                </div>\n              </ion-card-content>\n            </ion-card>\n            </ion-col>\n         \n           \n          </ion-row>\n  \n      <!-- </ion-card-content>\n    </ion-card>  -->\n\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"D:\Amir\app\urbanmart\src\pages\sub-category\sub-category.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_0__providers_api_api__["a" /* Api */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* LoadingController */]])
    ], SubCategoryPage);
    return SubCategoryPage;
}());

//# sourceMappingURL=sub-category.js.map

/***/ })

});
//# sourceMappingURL=4.js.map