webpackJsonp([8],{

/***/ 716:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPageModule", function() { return SearchPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_search_filter__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__search__ = __webpack_require__(746);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SearchPageModule = /** @class */ (function () {
    function SearchPageModule() {
    }
    SearchPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__search__["a" /* SearchPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__search__["a" /* SearchPage */]),
                __WEBPACK_IMPORTED_MODULE_2_ng2_search_filter__["a" /* Ng2SearchPipeModule */]
            ],
        })
    ], SearchPageModule);
    return SearchPageModule;
}());

//# sourceMappingURL=search.module.js.map

/***/ }),

/***/ 746:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_woocommerce_woocommerce__ = __webpack_require__(381);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SearchPage = /** @class */ (function () {
    function SearchPage(navCtrl, navParams, storage, http, api, loadingCtrl, WP, toastCtrl, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.http = http;
        this.api = api;
        this.loadingCtrl = loadingCtrl;
        this.WP = WP;
        this.toastCtrl = toastCtrl;
        this.events = events;
        this.term = '';
        this.noProductDIV = false;
        this.ProductDIV = false;
        this.AllDiv = false;
        this.reviews = [];
        this.selectedOptions = {};
        this.requireOptions = true;
        this.productVariations = [];
        this.productPrice = 0.0;
        //count:any;
        //count_:any;
        this.entryStatus = "";
        this.WooCommerce = WP.init(true);
        events.subscribe('cartLength:created', function (val) {
            _this.cartLength = val;
        });
    }
    SearchPage.prototype.getAllMixProductList = function () {
        var _this = this;
        this.page = 2;
        this.WooCommerce = this.WP.init();
        var loading = this.loadingCtrl.create({
            spinner: "dots",
            content: "Please wait...",
        });
        loading.present();
        this.WooCommerce.getAsync("products").then(function (data) {
            _this.product = JSON.parse(data.body).products;
            console.log("allMixProductList:-", _this.product);
            _this.noProductDIV = false;
            _this.ProductDIV = true;
            loading.dismiss();
        }, function (err) {
            console.log(err);
        });
    };
    SearchPage.prototype.getAllMixProuctListWPJSON = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.http.get(this.api.GetAllMixProductList).subscribe(function (response) {
            loading.dismiss();
            // console.log('response:',response);
            _this.product = response.data;
            console.log("allMixProductList_WPJSON:", _this.product);
            _this.noProductDIV = false;
            _this.ProductDIV = true;
        });
    };
    SearchPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.get("cart").then(function (val) {
            if (val == null) {
                console.log("cart_null");
            }
            else {
                _this.cartLength = val.length;
            }
        });
        // this.getAllMixProductList();
        this.getAllMixProuctListWPJSON();
    };
    // navigate product details
    SearchPage.prototype.openProductPage = function (product) {
        console.log("testing:-", product);
        this.navCtrl.push("ProductDetails", { product: product });
    };
    // testing
    SearchPage.prototype.check = function (justSelectedAttribute, prdct, prID) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var loading, count, k, count_, index, _a, _b, _c, i, matchFailed, key, j;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        console.log('Search');
                        this.checkId = prID;
                        loading = this.loadingCtrl.create({
                            content: "Getting Product Variations"
                        });
                        count = 0;
                        for (k in this.selectedOptions)
                            if (this.selectedOptions.hasOwnProperty(k))
                                count++;
                        count_ = 0;
                        for (index = 0; index < prdct.attributes.length; index++) {
                            if (prdct.attributes[index].variation)
                                count_++;
                        }
                        count = 0; // change
                        count_ = 0; // change
                        if (!(count_ != count)) return [3 /*break*/, 1];
                        // this.navCtrl.setRoot(this.navCtrl.getActive().component);  // test
                        this.requireOptions = true;
                        return [2 /*return*/];
                    case 1:
                        // this.selectedOptions = justSelectedAttribute  // change
                        this.requireOptions = false;
                        //Get product variations only once when all product variables are selected by the user
                        loading.present();
                        _a = this;
                        _c = (_b = JSON).parse;
                        return [4 /*yield*/, this.WooCommerce.getAsync('products/' + prdct.id + '/variations/')];
                    case 2:
                        _a.productVariations = _c.apply(_b, [(_d.sent()).body]);
                        console.log('productVariations:-', this.productVariations);
                        _d.label = 3;
                    case 3:
                        i = 0, matchFailed = false;
                        if (this.productVariations.length > 0) {
                            for (i = 0; i < this.productVariations.length; i++) {
                                matchFailed = false;
                                key = "";
                                for (j = 0; j < this.productVariations[i].attributes.length; j++) {
                                    key = this.productVariations[i].attributes[j].name;
                                    console.log(this.selectedOptions[key].toLowerCase() + " " + this.productVariations[i].attributes[j].option.toLowerCase());
                                    if (this.selectedOptions[key].toLowerCase() == this.productVariations[i].attributes[j].option.toLowerCase()) {
                                        //Do nothing
                                    }
                                    else {
                                        console.log(matchFailed);
                                        matchFailed = true;
                                        break;
                                    }
                                }
                                if (matchFailed) {
                                    continue;
                                }
                                else {
                                    //found the matching variation
                                    //console.log(productVariations[i])
                                    this.productPrice = this.productVariations[i].price;
                                    this.selectedVariation = this.productVariations[i];
                                    console.log(this.selectedVariation);
                                    break;
                                }
                            }
                            if (matchFailed == true) {
                                this.toastCtrl.create({
                                    message: "No Such Product Found",
                                    duration: 3000
                                }).present().then(function () {
                                    _this.requireOptions = true;
                                });
                            }
                        }
                        else {
                            this.productPrice = this.product.price;
                        }
                        loading.dismiss();
                        return [2 /*return*/];
                }
            });
        });
    };
    SearchPage.prototype.addToCart = function (product, prID) {
        // console.log('selectedOptions:',this.selectedOptions);
        var _this = this;
        if (this.checkId == prID) {
            //counting selected attribute options
            var count = 0;
            for (var k in this.selectedOptions)
                if (this.selectedOptions.hasOwnProperty(k))
                    count++;
            //counting variation attributes options
            var count_ = 0;
            for (var index = 0; index < product.attributes.length; index++) {
                if (product.attributes[index].variation)
                    count_++;
            }
            console.log("cart--count_", count_);
            //checking if user selected all the variation options or not
            count_ = 0; // change
            count = 0; // change
            if (count_ != count || this.requireOptions) {
                this.toastCtrl.create({
                    message: "Select Product Options",
                    duration: 2000,
                    showCloseButton: true
                }).present();
                return;
            }
            // console.log('asdsadasd');
            this.storage.get("cart").then(function (data) {
                if (data == undefined || data.length == 0) {
                    data = [];
                    data.push({
                        "product": product,
                        "qty": 1,
                        "amount": parseFloat(product.price)
                    });
                    if (_this.selectedVariation) {
                        data[0].variation = _this.selectedVariation;
                        data[0].amount = parseFloat(_this.selectedVariation.price);
                    }
                    //  console.log('asdsadasd');
                }
                else {
                    var alreadyAdded = false;
                    var alreadyAddedIndex = -1;
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].product.id == product.id) {
                            if (_this.productVariations.length > 0) {
                                if (data[i].variation.id == _this.selectedVariation.id) {
                                    alreadyAdded = true;
                                    alreadyAddedIndex = i;
                                    break;
                                }
                            }
                            else {
                                alreadyAdded = true;
                                alreadyAddedIndex = i;
                                break;
                            }
                        }
                    }
                    if (alreadyAdded == true) {
                        if (_this.selectedVariation) {
                            data[alreadyAddedIndex].qty = parseFloat(data[alreadyAddedIndex].qty) + 1;
                            data[alreadyAddedIndex].amount = parseFloat(data[alreadyAddedIndex].amount) + parseFloat(_this.selectedVariation.price);
                            data[alreadyAddedIndex].variation = _this.selectedVariation;
                        }
                        else {
                            data[alreadyAddedIndex].qty = parseFloat(data[alreadyAddedIndex].qty) + 1;
                            data[alreadyAddedIndex].amount = parseFloat(data[alreadyAddedIndex].amount) + parseFloat(data[alreadyAddedIndex].product.price);
                        }
                    }
                    else {
                        if (_this.selectedVariation) {
                            data.push({
                                product: product,
                                qty: 1,
                                amount: parseFloat(_this.selectedVariation.price),
                                variation: _this.selectedVariation
                            });
                        }
                        else {
                            data.push({
                                product: product,
                                qty: 1,
                                amount: parseFloat(product.price)
                            });
                        }
                    }
                }
                _this.storage.set("cart", data).then(function () {
                    console.log("Cart Updated");
                    console.log(data);
                    _this.events.publish('cartLength:created', data.length); // set cart length events  
                    _this.toastCtrl.create({
                        message: "Cart Updated",
                        duration: 500
                    }).present();
                    // this.navCtrl.setRoot(this.navCtrl.getActive().component);  // test   
                });
            });
        }
        else {
            this.toastCtrl.create({
                message: "Select Product Options",
                duration: 2000,
                showCloseButton: true
            }).present();
            return;
        }
    };
    // tab navigation
    SearchPage.prototype.homeTab = function () {
        this.navCtrl.setRoot("HomePage", {}, {
            animate: true,
            direction: "forward",
        });
    };
    SearchPage.prototype.categoryTab = function () {
        this.navCtrl.setRoot("CategoryPage", {}, {
            animate: true,
            direction: "forward",
        });
    };
    SearchPage.prototype.myListTab = function () {
        this.navCtrl.setRoot("MyListPage", {}, {
            animate: true,
            direction: "forward",
        });
    };
    SearchPage.prototype.CartTab = function () {
        this.navCtrl.setRoot("CartPage", {}, {
            animate: true,
            direction: "forward",
        });
    };
    // end tab navigation
    SearchPage.prototype.back = function () {
        var _this = this;
        this.storage.get('Session_BackSearch').then(function (val) {
            if (val == 'home') {
                _this.navCtrl.setRoot("HomePage", {}, {
                    animate: true,
                    direction: "forward",
                });
            }
            else if (val == 'myList') {
                _this.navCtrl.setRoot("MyListPage", {}, {
                    animate: true,
                    direction: "forward",
                });
            }
            else if (val == 'category') {
                _this.navCtrl.setRoot("CategoryPage", {}, {
                    animate: true,
                    direction: "forward",
                });
            }
            else if (val == 'search') {
                _this.navCtrl.setRoot("HomePage", {}, {
                    animate: true,
                    direction: "forward",
                });
            }
            else if (val == 'product-details') {
                _this.navCtrl.setRoot("HomePage", {}, {
                    animate: true,
                    direction: "forward",
                });
            }
            else {
                _this.navCtrl.setRoot("HomePage", {}, {
                    animate: true,
                    direction: "forward",
                });
            }
        });
    };
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-search",template:/*ion-inline-start:"D:\Amir\app\urbanmart\src\pages\search\search.html"*/'<!--\n  Generated template for the MyListPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar  color="danger">\n    \n  <ion-icon\n      name="arrow-back"\n      (click)="back()"\n      class="subIcon"\n      style="color: white !important"\n    ></ion-icon> \n\n    <!-- <button ion-button menuToggle>\n   <ion-icon name="menu" style="color: white !important;"></ion-icon>\n </button> -->\n\n <ion-title>Search Products</ion-title> \n</ion-navbar>\n\n<ion-toolbar color="danger">\n  <ion-searchbar [(ngModel)]="term"></ion-searchbar>    \n</ion-toolbar>\n\n</ion-header>\n\n<!-- <ion-content>  \n  <div class="divtoplist"> \n  <div no-border class="scrollable-segments">\n<ion-segment  value="heart" class="listSeg">\n  <span style="background-color: #d8d7d7 !important;">\n    <span class="quick">QUICK FILTER</span>\n  </span>\n  <ion-segment-button value="home">\n    <span>bb Royal</span>\n  </ion-segment-button>\n  <ion-segment-button value="heart">\n    <span>Super Saver</span>\n  </ion-segment-button>\n  <ion-segment-button value="pin">\n    <span>Pedigree</span>\n  </ion-segment-button>\n  <ion-segment-button value="popular">\n    <span>bb Popular</span>\n  </ion-segment-button>\n  <ion-segment-button value="Gate">\n    <span>India Gate</span>\n  </ion-segment-button>    \n</ion-segment>\n</div>\n</div>\n</ion-content> -->\n\n<ion-content>\n  \n  <ion-row *ngIf="noProductDIV">\n    <ion-col col-12 class="empty" style="padding: 20px 80px 0px 80px;">\n      <img src="./assets/icon/product.png">\n      <div text-center class="empty1">No Products</div>     \n    </ion-col>      \n  </ion-row>\n\n  <!--product list-->\n  <div *ngIf="ProductDIV">\n  <ion-grid style="border-bottom: 1px solid #d4cfcf" *ngFor="let prdct of product.prodata | filter:term" >\n    <ion-row>\n      <ion-col col-4 (click)="openProductPage(prdct)">\n        <!-- <span class="off">$21 OFF</span> -->\n        <ion-card class="cardList">\n          <img\n            src="{{prdct.images[0].src}}"\n          />\n        </ion-card>\n      </ion-col>\n      <ion-col col-8>\n        <ion-row>\n          <ion-col col-8 class="firstCol"> {{prdct.categories[0]}} </ion-col>\n          <ion-col col-4>\n            <span class="spanCar"\n              ><ion-icon name="car"></ion-icon><span>1 day</span>\n            </span>\n          </ion-col>\n\n          <div class="prNameDiv">{{prdct.title}}</div>\n\n          <!--condition based div-->\n\n          <ion-col col-12 *ngIf="prdct.variations.length > 0">\n            <!--not dropdown-->\n            <!-- <div class="divGram">200g-(8 Cubes)</div>  -->\n\n            <!--show dropdown-->\n            <ng-container *ngFor="let attribute of prdct.attributes">\n            <div *ngIf="attribute.variation">    \n\n              <ion-select class="select1" placeholder="Select option" [(ngModel)]="selectedOptions[attribute.name]" (ionChange)="check(attribute.name, prdct, prdct.id)">\n                <!-- <ion-option selected="true"> 200 g- Pouch- Rs 118</ion-option> -->\n                <ion-option *ngFor="let option of attribute.options" [value]="option">{{ option }}</ion-option>\n               \n              </ion-select>\n              \n            </div>\n          </ng-container>\n          </ion-col>\n\n          <!--end condion based div-->\n\n          <div class="prNameDiv" *ngIf="prdct.sale_price == null">Rs  {{prdct.price}}</div>\n\n          <span style="margin-left: 5px;" *ngIf="prdct.sale_price != null">\n            <span class="spanPrice">Rs: {{ prdct.sale_price }}</span>&nbsp;&nbsp;&nbsp;<span class="spanMrp">MRP: <span>Rs {{ prdct.regular_price }}</span></span><br>\n            </span>\n\n          <div style="width: 100%">\n            <button ion-button color="danger" class="addBtn" (click)="addToCart(prdct, prdct.id)">Add</button>\n          </div>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</div>\n  <!-- end product list-->\n \n</ion-content>\n\n\n<ion-footer>\n  <ion-row class="footerRow">\n\n    <ion-col text-center (click)="homeTab()">\n      <ion-icon name="ios-home-outline"></ion-icon><br>\n      <span>Home</span>        \n    </ion-col>\n\n    <ion-col text-center (click)="categoryTab()">\n      <ion-icon name="ios-apps-outline"></ion-icon><br>\n      <span>Categories</span>        \n    </ion-col>\n\n    <ion-col text-center  style="color: green !important;">\n      <ion-icon name="search" style="color: green !important;"></ion-icon><br>\n      <span>Search</span>        \n    </ion-col>\n\n    <ion-col text-center (click)="myListTab()">\n      <ion-icon name="ios-document-outline"></ion-icon><br>\n      <span>My List</span>        \n    </ion-col>\n\n    <ion-col text-center (click)="CartTab()">\n      <span class="cartSpan"*ngIf="cartLength">{{cartLength}}</span>\n      <ion-icon name="ios-cart-outline"></ion-icon><br>\n      <span>Cart</span>        \n    </ion-col>\n\n  </ion-row>\n</ion-footer>'/*ion-inline-end:"D:\Amir\app\urbanmart\src\pages\search\search.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_4__providers__["a" /* Api */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_woocommerce_woocommerce__["a" /* WoocommerceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], SearchPage);
    return SearchPage;
}());

//# sourceMappingURL=search.js.map

/***/ })

});
//# sourceMappingURL=8.js.map