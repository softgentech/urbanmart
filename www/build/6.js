webpackJsonp([6],{

/***/ 718:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupDetailsPageModule", function() { return SignupDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_details__ = __webpack_require__(748);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(32);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SignupDetailsPageModule = /** @class */ (function () {
    function SignupDetailsPageModule() {
    }
    SignupDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__signup_details__["a" /* SignupDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["e" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__signup_details__["a" /* SignupDetailsPage */]),
            ],
        })
    ], SignupDetailsPageModule);
    return SignupDetailsPageModule;
}());

//# sourceMappingURL=signup-details.module.js.map

/***/ }),

/***/ 748:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(114);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






;
/**
 * Generated class for the SignupDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SignupDetailsPage = /** @class */ (function () {
    function SignupDetailsPage(navCtrl, navParams, httpClient, apiPvdr, formBuilder, storage, events, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpClient = httpClient;
        this.apiPvdr = apiPvdr;
        this.formBuilder = formBuilder;
        this.storage = storage;
        this.events = events;
        this.loadingCtrl = loadingCtrl;
        this.sessionMobile = '';
        this.isSubmitted = false;
        // validation customer
        this.ionicForm = this.formBuilder.group({
            firstname: ['', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required]],
            lastname: ['', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required]],
            //  username: ['', [Validators.required]],
            email: ['', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].pattern('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}')]],
        }, {
            validators: this.password.bind(this)
        });
        // end validation
    }
    SignupDetailsPage.prototype.password = function (formGroup) {
        var password = formGroup.get('password').value;
        var con_password = formGroup.get('con_password').value;
        return password === con_password ? null : { passwordNotMatch: true };
    };
    Object.defineProperty(SignupDetailsPage.prototype, "errorControl", {
        get: function () {
            return this.ionicForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    SignupDetailsPage.prototype.submitForm = function () {
        this.isSubmitted = true;
        if (!this.ionicForm.valid) {
            console.log('Please provide all the required values!');
            return false;
        }
        else {
            console.log(this.ionicForm.value);
            this.sendPostRequest(this.ionicForm.value); // post method
        }
    };
    SignupDetailsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad SignupDetailsPage');
        // get session mobile number
        this.storage.get('session_mobile').then(function (val) {
            _this.sessionMobile = val;
        });
        this.storage.get('session_uid').then(function (val) {
            _this.player_id = val;
            console.log('Your playerID is:', _this.player_id);
        });
    };
    SignupDetailsPage.prototype.closeIcon = function () {
        this.navCtrl.setRoot('SignupPage', {}, {
            animate: true,
            direction: 'forward'
        });
    };
    SignupDetailsPage.prototype.sendPostRequest = function (data) {
        var _this = this;
        var postData = {
            firstname: data.firstname,
            lastname: data.lastname,
            // username: data.username,
            email: data.email,
            // password: data.password,
            mobile: this.sessionMobile,
            playerId: this.player_id // add playerId
        };
        console.log('body:', postData);
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        // this.httpClient.post("https://aba-india.org/urbanMart/wp-json/wp/v2/users/register", postData)
        this.httpClient.post(this.apiPvdr.signupApi, postData)
            .subscribe(function (data) {
            loading.dismiss();
            console.log("data", data);
            if (data.code == 200) {
                alert(data.message);
                _this.storage.set('Session_login', data); // session login user data
                _this.events.publish('user:created', data); // events
                _this.navCtrl.setRoot('HomePage', {}, {
                    animate: true,
                    direction: 'forward'
                });
            }
            else if (data.code == 406) {
                alert(data.message);
            }
        }, function (error) {
            console.log(error);
        });
    };
    SignupDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-signup-details',template:/*ion-inline-start:"D:\Amir\app\urbanmart\src\pages\signup-details\signup-details.html"*/'<!--\n  Generated template for the SignupDetailsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n  <ion-navbar>\n    <ion-title>signup-details</ion-title>\n  </ion-navbar>\n</ion-header> -->\n\n<ion-content>\n\n  <ion-row class="rowOtp">\n    <ion-col col-2 class="colIcon">\n      <ion-icon name="close" (click)="closeIcon()"></ion-icon>\n    </ion-col>\n    <ion-col col-8>\n      <p text-center class="p2">Almost There!</p>\n    </ion-col>\n    <ion-col col-2></ion-col>\n  </ion-row>\n\n  <ion-row >\n    <ion-col size="5" size-sm="5">            \n    </ion-col>\n    <ion-col size="2" size-sm="2">\n      <img src="/assets/icon/logo3.png" style="margin-top: 0px;">\n    </ion-col>\n    <ion-col size="5" size-sm="5"></ion-col>\n  </ion-row>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col col-12 text-center>       \n        <span class="p1">Help us to know you better</span>\n      </ion-col>     \n    </ion-row>\n  </ion-grid>\n\n  <form [formGroup]="ionicForm" (ngSubmit)="submitForm()" novalidate>\n<ion-row>\n  \n  <ion-col col-6>\n\n    <ion-item>\n      <ion-label floating>First Name</ion-label>\n      <ion-input type="text" class="input" formControlName="firstname"></ion-input>\n    </ion-item>\n    <span class="error" *ngIf="isSubmitted && errorControl.firstname.errors?.required">\n      First Name is required.\n    </span>\n    \n\n  </ion-col>\n  <ion-col col-6>\n\n    <ion-item>\n      <ion-label floating>Last Name</ion-label>\n      <ion-input type="text" class="input" formControlName="lastname"></ion-input>\n    </ion-item>\n    <span class="error" *ngIf="isSubmitted && errorControl.lastname.errors?.required">\n      Last Name is required.\n    </span>\n\n  </ion-col>\n\n\n\n  <ion-col col-12>\n\n    <!-- <ion-item>\n      <ion-label floating>User Name</ion-label>\n      <ion-input type="text" class="input" formControlName="username"></ion-input>\n    </ion-item>\n    <span class="error" *ngIf="isSubmitted && errorControl.username.errors?.required">\n      Username is required.\n    </span>     -->\n\n    <ion-item>\n      <ion-label floating>Email Address</ion-label>\n      <ion-input type="email" class="input" formControlName="email"></ion-input>\n    </ion-item>\n    <span class="error" *ngIf="isSubmitted && errorControl.email.errors?.pattern">\n        Please enter valid email id\n    </span>\n    \n\n    <!-- <ion-item>\n      <ion-label floating>Password</ion-label>\n      <ion-input type="password" class="input" formControlName="password"></ion-input>\n    </ion-item>\n    <span class="error" *ngIf="isSubmitted && errorControl.password.errors?.required">\n      Password is required.\n    </span>\n    <span class="error" *ngIf="isSubmitted && errorControl.password.errors?.minlength">\n      Password should be minimum 6 character.\n    </span>\n\n    <ion-item>\n      <ion-label floating>Confirm Password</ion-label>\n      <ion-input type="password" class="input" formControlName="con_password"></ion-input>\n    </ion-item>\n    <span class="error" *ngIf="isSubmitted && ionicForm.value.password !== ionicForm.value.con_password">\n      Password did not match.\n       </span> -->\n\n  </ion-col>\n \n\n</ion-row>\n\n \n\n  <div class="otpBtnDiv">\n    <button ion-button color="danger" block type="submit">Start Shopping</button>\n  </div>\n</form>\n</ion-content>\n'/*ion-inline-end:"D:\Amir\app\urbanmart\src\pages\signup-details\signup-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__providers__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* Events */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* LoadingController */]])
    ], SignupDetailsPage);
    return SignupDetailsPage;
}());

//# sourceMappingURL=signup-details.js.map

/***/ })

});
//# sourceMappingURL=6.js.map