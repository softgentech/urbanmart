webpackJsonp([13],{

/***/ 711:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyOrderPageModule", function() { return MyOrderPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__my_order__ = __webpack_require__(741);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MyOrderPageModule = /** @class */ (function () {
    function MyOrderPageModule() {
    }
    MyOrderPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__my_order__["a" /* MyOrderPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__my_order__["a" /* MyOrderPage */]),
            ],
        })
    ], MyOrderPageModule);
    return MyOrderPageModule;
}());

//# sourceMappingURL=my-order.module.js.map

/***/ }),

/***/ 741:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyOrderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_geocoder__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_post_post__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__order_details_order_details__ = __webpack_require__(387);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the MyOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MyOrderPage = /** @class */ (function () {
    function MyOrderPage(navCtrl, navParams, geolocation, nativeGeocoder, postPvdr, http, apiPvdr, storage, loadingCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.geolocation = geolocation;
        this.nativeGeocoder = nativeGeocoder;
        this.postPvdr = postPvdr;
        this.http = http;
        this.apiPvdr = apiPvdr;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.emptyDiv = false;
        this.fillDiv = false;
        //Geocoder configuration
        this.geoencoderOptions = {
            useLocale: true,
            maxResults: 5,
        };
    }
    MyOrderPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.get("Session_login").then(function (val) {
            _this.userID = val.ID;
            // console.log('UserID:', this.userID);
            _this.getALlOrderList();
        });
    };
    //Get current coordinates of device
    MyOrderPage.prototype.getGeolocation = function () {
        var _this = this;
        this.geolocation
            .getCurrentPosition()
            .then(function (resp) {
            _this.geoLatitude = resp.coords.latitude;
            _this.geoLongitude = resp.coords.longitude;
            _this.geoAccuracy = resp.coords.accuracy;
            _this.getGeoencoder(_this.geoLatitude, _this.geoLongitude);
        })
            .catch(function (error) {
            alert("Error getting location" + JSON.stringify(error));
        });
    };
    //geocoder method to fetch address from coordinates passed as arguments
    MyOrderPage.prototype.getGeoencoder = function (latitude, longitude) {
        var _this = this;
        this.nativeGeocoder
            .reverseGeocode(latitude, longitude, this.geoencoderOptions)
            .then(function (result) {
            _this.geoAddress = _this.generateAddress(result[0]);
            console.log("address-console:", _this.geoAddress);
        })
            .catch(function (error) {
            alert("Error getting location" + JSON.stringify(error));
        });
    };
    //Return Comma saperated address
    MyOrderPage.prototype.generateAddress = function (addressObj) {
        var obj = [];
        var address = "";
        for (var key in addressObj) {
            obj.push(addressObj[key]);
        }
        obj.reverse();
        for (var val in obj) {
            if (obj[val].length)
                address += obj[val] + ", ";
        }
        return address.slice(0, -2);
    };
    // get All Order
    MyOrderPage.prototype.getALlOrderList = function () {
        var _this = this;
        var postData = {
            userid: this.userID,
        };
        // console.log("body:", postData);
        var loading = this.loadingCtrl.create({
            content: "Please wait...",
        });
        loading.present();
        this.http
            .post(this.apiPvdr.GetAllOrderAPI, postData)
            .subscribe(function (data) {
            loading.dismiss();
            // console.log("Data:", data.data.prodata);
            if (data.code == 200) {
                _this.orderList = data.data.prodata;
                _this.emptyDiv = false;
                _this.fillDiv = true;
            }
            else {
                // do nothing
                _this.emptyDiv = true;
                _this.fillDiv = false;
            }
        }, function (error) {
            console.log(error);
        });
    };
    MyOrderPage.prototype.openModal = function (orderID) {
        var _this = this;
        var profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__order_details_order_details__["a" /* OrderDetailsPage */], {
            orderId: orderID,
            userid: this.userID
        });
        profileModal.onDidDismiss(function (data) {
            // console.log('test2:',data);
            _this.getALlOrderList();
            _this.madalDismissData = JSON.stringify(data);
        });
        profileModal.present();
    };
    MyOrderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-my-order",template:/*ion-inline-start:"D:\Amir\app\urbanmart\src\pages\my-order\my-order.html"*/'<!--\n  Generated template for the MyOrderPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header class="headerCart">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu" style="color: white !important;"></ion-icon>\n    </button>\n    <ion-title>My Order</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-row *ngIf="emptyDiv">\n    <ion-col col-12 class="empty" style="padding: 20px 80px 0px 80px;">\n      <!-- <img src="./assets/icon/address.png">         -->\n      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTuH-uHbmPcHKaR5rVF8kotXRHSxVdCOFYpOQ&usqp=CAU">\n      <div text-center class="empty1" style="color: green;">No Orders</div>     \n    </ion-col>      \n  </ion-row>\n\n\n    <span *ngIf="fillDiv">\n    <ion-card class="myorderCard" *ngFor="let order of orderList">\n      <ion-grid>\n\n        <!-- <ion-row>\n          <ion-col col-12 class="myCol">#100000015</ion-col>  \n          <img class="eyeImage"  src="./assets/icon/eye.png">       \n        </ion-row> -->\n\n        <ion-row style="border-bottom: 1px solid rgb(175, 172, 172);">\n          <ion-col col-8 class="myCol">#{{order.order_number}}</ion-col>  \n          <ion-col col-4>\n            <img class="eyeImage"  src="./assets/icon/eye.png" (click)="openModal(order.id)">  \n          </ion-col>  \n              \n        </ion-row>\n\n       \n\n        <ion-row style="margin-top: 20px;">\n          <ion-col col-4 class="colHeading">Ordered On</ion-col>\n          <ion-col col-1>:</ion-col>\n          <ion-col col-7>{{order.created_at}}</ion-col>\n        </ion-row>\n       \n        <ion-row>\n          <ion-col col-4 class="colHeading">Order Total</ion-col>\n          <ion-col col-1>:</ion-col>\n          <ion-col col-7>{{order.subtotal}}</ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col col-4 class="colHeading">Status</ion-col>\n          <ion-col col-1>:</ion-col>\n          <ion-col col-7>{{order.status}}</ion-col>\n        </ion-row>\n\n        <!-- <ion-row>\n          <ion-col col-4 class="colHeading">Ship To</ion-col>\n          <ion-col col-1>:</ion-col>\n          <ion-col col-7>tedhi puliya, Lucknow, 226021</ion-col>\n        </ion-row> -->\n\n      </ion-grid>\n    </ion-card>\n  </span>\n   \n\n    <!-- <h1>Get Location</h1> -->\n\n  <!-- <button ion-button (click)="getGeolocation()">Get Location</button>  \n address: {{geoAddress }} -->\n\n <!-- <button ion-button (click)="register()"></button> -->\n\n</ion-content>\n'/*ion-inline-end:"D:\Amir\app\urbanmart\src\pages\my-order\my-order.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
            __WEBPACK_IMPORTED_MODULE_4__providers_post_post__["a" /* PostProvider */],
            __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_6__providers__["a" /* Api */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */]])
    ], MyOrderPage);
    return MyOrderPage;
}());

//# sourceMappingURL=my-order.js.map

/***/ })

});
//# sourceMappingURL=13.js.map