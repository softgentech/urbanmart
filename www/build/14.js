webpackJsonp([14],{

/***/ 710:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyListPageModule", function() { return MyListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__my_list__ = __webpack_require__(740);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MyListPageModule = /** @class */ (function () {
    function MyListPageModule() {
    }
    MyListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__my_list__["a" /* MyListPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__my_list__["a" /* MyListPage */]),
            ],
        })
    ], MyListPageModule);
    return MyListPageModule;
}());

//# sourceMappingURL=my-list.module.js.map

/***/ }),

/***/ 740:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_woocommerce_woocommerce__ = __webpack_require__(381);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






/**
 * Generated class for the MyListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MyListPage = /** @class */ (function () {
    function MyListPage(navCtrl, navParams, storage, http, api, loadingCtrl, WP, toastCtrl, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.http = http;
        this.api = api;
        this.loadingCtrl = loadingCtrl;
        this.WP = WP;
        this.toastCtrl = toastCtrl;
        this.events = events;
        this.noProductDIV = false;
        this.ProductDIV = false;
        this.AllDiv = false;
        this.reviews = [];
        this.selectedOptions = {};
        this.requireOptions = true;
        this.productVariations = [];
        this.productPrice = 0.0;
        // count:any;
        // count_:any;
        this.entryStatus = "";
        this.WooCommerce = WP.init(true);
        events.subscribe('cartLength:created', function (val) {
            _this.cartLength = val;
        });
    }
    MyListPage.prototype.getAllMixProductList = function () {
        var _this = this;
        this.page = 2;
        this.WooCommerce = this.WP.init();
        var loading = this.loadingCtrl.create({
            spinner: "dots",
            content: "Please wait...",
        });
        loading.present();
        this.WooCommerce.getAsync("products").then(function (data) {
            _this.noProductDIV = false;
            _this.ProductDIV = true;
            _this.product = JSON.parse(data.body).products;
            console.log("allMixProductList:", _this.product);
            loading.dismiss();
        }, function (err) {
            console.log(err);
        });
    };
    MyListPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.get("cart").then(function (val) {
            if (val == null) {
                console.log("cart_null");
            }
            else {
                _this.cartLength = val.length;
            }
        });
    };
    MyListPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.storage.get('Session_Category_Id').then(function (val) {
            //  console.log('catId-my-list:', val);
            _this.categoryID = val;
        });
        // get Sub_Category id
        this.storage.get("Session_subCatId").then(function (val) {
            _this.subCategoryID = val;
            // console.log("session_subID:-",this.subCategoryID);
            _this.entryStatus = _this.subCategoryID;
            _this.productList(_this.subCategoryID);
            if (_this.categoryID == null && _this.subCategoryID == null) {
                _this.AllDiv = true;
                console.log('Null');
                _this.getAllMixProductList();
            }
            else {
                _this.AllDiv = false;
                console.log('Not_Null');
                // get category id
                _this.storage.get("Session_Category_Id").then(function (val) {
                    _this.categoryID = val;
                    // console.log("session_catID:-", this.categoryID);
                    var postData = {
                        categoryid: _this.categoryID,
                    };
                    //  console.log("body-my-List:-", postData);
                    var loading = _this.loadingCtrl.create({
                        content: "Please wait...",
                    });
                    loading.present();
                    _this.http
                        .post(_this.api.Sub_CategoryApi, postData)
                        .subscribe(function (data) {
                        _this.subCategoryList = data.data;
                        loading.dismiss();
                        // console.log("subCategoryList:", this.subCategoryList);
                    }, function (error) {
                        console.log(error);
                    });
                });
            }
        });
        //  this.getAllMixProductList();
    };
    // get All Product List
    MyListPage.prototype.productList = function (subCatID) {
        var _this = this;
        // console.log("subCatID:-", subCatID);
        this.entryStatus = subCatID;
        var postData = {
            subcatid: subCatID,
        };
        var loading = this.loadingCtrl.create({
            content: "Please wait...",
        });
        loading.present();
        this.http
            .post(this.api.Product_List_Api, postData)
            .subscribe(function (response) {
            loading.dismiss();
            if (response.data == null) {
                _this.noProductDIV = true;
                _this.ProductDIV = false;
            }
            else {
                _this.product = response.data.prodata;
                console.log("product_List:-", _this.product);
                _this.noProductDIV = false;
                _this.ProductDIV = true;
            }
        }, function (error) {
            console.log("Error", error);
        });
    };
    // navigate product details
    MyListPage.prototype.openProductPage = function (product) {
        console.log("testing:-", product);
        this.navCtrl.push("ProductDetails", { product: product });
    };
    // tab navigation
    MyListPage.prototype.homeTab = function () {
        this.navCtrl.setRoot("HomePage", {}, {
            animate: true,
            direction: "forward",
        });
    };
    MyListPage.prototype.searchTab = function () {
        this.storage.set('Session_BackSearch', 'search');
        this.navCtrl.setRoot("SearchPage", {}, {
            animate: true,
            direction: "forward",
        });
    };
    MyListPage.prototype.categoryTab = function () {
        this.navCtrl.setRoot("CategoryPage", {}, {
            animate: true,
            direction: "forward",
        });
    };
    MyListPage.prototype.CartTab = function () {
        this.navCtrl.setRoot("CartPage", {}, {
            animate: true,
            direction: "forward",
        });
    };
    // end tab navigation
    // testing
    MyListPage.prototype.check = function (justSelectedAttribute, prdct, prID) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var loading, count, k, count_, index, _a, _b, _c, i, matchFailed, key, j;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        console.log('My-list0');
                        this.checkId = prID;
                        loading = this.loadingCtrl.create({
                            content: "Getting Product Variations"
                        });
                        count = 0;
                        for (k in this.selectedOptions)
                            if (this.selectedOptions.hasOwnProperty(k))
                                count++;
                        count_ = 0;
                        for (index = 0; index < prdct.attributes.length; index++) {
                            if (prdct.attributes[index].variation)
                                count_++;
                        }
                        count = 0; // change
                        count_ = 0; // change
                        if (!(count_ != count)) return [3 /*break*/, 1];
                        // this.navCtrl.setRoot(this.navCtrl.getActive().component);  // test
                        this.requireOptions = true;
                        return [2 /*return*/];
                    case 1:
                        // this.selectedOptions = justSelectedAttribute  // change
                        this.requireOptions = false;
                        //Get product variations only once when all product variables are selected by the user
                        loading.present();
                        _a = this;
                        _c = (_b = JSON).parse;
                        return [4 /*yield*/, this.WooCommerce.getAsync('products/' + prdct.id + '/variations/')];
                    case 2:
                        _a.productVariations = _c.apply(_b, [(_d.sent()).body]);
                        console.log('productVariations:-', this.productVariations);
                        _d.label = 3;
                    case 3:
                        i = 0, matchFailed = false;
                        if (this.productVariations.length > 0) {
                            for (i = 0; i < this.productVariations.length; i++) {
                                matchFailed = false;
                                key = "";
                                for (j = 0; j < this.productVariations[i].attributes.length; j++) {
                                    key = this.productVariations[i].attributes[j].name;
                                    console.log(this.selectedOptions[key].toLowerCase() + " " + this.productVariations[i].attributes[j].option.toLowerCase());
                                    if (this.selectedOptions[key].toLowerCase() == this.productVariations[i].attributes[j].option.toLowerCase()) {
                                        //Do nothing
                                    }
                                    else {
                                        console.log(matchFailed);
                                        matchFailed = true;
                                        break;
                                    }
                                }
                                if (matchFailed) {
                                    continue;
                                }
                                else {
                                    //found the matching variation
                                    //console.log(productVariations[i])
                                    this.productPrice = this.productVariations[i].price;
                                    this.selectedVariation = this.productVariations[i];
                                    console.log(this.selectedVariation);
                                    break;
                                }
                            }
                            if (matchFailed == true) {
                                this.toastCtrl.create({
                                    message: "No Such Product Found",
                                    duration: 3000
                                }).present().then(function () {
                                    _this.requireOptions = true;
                                });
                            }
                        }
                        else {
                            this.productPrice = this.product.price;
                        }
                        loading.dismiss();
                        return [2 /*return*/];
                }
            });
        });
    };
    MyListPage.prototype.addToCart = function (product, prID) {
        // console.log('selectedOptions:',this.selectedOptions);
        var _this = this;
        if (this.checkId == prID) {
            //counting selected attribute options
            var count = 0;
            for (var k in this.selectedOptions)
                if (this.selectedOptions.hasOwnProperty(k))
                    count++;
            //counting variation attributes options
            var count_ = 0;
            for (var index = 0; index < product.attributes.length; index++) {
                if (product.attributes[index].variation)
                    count_++;
            }
            console.log("cart--count:", count_);
            //checking if user selected all the variation options or not
            count_ = 0; // change
            count = 0; // change
            if (count_ != count || this.requireOptions) {
                this.toastCtrl.create({
                    message: "Select Product Options",
                    duration: 2000,
                    showCloseButton: true
                }).present();
                return;
            }
            // console.log('asdsadasd');
            this.storage.get("cart").then(function (data) {
                if (data == undefined || data.length == 0) {
                    data = [];
                    data.push({
                        "product": product,
                        "qty": 1,
                        "amount": parseFloat(product.price)
                    });
                    if (_this.selectedVariation) {
                        data[0].variation = _this.selectedVariation;
                        data[0].amount = parseFloat(_this.selectedVariation.price);
                    }
                    //  console.log('asdsadasd');
                }
                else {
                    var alreadyAdded = false;
                    var alreadyAddedIndex = -1;
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].product.id == product.id) {
                            if (_this.productVariations.length > 0) {
                                if (data[i].variation.id == _this.selectedVariation.id) {
                                    alreadyAdded = true;
                                    alreadyAddedIndex = i;
                                    break;
                                }
                            }
                            else {
                                alreadyAdded = true;
                                alreadyAddedIndex = i;
                                break;
                            }
                        }
                    }
                    if (alreadyAdded == true) {
                        if (_this.selectedVariation) {
                            data[alreadyAddedIndex].qty = parseFloat(data[alreadyAddedIndex].qty) + 1;
                            data[alreadyAddedIndex].amount = parseFloat(data[alreadyAddedIndex].amount) + parseFloat(_this.selectedVariation.price);
                            data[alreadyAddedIndex].variation = _this.selectedVariation;
                        }
                        else {
                            data[alreadyAddedIndex].qty = parseFloat(data[alreadyAddedIndex].qty) + 1;
                            data[alreadyAddedIndex].amount = parseFloat(data[alreadyAddedIndex].amount) + parseFloat(data[alreadyAddedIndex].product.price);
                        }
                    }
                    else {
                        if (_this.selectedVariation) {
                            data.push({
                                product: product,
                                qty: 1,
                                amount: parseFloat(_this.selectedVariation.price),
                                variation: _this.selectedVariation
                            });
                        }
                        else {
                            data.push({
                                product: product,
                                qty: 1,
                                amount: parseFloat(product.price)
                            });
                        }
                    }
                }
                _this.storage.set("cart", data).then(function () {
                    console.log("Cart Updated");
                    console.log(data);
                    _this.events.publish('cartLength:created', data.length); // set cart length events
                    _this.toastCtrl.create({
                        message: "Cart Updated",
                        duration: 500
                    }).present();
                    // this.navCtrl.setRoot(this.navCtrl.getActive().component);  // test   
                });
            });
        }
        else {
            this.toastCtrl.create({
                message: "Select Product Options",
                duration: 2000,
                showCloseButton: true
            }).present();
            return;
        }
    };
    MyListPage.prototype.ionViewWillUnload = function () {
        console.log('ionViewWillUnload');
        this.storage.set('Session_subCatId', null);
        this.storage.set('Session_Category_Id', null);
    };
    MyListPage.prototype.searchPage = function () {
        this.storage.set('Session_BackSearch', 'myList');
        this.navCtrl.setRoot("SearchPage", {}, {
            animate: true,
            direction: "forward",
        });
    };
    MyListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: "page-my-list",template:/*ion-inline-start:"D:\Amir\app\urbanmart\src\pages\my-list\my-list.html"*/'<!--\n  Generated template for the MyListPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="danger">\n    <button ion-button menuToggle>\n      <ion-icon name="menu" style="color: white !important"></ion-icon>\n    </button>\n    <ion-title>Your Shopping List</ion-title>\n    <ion-icon name="search" class="listIcon" (click)="searchPage()"></ion-icon>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <!--segment button div-->\n  <div class="divtoplist1">\n    <!-- Scrollable Segment -->\n    <div no-border class="scrollable-segments">\n      \n      <ion-segment [(ngModel)]="entryStatus" class="listSeg">\n\n        <ion-segment-button value="all" (click)="getAllMixProductList()" *ngIf="AllDiv">\n          <span>All</span>\n        </ion-segment-button>\n\n        <ion-segment-button [value]="sub.id" *ngFor="let sub of subCategoryList" (click)="productList(sub.id)">\n          <span>{{sub.name}}</span>\n        </ion-segment-button>\n\n      </ion-segment>\n    </div>\n\n    <div text-center class="divOur">Our Recommendations For You</div>\n    <div text-center class="divBased">\n      Based on what customers like you have bought\n    </div>\n  </div>\n  <!--end segment div-->\n\n\n  <ion-row *ngIf="noProductDIV">\n    <ion-col col-12 class="empty" style="padding: 20px 80px 0px 80px;">\n      <img src="./assets/icon/product.png">\n      <div text-center class="empty1">No Products</div>     \n    </ion-col>      \n  </ion-row>\n\n  <!--product list-->\n  <div *ngIf="ProductDIV">\n  <ion-grid style="border-bottom: 1px solid #d4cfcf" *ngFor="let prdct of product" >\n    <ion-row>\n      <ion-col col-4 (click)="openProductPage(prdct)">\n        <!-- <span class="off">$21 OFF</span> -->\n        <ion-card class="cardList">\n          <img\n            src="{{prdct.images[0].src}}"\n          />\n        </ion-card>\n      </ion-col>\n      <ion-col col-8>\n        <ion-row>\n          <ion-col col-8 class="firstCol"> {{prdct.categories[0]}} </ion-col>\n          <ion-col col-4>\n            <span class="spanCar"\n              ><ion-icon name="car"></ion-icon><span>1 day</span>\n            </span>\n          </ion-col>\n\n          <div class="prNameDiv">{{prdct.title}}</div>\n\n          <!--condition based div-->\n\n          <ion-col col-12 *ngIf="prdct.variations.length > 0">\n            <!--not dropdown-->\n            <!-- <div class="divGram">200g-(8 Cubes)</div>  -->\n\n            <!--show dropdown-->\n            <ng-container *ngFor="let attribute of prdct.attributes">\n            <div *ngIf="attribute.variation">    \n\n              <ion-select class="select1" placeholder="Select option" [(ngModel)]="selectedOptions[attribute.name]" (ionChange)="check(attribute.name, prdct, prdct.id)">\n                <!-- <ion-option selected="true"> 200 g- Pouch- Rs 118</ion-option> -->\n                <ion-option *ngFor="let option of attribute.options" [value]="option">{{ option }}</ion-option>\n               \n              </ion-select>\n              \n            </div>\n          </ng-container>\n          </ion-col>\n\n          <!--end condion based div-->\n\n          <div class="prNameDiv" *ngIf="prdct.sale_price == null">Rs  {{prdct.price}}</div>\n\n          <span style="margin-left: 5px;" *ngIf="prdct.sale_price != null">\n            <span class="spanPrice">Rs: {{ prdct.sale_price }}</span>&nbsp;&nbsp;&nbsp;<span class="spanMrp">MRP: <span>Rs {{ prdct.regular_price }}</span></span><br>\n            </span>\n\n          <div style="width: 100%">\n            <button ion-button color="danger" class="addBtn" (click)="addToCart(prdct, prdct.id)">Add</button>\n          </div>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</div>\n  <!-- end product list-->\n \n</ion-content>\n\n<ion-footer>\n  <ion-row class="footerRow">\n    <ion-col text-center (click)="homeTab()">\n      <ion-icon name="ios-home-outline"></ion-icon><br />\n      <span>Home</span>\n    </ion-col>\n\n    <ion-col text-center>\n      <ion-icon name="ios-apps-outline" (click)="categoryTab()"></ion-icon\n      ><br />\n      <span>Categories</span>\n    </ion-col>\n\n    <ion-col text-center (click)="searchTab()">\n      <ion-icon name="search"></ion-icon><br />\n      <span>Search</span>\n    </ion-col>\n\n    <ion-col text-center style="color: green !important">\n      <ion-icon\n        name="ios-document-outline"\n        style="color: green !important"\n      ></ion-icon\n      ><br />\n      <span>My List</span>\n    </ion-col>\n\n    <ion-col text-center (click)="CartTab()">\n      <span class="cartSpan"*ngIf="cartLength">{{cartLength}}</span>\n      <ion-icon name="ios-cart-outline"></ion-icon><br />\n      <span>Cart</span>\n    </ion-col>\n  </ion-row>\n</ion-footer>\n'/*ion-inline-end:"D:\Amir\app\urbanmart\src\pages\my-list\my-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__providers__["a" /* Api */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_woocommerce_woocommerce__["a" /* WoocommerceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* Events */]])
    ], MyListPage);
    return MyListPage;
}());

//# sourceMappingURL=my-list.js.map

/***/ })

});
//# sourceMappingURL=14.js.map